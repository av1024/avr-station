

#include <avr/io.h>
#include "onewire.h"
#include "ds18x20.h"
#include "../uart/uart.h"
#include "../uart/strutil.h"
#include "zprintf.h"

void test(uint8_t hi, uint8_t lo) {
    uint8_t t[2];
    t[0] = lo;
    t[1] = hi;
    char buf[10];
    uint8_t v[3];
    DS18x20Value z;
    DS18x20_ConvertToThemperature(t, v);
    uart_print_bin(t[1]); uart_putch(' '); uart_print_bin(t[0]);
    uart_print(" => ");
    uart_putch(v[0]);
    uart_print_dec(v[1]); uart_putch('.'); uart_print_dec(v[2]);
    uart_print(", ");
    z = DS18x20_ConvertToTemp(t);
    //itoa(z.dec, buf, 10);
    //uart_print(buf); uart_putch('.'); uart_print(int2str(z.frac));
    zhprintf(4, "%d.%u\n\r", z.dec, z.frac);
    
    //uart_putch('\n'); uart_putch('\r');

}

void main(void) {
    uart_init(38400);
    asm volatile("sei");
    uart_print("Test DS18x20.\n\r");
    //1: +85
    uart_print("+125: "); test(0x07, 0xD0);
    uart_print("+85: "); test(0x05, 0x50);
    uart_print("+25.0625: "); test(0x01, 0x91);
    uart_print("+10.125: "); test(0x00, 0xA2);
    uart_print("+0.5: "); test(0, 0x08);
    uart_print("0: "); test(0, 0);
    uart_print("-0.5: "); test(0xFF, 0xF8);
    uart_print("-10.125: "); test(0xFF, 0x5E);
    uart_print("-25.0625: "); test(0xFE, 0x6F);
    uart_print("-55: "); test(0xFC, 0x90);
    for(;;) { ; }

}