// BMP085 sensor handling implementation

#include <avr/io.h>
#include "gcc_macro.h"
#include "i2c.h"
#include "bmp085.h"
#include "delay.h"

#define BMP085_ADDR_READ    0xEF
#define BMP085_ADDR_WRITE   0xEE

BMP085_Values_t BMP085Values;

//static BMP085_eeprom_t BMP085_eeprom;
static BMP085_eeprom_t BMP085_eeprom;

static BMP085_data_t BMP085_data;


uint16_t bmp085_read16(uint8_t addr) {

    i2c_start(BMP085_ADDR_WRITE);
    i2c_write(addr);

    return i2c_read_word(BMP085_ADDR_READ);
}


void bmp085_init(void) {
    #ifdef BMP085_EOC
        BMP085_EOC_DDR &= ~(1<<BMP085_EOC);
        BMP085_EOC_PORT &= ~(1<<BMP085_EOC);
    #endif
}

bool bmp085_readEEPROM(void) {

    for(uint8_t i=0;i<11;i++) {
        BMP085_eeprom.buf16[i] = bmp085_read16(0xAA+(i<<1));
    }
    return 1;
}

bool bmp085_startMeasureT(void) {
    if (i2c_start(BMP085_ADDR_WRITE))
        if (i2c_write(0xF4))
            if (i2c_write(0x2E)) {
                i2c_stop();
                return 1;
            }
    i2c_stop();
    return 0;
}

bool bmp085_startMeasureP(void) {
    if (i2c_start(BMP085_ADDR_WRITE))
        if (i2c_write(0xF4))
            if (i2c_write(0x34 + (BMP085_OSS<<6))) {
                i2c_stop();
                return 1;
            }
    i2c_stop();
    return 0;
}

void bmp085_waitMeasureT(void) {
    #ifdef BMP085_EOC
        while(!(BMP085_EOC_PIN & (1<<BMP085_EOC))) ;
    #else
        delay1ms(5);
    #endif
}

void bmp085_waitMeasureP(void) {
    #ifdef BMP085_EOC
        while(!(BMP085_EOC_PIN & (1<<BMP085_EOC))) ;
    #else
        delay1ms(2 + (3<<BMP085_OSS));
    #endif
}

void bmp085_receiveT(void) {
    BMP085_data.ut = bmp085_read16(0xF6);
}

void bmp085_receiveP(void) {
    uint8_t lsb, msb, xlsb;

    i2c_start(BMP085_ADDR_WRITE);
    i2c_write(0xF6);
    i2c_start(BMP085_ADDR_READ);

    i2c(I2C_RECEIVE_ACK);
    msb = TWDR;
    i2c(I2C_RECEIVE_ACK);
    lsb = TWDR;
    i2c(I2C_RECEIVE_NACK);
    xlsb = TWDR;
    BMP085_data.up = (((uint32_t)msb << 16) | ((uint32_t)lsb << 8) | xlsb) >> (8-BMP085_OSS);

    i2c_stop();
}

bool bmp085_readAll(void) {
    if (!bmp085_startMeasureT()) return 0;
    bmp085_waitMeasureT();
    bmp085_receiveT();
    if (!bmp085_startMeasureP()) return 0;
    bmp085_waitMeasureP();
    bmp085_receiveP();
    return 1;
}

void bmp085_calculate(void) {
    int32_t x1, x2, x3, b3, b5, b6, p;
    uint32_t b4;
    uint32_t b7;
    int16_t tt;

    x1 = (int32_t) BMP085_data.ut;
    x1 = (x1 - BMP085_eeprom.ac6) * BMP085_eeprom.ac5 >> 15;

    x2 = BMP085_eeprom.mc;
    x2 = (x2 << 11) / (x1 + (int32_t)BMP085_eeprom.md);

    b5 = x1 + x2;
    //BMP085Values.temperature = (b5 + 8) >> 4; // 2^4
    tt = (int16_t)((b5 + 8) >> 4);
    BMP085Values.t.intg = tt / 10;
    if (tt >= 0)
        BMP085Values.t.frac = tt % 10;
    else
        BMP085Values.t.frac = (-tt) % 10;

    b6 = b5 - 4000;

    x1 = BMP085_eeprom.b2;
    x1 = (x1 * ((b6 * b6) >> 12)) >> 11;

    x2 = BMP085_eeprom.ac2;
    x2 = (x2 * b6) >> 11;

    x3 = x1 + x2;

    b3 = (int32_t)BMP085_eeprom.ac1;
    b3 = (((b3 * 4 + x3) << BMP085_OSS) + 2) >> 2;

    x1 = (int32_t)BMP085_eeprom.ac3;
    x1 = (x1 * b6) >> 13;

    x2 = BMP085_eeprom.b1;
    x2 = (x2 * ((b6 * b6) >> 12)) >> 16;

    x3 = ((x1 + x2) + 2) >> 2;

    b4 = (uint32_t)BMP085_eeprom.ac4;
    b4 = (b4 * (uint32_t) (x3 + 32768)) >> 15;

    b7 = ((uint32_t)(BMP085_data.up - b3)) * (50000 >> BMP085_OSS);
    if (b7 < 0x80000000) p = (b7 << 1) / b4;
    else p = (b7/b4) << 1;

    x1 = (p >> 8) * (p >> 8);
    x1 = (x1 * 3038) >> 16;

    x2 = (-7357 * p) >> 16;

    p = p + ((x1 + x2 + 3791) >> 4);

    #ifdef BMP085_PRESS_PA
        BMP085Values.p.intg = p;
    #else
        #ifdef BMP085_PRESS_HPA
            BMP085Values.p.intg = (uint16_t)(p / 100);
            #ifdef BMP085_PRESS_FRAC
                BMP085Values.p.frac = (uint8_t)((p % 100) / 10);
            #endif
        #elif defined(BMP085_PRESS_MMHG)
            BMP085Values.p.intg = (uint16_t)(p / 133);
            #ifdef BMP085_PRESS_FRAC
                BMP085Values.p.frac = (uint8_t)((p % 133) / 14); // 13.3 -> 14 => round to 0.1
            #endif
        #else
            #error "No pressure units defined"
        #endif
    #endif
}

bool bmp085_valid(void) {
    for(uint8_t i=0; i<11; i++) {
        if (BMP085_eeprom.buf16[i]==0xFFFF || BMP085_eeprom.buf16[i]==0x0000) return false;
    }

    return true;
}
