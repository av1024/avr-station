//test

#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>

#include <stdlib.h>
#include "../gcc_macro.h"
#include "../delay.h"

#include "uart/uart.h"
#include "uart/strutil.h"
#include "eth/w5100ex.h"
#include "eth/ntp.h"
#include "eth/wol.h"
#include "eth/dns.h"


const __flash W5100_CFG __flash pcfg= {
    { 0x00, 0x08, 0xDC, 0x41, 0x57, 0x30 }, // mac, 00:08:DC == WizNet
    { 10, 0, 0, 101 },
    //{192, 168, 231, 101},
    { 255, 255, 255, 0 },
    //{192, 168, 231, 1}
    { 10, 0, 0, 1 }
};

/*
EEMEM W5100_CFG ecfg= {
    { 0x00, 0x08, 0xDC, 0x41, 0x57, 0x30 }, // mac, 00:08:DC == WizNet
    { 192, 168, 231, 91 },
    { 255, 255, 255, 0 },
    { 192, 168, 231, 2 }
};
*/

//EEMEM uint8_t eeNTP_ip[4] = {146,185,130,223};



void __attribute__((noreturn)) main(void) {

    w5100_init();
    uart_init(38400);
    asm volatile("sei");

    delay100ms(20);

    W51_config_p(&pcfg);
    //W51_config_e(&ecfg);

    uint32_t time;
    uint8_t ntp_ip[4] = {146,185,130,223};
    //uint8_t ntp_ip[4] = {192,168,200,3};
    //eeprom_read_block(ntp_ip[0], eeNTP_ip[0], 4);

    uart_pprint("NTP sync test\n\rNTP IP: ");
    uart_print_mem_d(ntp_ip, 4, '.');

    uart_pprint("\n\rMAC: ");
    for(uint8_t i=0;i<6;i++) {
        uart_print_hex(W51_read(W5100_SHAR+i));
        if (i<5) uart_putch(':');
    }
    uart_pprint("\nIP: ");
    for(uint8_t i=0;i<4;i++) {
        uart_print_dec(W51_read(W5100_SIPR+i));
        if (i<3) uart_putch('.');
    }
    uart_pprint("\nNetmask: ");
    for(uint8_t i=0;i<4;i++) {
        uart_print_dec(W51_read(W5100_SUBR+i));
        if (i<3) uart_putch('.');
    }
    uart_pprint("\nGW: ");
    for(uint8_t i=0;i<4;i++) {
        uart_print_dec(W51_read(W5100_GAR+i));
        if (i<3) uart_putch('.');
    }
    uart_putch('\n');


    uart_pprint("send WOL... ");
    uint8_t mac[]={0xf0, 0xde, 0xf1, 0x5c, 0xD1, 0xb8 };
    uint8_t ip[]={10,0,0,255};
    if (WOL(0, ip,mac)) uart_pprint("sent.\n\r");
    else uart_pprint("fail.\n\r");


    uint8_t aname[] = "pool.ntp.org";
    //uint8_t adns[5];
    uint8_t gdns[4] = { 8, 8, 8, 8 };

    uart_pprint("DNS of ");
    uart_print((char *)aname);
    uart_pprint(": ");

    if (gethostbyname(2, gdns, aname, ntp_ip)) {
        uart_pprint("OK\n");
        for(uint8_t i=0;i<4;i++) {
            uart_print_dec(ntp_ip[i]);
            if (i<3) uart_pprint(".");
        }

    } else {
        uart_pprint("FAILED");
    }
    uart_pprint("\n\r");

    if (NTPTime(1, ntp_ip, &time)) {

        uart_pprint("NTP timestamp: ");
        char c[25];
        ultoa(time, c, 10);
        uart_print(c);
        uart_pprint(" : ");
        //Time *t = gmtime(time);
        Time t;
        gettime(time, &t);
        //localtime(time, &t, 240);
        uart_print(word2str(t.year)); uart_putch('.');
        uart_print_dec(t.month); uart_putch('.');
        uart_print_dec(t.day); uart_putch(' ');
        uart_print(zero_pad2(word2str(t.hour))); uart_putch(':');
        uart_print(zero_pad2(word2str(t.minute))); uart_putch(':');
        uart_print(zero_pad2(word2str(t.second))); uart_pprint(" GMT, back: ");

        time = mktime(&t);
        ultoa(time, c, 10);
        uart_print(c);
        uart_pprint("\n\r");


    } else {
        uart_pprint("FAIL\n\r");
    }


    for(;;) ;

}
