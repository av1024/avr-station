#!/usr/bin/env python2

import os, sys
import socket
import struct

NTP = ('pool.ntp.org', 123)

print "create"
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
print "send 0"
s.sendto('\x1b'+'\0'*47, NTP)
print "receive"
d = s.recvfrom(56)

print "result: %r" % (d,)
