#!/usr/bin/env python2

import time

FIRSTDAY = 6 # 0: sunday
FIRSTYEAR = 1970

ts = int(time.time())
print "TStamp:", ts, "=>", time.asctime(time.gmtime(ts))
print "TStamp/LOC:", ts, "=>", time.asctime(time.gmtime(ts+240*60))

#ts += 240*60;

mds = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

def ts2time(ts):

    sec = ts % 60
    ts //= 60
    min = ts % 60
    ts //= 60
    hrs = ts % 24

    day = ts // 24
    wkday = (day + 6) % 7

    #print hrs, ":", min, ":", sec, "wkday:", wkday, "day*:", day

    year = FIRSTYEAR % 100
    leap400 = 4 - (FIRSTYEAR-1)//100 & 0x03
    #print "Y:", year

    while True:
        dayofyear = 365
        if (year & 3) == 0:
            dayofyear = 366;
            if year==0 or year==100 or year==200:
                leap400 -= 1
                if leap400:
                    dayofyear = 365
        if day < dayofyear:
            break

        day -= dayofyear
        year += 1

    yr = year + FIRSTYEAR // 100 * 100

    #print "Yr:", yr

    if dayofyear & 1 and day > 58:
        day += 1

    mon=1
    while day >= mds[mon-1]:
        day -= mds[mon-1]
        mon += 1

    #print yr, "-", mon, "-", day+1

    return yr, mon, day+1, hrs, min, sec

# == back

def time2ts(yr, mon, day, hrs, min, sec):

    ndays = 0
    leap400 = 4 - (FIRSTYEAR-1)//100 & 0x03
    year =  FIRSTYEAR % 100
    yr = yr - FIRSTYEAR//100*100

    #print "Y@", yr

    while True:
        dy = 365
        if (year & 3) == 0:
            dy = 366
            if year==0 or year==100 or year==200:
                leap400 -= 1
                if leap400:
                    dy = 365
                    #print "leap400"

        if year >= yr:
            break

        ndays += dy

        year += 1

    mm=1
    while mm < mon:
        ndays += mds[mm-1]
        mm += 1

    if dy & 1 and (mon > 2):
        ndays -= 1
        #print "!"

    ndays += day - 1

    #print "Total days:", ndays

    tst = ndays * 24*60*60 + hrs*3600 + min*60 + sec
    #print "TStamp:", tst
    return tst


tst = time.time()
for i in xrange(15):
    ts = int(tst) - i*86400*365
    y,mm,d,h,m,s = ts2time(ts)
    print ts, time.asctime(time.gmtime(ts)), "=>", "%02d %02d %02d:%02d:%02d %04d" % (mm,d,h,m,s,y)
    ts1 = time2ts(y,mm,d,h,m,s)
    print ts1, time.asctime(time.gmtime(ts1))
    print ts - ts1
