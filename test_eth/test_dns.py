#!/usr/bin/env python2

import os, sys
import socket
import struct


def mk_query(aname):
    #QR=0
    #opcode=0
    #header:
    s = ('\x99\x88' #ID = 1
         '\x01\x00' #Flags (RD)
         '\x00\x01' #QDCount = 1
         '\x00\x00' #ANCount
         '\x00\x00' #NSCount
         '\x00\x00' #ARCount
        )

    #question section:
    #q = chr(len(aname)) + aname  + "\x00" # QNAME
    #q = "%s%s%s" % ("\x04", aname, "\x00")
    q = ""
    for qq in aname.split("."):
        q = "%s%s%s" % (q, chr(len(qq)), qq)

    q += "\x00"

    q += "\x00\x01" # QTYPE = A
    q += "\x00\x01" #QCLASS

    return s + q

def extr_qname(buf, pos):
    sz = pos
    nm = ""
    pre_cmp = False
    while(ord(buf[sz]) != 0):
        pre_cmp = False
        qsz = ord(buf[sz])
        if nm:
            nm += "."

        # compression
        if (qsz & 0xC0 == 0xC0):
            qsz = struct.unpack("!H", buf[sz:sz+2])[0] & 0x3FFF
            #print "-comp-", qsz
            sz += 2
            tmp = extr_qname(buf, qsz)
            nm += tmp[1]
            pre_cmp = True
            break

        nm += buf[sz+1:sz+qsz+1]
        sz += qsz+1

    if not pre_cmp:
        sz += 1

    #print "!!! %r" % (buf[sz:])
    ty, cls = struct.unpack("!HH", buf[sz:sz+4])

    return sz+4, nm, ty, cls


def skip_qname(buf, pos):

    while ord(buf[pos]) != 0:
        sz = ord(buf[pos])
        if sz == 0xC0:
            pos += 2
            break
        pos += qsz+1

    if sz != 0xC0:
        pos += 1

    #ty, cls = struct.unpack("!HH", buf[sz:sz+4])

    return pos


DNS = ('192.168.200.3', 53)

print "create"
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#q = mk_query("narodmon.ru")
#q = mk_query("habrahabr.ru")
#q = mk_query("pool.ntp.org")
q = mk_query("31.28.25.35")
print "send req", "%r" % (q,)

s.sendto(q, DNS)
print "receive"
d = s.recvfrom(512)

dd = d[0]
print "result: %r" % (dd,)
print "hdr:"
print "\tid: %r" % dd[0:2]
print "\tflags: %r" % dd[2:4]

qd_cnt = struct.unpack("!H", dd[4:6])[0]
an_cnt = struct.unpack("!H", dd[6:8])[0]
ns_cnt = struct.unpack("!H", dd[8:10])[0]
ar_cnt = struct.unpack("!H", dd[10:12])[0]

print "\tqd cnt: %r" % qd_cnt # dd[4:6]
print "\tan cnt: %r" % an_cnt # dd[6:8]
print "\tns cnt: %r" % ns_cnt # dd[8:10]
print "\tar cnt: %r" % ar_cnt # dd[10:12]

#dd = dd[12:]

pos, nm, ty, cls = extr_qname(dd, 12)

print "QD: QName: ", nm
print "Type:", ty, "Class:", cls

for i in xrange(0,an_cnt):
    pos, nm, ty, cls = extr_qname(dd, pos)
    ttl, rdsz = struct.unpack("!LH", dd[pos:pos+6])
    print "AN: QName", nm, "Type", ty, "Class", cls,
    print "TTL", ttl, "RDLen",rdsz
    pos += 6
    if ty==1 and cls==1 and rdsz==4:
        ip = struct.unpack("BBBB", dd[pos:pos+4])
        print "IP: %d.%d.%d.%d" % ip
    pos += rdsz


for i in xrange(0,ns_cnt):
    pos, nm, ty, cls = extr_qname(dd, pos)
    ttl, rdsz = struct.unpack("!LH", dd[pos:pos+6])
    print "NS: QName", nm, "Type", ty, "Class", cls,
    print "TTL", ttl, "RDLen",rdsz
    pos += 6
    if ty==1 and cls==1 and rdsz==4:
        ip = struct.unpack("BBBB", dd[pos:pos+4])
        print "IP: %d.%d.%d.%d" % ip
    pos += rdsz

for i in xrange(ar_cnt):
    pos, nm, ty, cls = extr_qname(dd, pos)
    ttl, rdsz = struct.unpack("!LH", dd[pos:pos+6])
    print "AR: QName", nm, "Type", ty, "Class", cls,
    print "TTL", ttl, "RDLen",rdsz
    pos += 6
    if ty==1 and cls==1 and rdsz==4:
        ip = struct.unpack("BBBB", dd[pos:pos+4])
        print "IP: %d.%d.%d.%d" % ip
    pos += rdsz


# === test fo C ==

#print "rest DD: %r" % dd[sz+rdsz+10:]

