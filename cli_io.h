#ifndef __CLI_IO_H__
#define __CLI_IO_H__

/* Combined CLI sending functions for W5100 and UART printing
 *
 *
 *
 */

#include <avr/pgmspace.h>
#include "uart/uart.h"
//#include "uart/strutil.h"
#include "eth/w5100ex.h"
#include "zprintf.h"
#include "datetime.h"


//from zprintf.h:
//#define UART_SOCKET      4
//#define LCD_SOCKET       5

#define COMMON_SOCKET     0
#define TELNET_SOCKET     1

#define EFL_ETH     0x01    /* ethernet OK */
#define EFL_TELNET  0x02    /* telnet socket connected */
#define EFL_TELMON  0x04    /* telnet socket in monitor mode */
#define EFL_NTP     0x08    /* NTP sync OK */
extern uint8_t EFlags;

#define SFL_RTC     0x01    /* RTC Present */
#define SFL_BMP     0x02    /* BMP085 valid */
#define SFL_DHT     0x04    /* AM2302 read success */
#define SFL_1W      0x08    /* 1wire devices found */
#define SFL_UARTCMD 0x10    /* UART in CLI mode */
extern uint8_t SFlags;

extern Time dt;

#define cli_pwrite(h, s) cli_write_p(h, PSTR(s))
#define cli_pwriteln(h, s) cli_writeln_p(h, PSTR(s))
#define cli_printf(h, fmt, ...) zhprintf(h, fmt, __VA_ARGS__)
#define cli_printf_p(h, fmt, ...) zhprintf_p(h, fmt, __VA_ARGS__)
#define cli_pprintf(h, fmt, ...) zhprintf_p(h, PSTR(fmt), __VA_ARGS__)
#define _plog(s) _log_p(PSTR(s))

// put character into w5100 buffer W/O sending or write to UART
void cli_putch(uint8_t handle, uint8_t ch);

// put string into w5100 buffer W/O sending or write to UART
void cli_write(uint8_t handle, const uint8_t *str);
//void cli_write_p(uint8_t handle, const char *str);

void cli_write_cr(uint8_t handle); // write '\n' and flush

void cli_write_p(uint8_t handle, const __flash char *str);
void cli_write_e(uint8_t handle, const uint8_t *estr);

// put string into w5100 buffer WITH sending or write to UART. Auto Add CRLF and flush output.
void cli_writeln(uint8_t handle, const uint8_t *str);
void cli_writeln_p(uint8_t handle, const __flash char *str);

// force complete send buffered data and flush TX buffer
void cli_flush(uint8_t handle);

// print prompt. Flush TX
void cli_prompt(uint8_t handle);


void cli_write_ip(uint8_t handle, const uint8_t *ip);
void cli_write_ip_e(uint8_t handle, const uint8_t *ip); // from EEPROM
void cli_write_mac(uint8_t handle, const uint8_t *mac);
void cli_write_mac_e(uint8_t handle, const uint8_t *mac); // from EEPROM

void cli_write_date(uint8_t handle, const Time *t); // "Y-mm-DD "
void cli_write_time(uint8_t handle, const Time *t); // "HH:MM:SS "
void cli_write_datetime(uint8_t handle, const Time *t);


void _log(const void* str);
void _log_p(const void* pstr);
void _log_mac_p(const void* pstr, const uint8_t *mac);
void _log_byte_mac_p(const void* pstr, uint8_t x, const uint8_t *mac);
void _logf(const char *fmt, ...);
void _logf_p(const __flash char *fmt, ...);

#endif  // __CLI_IO_H__
