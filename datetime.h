#ifndef __DATETIME_H__
#define __DATETIME_H__

#include <avr/io.h>

#define DATE_DMY 1
#define DATE_MDY 2
#define DATE_YMD 3

#define DATE_SEPARATOR  '-'
#define DATE_FORMAT  DATE_DMY

extern const __flash uint8_t __flash DayOfMonth[12];

typedef struct {
   uint16_t year;
   uint8_t month;
   uint8_t day;
   uint8_t hour;
   uint8_t minute;
   uint8_t second;
}Time;


//UNIX timestamp conversion routines
uint32_t mktime(const Time *t);

void gettime(uint32_t sec, Time *t);
void localtime(uint32_t time, Time *t, int16_t tz_offset_min);


#endif // __DATETIME_H__
