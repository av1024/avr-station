// main.c
#include <avr/io.h>
#include <avr/wdt.h>
#include <stdlib.h>
#include <util/delay.h>
#include <string.h>

#include "gcc_macro.h"
#include "delay.h"
#include "uart/uart.h"
#include "ir.h"
#include "i2c.h"
#include "bmp085.h"
#include "DHT22.h"
#include "ds1307.h"
#include "onewire.h"
#include "ds18x20.h"
#include "lcd.h"
#include "eep.h"
#include "str.h"
#include "timer.h"
//#include "uart/strutil.h"
#include "eth/w5100ex.h"
#include "eth/ntp.h"
#include "eth/wol.h"
#include "zprintf.h"
#include "cli_io.h"
#include "datetime.h"
#include "sensors.h"
#include "cli.h"

// comment out for Logger mode
//#define SENDER


#define RESET_IP_PORT   PORTD
#define RESET_IP_PIN    0

#define LED_PORT    PORTC
#define LED_PIN     0
#define LED_ON      LED_PORT |= (1<<LED_PIN)
#define LED_OFF     LED_PORT &= ~(1<<LED_PIN)

//from zprintf.h.h:
//#define UART_SOCKET       4
//#define LCD_SOCKET       5

#define LOGGER_SOCKET   COMMON_SOCKET

#define TELNET_PORT     23

//TODO: move to EEPROM
#define NARODMON_PERIOD 15  /* every- minute to send data */

// flag defines in cli_io.h
uint8_t EFlags = 0;
uint8_t SFlags = 0;

// Global date/time
Time dt;

//CLI
#define UART_CLI_TIMEOUT    60   /* in seconds, switch back to "monitor" mode */
extern uint8_t UART_CLI_countdown; // 0: switch UART CLI to "monitor" mode

typedef struct {
    uint8_t buf[CLI_MAXCMDSIZE+1];
    uint8_t len;
} CLIBuffer;

CLIBuffer cli, ecli; // UART, Telnet CLI buffers

#define LCD_DEFAULT     0x00    /* default display mode */
uint8_t lcdMode = 0;

static const __flash uint8_t _sens_macs[4][6] = {
    SENS_DHT_MAC_T,
    SENS_DHT_MAC_RH,
    SENS_BMP_MAC_T,
    SENS_BMP_MAC_P
};


static inline void check_reset_ip(void) {
    if (!(PIN(RESET_IP_PORT) & (1<<RESET_IP_PIN))) {
        register bool do_reset_ip = true;
        lcd_gotoxy(4,2);
        lcd_puts_p(csRESETIP);
        lcd_gotoxy(4,3);
        for (uint8_t i=0;do_reset_ip && (i<5);i++) {
            if ((PIN(RESET_IP_PORT) & (1<<RESET_IP_PIN))) //do_reset_ip = false;
            lcd_putc(ccDOT);
            delay100ms(10);
        }
        lcd_clrscr();
        if (do_reset_ip) {
            reset_ip();
            lcd_puts_p(csOK);
        } else {
            lcd_puts_p(csCANCEL);
        }
    }
}

static void update_RTC(void) {

    int16_t tz = (int16_t)eeprom_read_word((uint16_t*)&eeNTPOFFSET);

    //TODO: test for buggy RTC (ds1307 with bad quartz)
    if (EFlags & EFL_NTP) {
        localtime(unix_timestamp, &dt, tz);
        rtc_set_date(dt.year-2000, dt.month, dt.day);
        rtc_set_time(dt.hour, dt.minute, dt.second);
    } else
        if (rtc_time(&dt)) {
            if ((SFlags & SFL_RTC) == 0) {
                _plog("RTC OK");
            }
            unix_timestamp = mktime(&dt) - 60L*tz;
            SFlags |= SFL_RTC;
        } else {
            if ((SFlags & SFL_RTC)) {
                _plog("RTC FAIL");
            }

            localtime(unix_timestamp, &dt, tz);
            SFlags &= ~SFL_RTC;
        }

}

void write_sens_C(SensorData *x, bool no_alarm) {
    if (!no_alarm) {
        cli_putch(LCD_SOCKET, ccALARM);
        cli_pwrite(LCD_SOCKET, "---.-C ");
    } else {
        cli_putch(LCD_SOCKET, dir_sensor(x));
        if (x->val.intg > 0) {
            cli_pprintf(LCD_SOCKET, "+%2d.%uC ", x->val.intg, x->val.frac);
        } else {
            cli_pprintf(LCD_SOCKET, "%3d.%uC ", x->val.intg, x->val.frac);
        }
    }
}

void paintLCD(void) {
    //uint8_t c1, c2;
    SensorData *x;
    if (lcdMode == LCD_DEFAULT) {
        if (timer.MS != 0) {
            lcd_gotoxy(13,3);
            lcd_putc(ccSPACE);
        } else {
            lcd_gotoxy(0, 0);
            //lcd_clrscr();
            // Line 1
            x = find_sensor_by_id(SENS_DHT_ID_T);
            /*
            if (x && x->flags & SENS_FLAG_OK)
                cli_pprintf(LCD_SOCKET, "%c%3d.%uC ", dir_sensor(x), x->val.intg, x->val.frac);
            else
                cli_pprintf(LCD_SOCKET, "%c---.-C", ccALARM);
            */
            write_sens_C(x, x && x->flags & SENS_FLAG_OK);

            x = find_sensor_by_id(SENS_DHT_ID_RH);
            if (x && x->flags & SENS_FLAG_OK)
                cli_pprintf(LCD_SOCKET, "%c%2u%% ", dir_sensor(x), x->val.intg + ((x->val.frac>4) ? 1 : 0));
            else
                cli_pprintf(LCD_SOCKET, "%c--% ", ccALARM);

            lcd_gotoxy(13, 0);
            cli_pprintf(LCD_SOCKET, " %02u", dt.second);

            // Line 2
            lcd_gotoxy(0, 1);
            x = find_sensor_by_id(SENS_BMP_ID_T);
            /*
            if (x && x->flags & SENS_FLAG_OK)
                    cli_pprintf(LCD_SOCKET, "%c%3d.%uC ",
                            dir_sensor(x),
                            x->val.intg, x->val.frac);
            else
                cli_pprintf(LCD_SOCKET, "%c---.-C ", ccALARM);
            */
            write_sens_C(x, x && x->flags & SENS_FLAG_OK);

            x = find_sensor_by_id(SENS_BMP_ID_P);
            if (x && x->flags & SENS_FLAG_OK)
                    cli_pprintf(LCD_SOCKET, "%c%u.%umm ",
                            dir_sensor(x),
                            x->val.intg + SENS_BMP_BASE,
                            x->val.frac);
                            //+ ((x->val.frac > 4) ? 1 : 0));
            else
                cli_pprintf(LCD_SOCKET, "%c---mm ", ccALARM);
            // Line 3
            lcd_gotoxy(0, 2);
            if (SFlags & SFL_1W) {
                //TODO: rolling string with DS18B20 values
                uint8_t c=0;
                for(uint8_t i=4;i<SENSOR_COUNT && c<3;i++) {
                    x = &sensors[i];
                    if (x->flags & SENS_FLAG_ON) {
                        /*if (x->flags & SENS_FLAG_OK)
                            cli_pprintf(LCD_SOCKET, "%2d.%uC ", x->val.intg, x->val.frac);
                        else
                            cli_pwrite(LCD_SOCKET, "--.-C ");
                            */
                        write_sens_C(x, x && x->flags & SENS_FLAG_OK);
                        c++;
                    }
                }
                //cli_pwrite(LCD_SOCKET, " 1-wire...");
            } else {
                cli_pwrite(LCD_SOCKET, " 1-wire fail");
            }
            // Line 4
            lcd_gotoxy(0, 3);
            if ((SFlags & SFL_RTC) || (EFlags && EFL_NTP) || (timer.S != 0)) {
                cli_write_datetime(LCD_SOCKET, &dt);
            } else {
                cli_pprintf(LCD_SOCKET, "%p", PSTR("  = RTC FAIL = "));
            }
        }
    }
}

bool timeSync(void) {
    lcd_gotoxy(13, 0);
    cli_pwrite(LCD_SOCKET, "NTP");

    if (!(EFlags & EFL_ETH)) return false; // Ethernet fail

    uint8_t ip[4];
    int16_t tz;// = eeprom_read_word(&eeNTPOFFSET);
    eeprom_read_block(&tz, &eeNTPOFFSET, 2);

    for (uint8_t tmp=0;tmp<3;tmp++){
        if (hostbyname_e(eeNTP, ip)) {
            uint32_t ts;
            if (NTPTime(NTP_SOCK, ip, &ts)) {
                int32_t z = (int32_t)unix_timestamp - (int32_t)ts;
                unix_timestamp = ts;
                //COPY32BIT(unix_timestamp, ts);
                EFlags |= EFL_NTP;
                localtime(unix_timestamp, &dt, tz);

                //Set on next RTC_Update
                //rtc_set_date(dt.year-2000, dt.month, dt.day);
                //rtc_set_time(dt.hour, dt.minute, dt.second);
                //_logf_p(PSTR("NTP sync. RTC set %s"), ok ? "ok" : "fail");

                _logf_p(PSTR("NTP sync diff: %ld sec."), z);
                return true;
            }
        }
    }
    EFlags &= ~EFL_NTP;
    _plog("NTP Fail");
    return false;
}

void w5100_reinit(void) {
    if (w5100_init() == W5100_OK) {
        if (W51_config_e(&eeETH) == W5100_OK) EFlags |= EFL_ETH;
    }
}


//TEST
bool udp_log(bool toNM) {
    uint8_t ip[4];
    uint16_t port;
    bool nm_send = false;

    lcd_gotoxy(13, 0);

    // get IP
    if (toNM) {
        cli_pwrite(LCD_SOCKET, " NM");
        if (!hostbyname_e(eeNARODMON, ip)) {
            _plog("NM DNS fail");
            return false;
        }
        port = eeprom_read_word(&eeNARODMON_PORT);
    } else {
        cli_pwrite(LCD_SOCKET, "LOG");
        if (!hostbyname_e(eeLOGGER, ip)) {
            _plog("Log DNS fail");
            return false;
        }
        port = eeprom_read_word(&eeLOGGER_PORT);
    }

    //_logf_p(PSTR("Log to %d.%d.%d.%d:%u"), ip[0], ip[1], ip[2], ip[3], port);

    if (ip_valid(ip) != W5100_OK) {
        _plog("Log IP BAD");
        return false;
    }

    // Does not start send if no sensors enabled
    for(uint8_t i=0; i<SENSOR_COUNT;i++) {
        if (toNM) {
            if ((sensors[i].flags & (SENS_FLAG_NAROD | SENS_FLAG_ON))==(SENS_FLAG_NAROD | SENS_FLAG_ON)) {
                nm_send = true;
                break;
            }
        } else {
            if (sensors[i].flags & SENS_FLAG_ON) {
                nm_send = true;
                break;
            }
        }
    }

    if (!nm_send) return true; // no sensors to send

    // send
    if (sock_open(LOGGER_SOCKET, W5100_SKT_MR_UDP, port) == W5100_SKT_SR_UDP) {

        sock_set_dest(LOGGER_SOCKET, ip, port);

        cli_pwrite(LOGGER_SOCKET, "#");
        cli_write_mac_e(LOGGER_SOCKET, &eeETH.mac_addr[0]);
        cli_write_p(LOGGER_SOCKET, csCR);


        for(uint8_t i=0; i<SENSOR_COUNT;i++) {
            SensorData x = sensors[i];
            SensorValue v;
            if (x.id == 0) continue;
            if (!(x.flags & SENS_FLAG_OK)) continue;
            if (!(x.flags & SENS_FLAG_ON)) continue;
            if (toNM && !(x.flags & SENS_FLAG_NAROD)) continue;

            v = avg_sensor(&x);
            if (x.id == SENS_BMP_ID_P) v.intg += SENS_BMP_BASE;

            sock_put_p(LOGGER_SOCKET, PSTR("#"));
            for(uint8_t j=0;j<6;j++) cli_printf_p(LOGGER_SOCKET, csFMT02X, x.mac[j]);
            //cli_pprintf(LOGGER_SOCKET, "#%d.%u0#%ld\n", x.val.intg, x.val.frac, unix_timestamp);
            cli_pprintf(LOGGER_SOCKET, "#%d.%u0\n", v.intg, v.frac);
        }
        sock_put_p(LOGGER_SOCKET, PSTR("##"));

        sock_put_done(LOGGER_SOCKET);
        if (sock_do_send(LOGGER_SOCKET)!=W5100_OK) {
            _plog("Temps send fail");
             sock_close(LOGGER_SOCKET);
             return false;
        };

        sock_tx_flush(LOGGER_SOCKET);
        sock_close(LOGGER_SOCKET);

        //_plog("temps logged");
        return true;
    } else {
        _plog("Sock open fail");
    }
    return false;
}


// ============== Timer event handlers ==============

void onTick(void) {
    // 10ms
    ;
}

void on500ms(void) {
    paintLCD();
}

void onSecond(void) {
    lcd_gotoxy(13,0);

    update_RTC();

    if (!ow_cnt || timer.S==55) {
        cli_pwrite(LCD_SOCKET, "=1W");
        ow_cnt = OW_SearchDevices(ow_ids, OW_MAXDEVICES);
    }

    //TODO: telnet non-monitor mode auto exit
    // UART CLI mode auto exit
    if (UART_CLI_countdown) {
        UART_CLI_countdown--;
        if (!UART_CLI_countdown) {
            uart_print_p(csUART_EXIT_CLI);
            uart_print_p(csUART_BOOT_WELCOME);
        }
    } else {
        SFlags &= ~SFL_UARTCMD;
    }

    uint8_t t10s = timer.S % 15;
    if (t10s == 0) {

        if (EFlags & EFL_ETH) {
            if (!(EFlags & EFL_NTP)) timeSync();
        } else {
            cli_pwrite(LCD_SOCKET, "ETH");
            w5100_reinit();
        }

        update_AM2302();
        update_BMP();
        DS18x20_StartMeasure();

    }
    if (t10s == 2) {
        update_DS18x20();
    }

}

void onMinute(void) {

    // update sensor's AVG array
    for(uint8_t i=0;i<SENSOR_COUNT;i++) {
        SensorData *x = &sensors[i];
        update_avg(x);
    }

    if (dt.minute == 0) {
        // force NTP sync
        EFlags &= ~EFL_NTP;
    }

    if (EFlags & EFL_ETH) {
        // send enabled sensors to logger or narodmon.ru
        if ((dt.minute % NARODMON_PERIOD) == 0) udp_log(true);
        delay1ms(5);
        udp_log(false); // to local logger
    }

    //TEST
    SensorData *x = find_sensor_by_id(SENS_DHT_ID_T);
    SensorData *y = find_sensor_by_id(SENS_DHT_ID_RH);

    //for(uint8_t i=0;i<SENS_AVG_SIZE;i++) {
    //    cli_pprintf(UART_SOCKET, "%d x0.1C %d x0.1%\n", x->avg[i], y->avg[i]);
    //}

    SensorValue v = avg_sensor(x);
    SensorValue v2 = avg_sensor(y);
    _logf_p(PSTR("AVG AM2302 values: %3d.%uC%c %u.%u%%%c"), v.intg, v.frac, dir_sensor(x),
                v2.intg, v2.frac, dir_sensor(y));

    x = find_sensor_by_id(SENS_BMP_ID_T);
    y = find_sensor_by_id(SENS_BMP_ID_P);

    //for(uint8_t i=0;i<SENS_AVG_SIZE;i++) {
    //    cli_pprintf(UART_SOCKET, "%d x0.1C %d x0.1%\n", x->avg[i], y->avg[i]);
    //}

    v = avg_sensor(x);
    v2 = avg_sensor(y);
    v2.intg += SENS_BMP_BASE;
    _logf_p(PSTR("AVG BMP085 values: %3d.%uC%c %u.%u mm Hg%c\n"), v.intg, v.frac, dir_sensor(x),
                v2.intg, v2.frac, dir_sensor(y));

}

void onHour(void) {
    ;
}

void setup(void) {

    DDR(RESET_IP_PORT) &= ~(1<<RESET_IP_PIN);

    DDR(LED_PORT) |= (1<<LED_PIN);
    RESET_IP_PORT |= (1<<RESET_IP_PIN); // pull-up

    LED_ON;
    lcd_init(LCD_DISP_ON);
    lcd_puts_p(csWELCOME);
    delay100ms(1);

    check_reset_ip();

    delay100ms(10);
    lcd_clrscr();

    uart_init(57600);
    ir_init();
    i2c_init();
    bmp085_init();

    w5100_reinit();

    init_timer();

    asm("sei");

    ow_cnt = OW_SearchDevices(ow_ids, OW_MAXDEVICES);

    memset(sensors, 0, sizeof(SensorData)*SENSOR_COUNT);
    for (uint8_t i=0;i<4;i++) {
        uint8_t mac[6];
        memcpy_P(mac, _sens_macs[i], 6);
        SensorData *x = add_sensor(SENS_DHT_ID_T+i, mac);
        if (!load_sensor(x)) {
            /*
            if (store_sensor(x)) _plog("New sensor stored");
            else _plog("New sensor NOT stored");
            */
            x->flags |= SENS_FLAG_ON | SENS_FLAG_OK;
            store_sensor(x);
        };
    }
    for (uint8_t i=0;i<ow_cnt;i++) {
        SensorData *x = add_sensor(SENS_1W_MIN_ID+i, &ow_ids[i][1]);
        if (!load_sensor(x))
        {
            x->flags |= SENS_FLAG_ON | SENS_FLAG_OK;
            store_sensor(x);
            _log_byte_mac_p(PSTR(" New 1W Sensor #%u "), x->id, x->mac);
        }
    }

    cli_pprintf(LCD_SOCKET, "%p%d\n", cs1W, ow_cnt);
    if (ow_cnt) SFlags |= SFL_1W;

    bmp085_readEEPROM();
    if (!bmp085_valid()) {
        cli_write_p(LCD_SOCKET, csSENS_P);
        cli_writeln_p(LCD_SOCKET, csFAIL);
        delay100ms(10);
    } else SFlags |= SFL_BMP;

    if (!rtc_init()) {
        //cli_pprintf(LCD_SOCKET, "%p%p\n", csRTC, csFAIL);
        cli_write_p(LCD_SOCKET, csRTC);
        cli_writeln_p(LCD_SOCKET, csFAIL);
        delay100ms(10);
    } else {
        SFlags |= SFL_RTC;
    };
    delay100ms(10);
    lcd_clrscr();
    LED_OFF;
}

void doCommandLine(uint8_t sock, CLIBuffer *cl) {

    if (!cl->len || !cl->buf) return;
    if (cl->len > CLI_MAXCMDSIZE) {
        cli_pprintf(sock, "\nERROR\nCommand to long (%u). Only %u allowed.\n", cl->len, CLI_MAXCMDSIZE);
        return;
    }
    if (cli_command(sock, cl->buf)) cli_pwriteln(sock, "\nOK");
    else cli_pwriteln(sock, "\nERROR");

    return; // don't print prompt
}


void onSerial(void) {

    while (!uart_rx_empty()) {
        if (SFlags & SFL_UARTCMD) UART_CLI_countdown = UART_CLI_TIMEOUT;

        uint8_t c = uart_getch();
        if (c > 0x40 && c < 0x5B) c += 32; // ASCII LoCase
        if (c == 9) c = ' '; // Tab -> space
        switch(c) {
            case 8:
                if (cli.len) {
                    cli.len--;
                    cli.buf[cli.len]=0;
                    uart_putch(0x08);
                    uart_tx_flush();
                }
                break;

            case '\n':
                if (SFlags & SFL_UARTCMD) {
                    cli.buf[cli.len]=0;

                    uart_putch('\n');
                    doCommandLine(UART_SOCKET, &cli);
                    cli.len = 0;

                    uart_tx_flush();
                } else {
                    SFlags |= SFL_UARTCMD;
                    UART_CLI_countdown = UART_CLI_TIMEOUT;
                    cli_writeln_p(UART_SOCKET, csCLI_WELCOME);
                }
                if (UART_CLI_countdown) cli_prompt(UART_SOCKET);
                uart_tx_flush();
                break;

            case '\r':
                break;

            default:
                if (cli.len <= CLI_MAXCMDSIZE) {
                    if (c >= ' ') {
                        uart_putch(c);
                        cli.buf[cli.len++] = c;
                    }
                }
        }
    }

}


void onTelnetRecv(uint16_t rsz) {

    while(rsz--) {
        uint8_t c = sock_getch(TELNET_SOCKET);
        if (c > 0x40 && c < 0x5B) c += 32; // ASCII LoCase
        if (c == 9) c = ' '; // Tab -> space
        if (c == 0x0D) continue;
        if (c == 0x0A) {
            EFlags &= ~EFL_TELMON;
            break;
        }
        if (ecli.len <= CLI_MAXCMDSIZE) {
            ecli.buf[ecli.len] = c;
        }
        ecli.len++;
        ecli.buf[ecli.len] = 0;
    }
    if (ecli.len) {
        doCommandLine(TELNET_SOCKET, &ecli);

        ecli.len = 0;
    }
    return; // no data, no prompt print
}

void onTelnet(void) {
    uint16_t rsz;

    switch (sock_status(TELNET_SOCKET)) {
        case W5100_SKT_SR_CLOSED:
            //_log_p(PSTR("Open TCP socket "));

            EFlags &= ~EFL_TELNET;
            if (sock_open(TELNET_SOCKET, W5100_SKT_MR_TCP, TELNET_PORT) != W5100_FAIL) {
                //_log_p(PSTR("Telnet ready"));

                sock_listen(TELNET_SOCKET);
            }
            /*else {
                _log_p(PSTR("fail."));
            }*/
            break;

        case W5100_SKT_SR_ESTABLISHED:
            if ((EFlags & EFL_TELNET) == 0) {
                //_log_p(PSTR("CONNECTED"));

                cli_writeln_p(TELNET_SOCKET, csCLI_WELCOME);
                cli_prompt(TELNET_SOCKET);
                EFlags |= EFL_TELNET;
                ecli.len = 0;
            }
            if ((rsz=sock_recv_size(TELNET_SOCKET))!=0) {
                onTelnetRecv(rsz);
                if (!(EFlags & EFL_TELMON))
                    cli_prompt(TELNET_SOCKET);
            }
            break;

        case W5100_SKT_SR_FIN_WAIT:
        case W5100_SKT_SR_CLOSING:
        case W5100_SKT_SR_TIME_WAIT:
        case W5100_SKT_SR_CLOSE_WAIT:
        case W5100_SKT_SR_LAST_ACK:
            //_log_p(PSTR("*CLOSING*"));

            sock_close(TELNET_SOCKET);
            EFlags &= ~EFL_TELNET;
            break;

        default:
            EFlags &= ~EFL_TELNET;
            break;
    }
}

void onIR(void) {

    lcd_gotoxy(13, 0);
    cli_pwrite(LCD_SOCKET, " IR");

    uint32_t c = ir_code();
    uint8_t *b = (uint8_t*)&c; //ir_code_bytes();
    uint16_t *h = (uint16_t*)&c;
    if (ir_isNEC8()) {

                //TEST
                cli_pprintf(UART_SOCKET, "NEC[%02X]: %02X BITS: %08b %08b %08b %08b\n",
                            b[3], b[1],
                            b[3], b[2], b[1], b[0]);

                if (ir_isRepeat()) {
                    uart_putch('*');
                }
                if (ir_NEC8code()==0xF0) {
                    uart_pprint("\nSending 0xF0...\n");
                    ir_sendNEC8(0, 0xF0, 0);
                    while (!ir_sendComplete()) delay10ms(1);
                }
            } else {
                //uint32_t c = ir_code();
                //uint8_t *b = ir_code_bytes();
                //TEST
                cli_pprintf(UART_SOCKET, "IR: %02X%02X%02X%02X XOR %02X%02X BITS %08b %08b %08b %08b\n",
                            b[3], b[2], b[1], b[0],
                            b[0]^b[1], b[2]^b[3],
                            b[3], b[2], b[1], b[0]);

                if ((*h & 0xF3FF) == 0x80D7) {
                    uint8_t ip[4] = {10,0,0,255};
                    uint8_t mac[6] = {0xf0, 0xde, 0xf1, 0x5c, 0xd1, 0xb8};
                    WOL(COMMON_SOCKET, ip, mac);
                }

            }
    //TEST UDP
/*
    uint8_t lirc_ip[4];
    uint16_t lirc_p = eeprom_read_word(&eeLIRC_PORT);
    if (hostbyname_e(eeLIRC, lirc_ip))
        if (sock_open(2, W5100_SKT_MR_UDP, lirc_p) != W5100_FAIL) {
            sock_set_dest(2, lirc_ip, lirc_p);

            //cli_pprintf(2, "00000000%02X%02X%02X%02X\n", b[3], b[2], b[1], b[0]);
            //sock_send(2, b, 4);
            sock_put(2, &b[3], 1);
            sock_put(2, &b[2], 1);
            sock_put(2, &b[1], 1);
            sock_put(2, &b[0], 1);
            _logf_p(PSTR("IR send %02X%02X%02X%02X"), b[3], b[2], b[1], b[0]);

            sock_put_done(2);
            sock_do_send(2);
            //sock_tx_flush(2);
            sock_close(2);
        }
*/
}

// *************************************************************************
void main(void) {
    uint8_t buf[6];
    wdt_disable();

    setup();

    //cli_pprintf(UART_SOCKET, "%p%p\nMAC: ", csWELCOME, csUART_BOOT_WELCOME);
    cli_write_p(UART_SOCKET, csWELCOME);
    cli_writeln_p(UART_SOCKET, csUART_BOOT_WELCOME);

    //TEST
    for(uint8_t i=0;i<6;i++) buf[i] = W51_read(W5100_SHAR+i);
    cli_write_mac(UART_SOCKET, buf);

    uart_pprint("\nIP: ");
    for(uint8_t i=0;i<4;i++) buf[i] = W51_read(W5100_SIPR+i);
    cli_write_ip(UART_SOCKET, buf);

    uart_pprint("\nNetmask: ");
    for(uint8_t i=0;i<4;i++) buf[i] = W51_read(W5100_SUBR+i);

    uart_pprint("\nGW: ");
    for(uint8_t i=0;i<4;i++) buf[i] = W51_read(W5100_GAR+i);
    cli_write_ip(UART_SOCKET, buf);

    uart_putch('\n');

    //===


    rtc_time(&dt);
    if (dt.year == 0) {
        rtc_set_date(14,1,1);
        rtc_set_time(0,0,0);
    }

    //cli_pwrite(UART_SOCKET, "RTC: ");
    //cli_write_datetime(UART_SOCKET, &dt);
    //cli_pwriteln(UART_SOCKET, "");

    update_AM2302();
    update_BMP();
    update_DS18x20();
    ir_start();

    LED_ON;
    // MAIN LOOP
    for(;;) {
        if (!uart_rx_empty()) onSerial();
        onTelnet();

        if (ir_available()) {
            onIR();
            ir_start();
        }

        if (timer.Flags & TFLAGS_10MS) {
            onTick();
            if (timer.Flags & TFLAGS_500MS) on500ms();
            if (timer.Flags & TFLAGS_SEC) onSecond();
            if (timer.Flags & TFLAGS_MIN) onMinute();
            if (timer.Flags & TFLAGS_HOUR) onHour();

            timer.Flags &= ~(TFLAGS_10MS | TFLAGS_500MS | TFLAGS_SEC | TFLAGS_MIN | TFLAGS_HOUR);
        }

    }


}
