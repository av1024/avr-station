#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""naronmon.ru to rrd database logger
"""

import sys
import os
import signal
import atexit
import logging
from argparse import ArgumentParser  # python 2.7+
from pyrrd.rrd import DataSource, RRA, RRD
#from pyrrd.backend import bindings
import time
import socket

from utils import setupcon


log = None
LOG_PORT = 8283
LOG_IP = "0.0.0.0"

TAM_MAC =  "414d23020001"
RHAM_MAC = "414d23020002"
TBM_MAC =  "424d50850001"
PBM_MAC =  "424d50850002"
T1W_out =  "37b5c7020000"
T1W_heat = "41abc7020000"

MAC_Names = {
    TAM_MAC : "Tam",
    RHAM_MAC : "RHam",
    TBM_MAC : "Tbm",
    PBM_MAC : "Pbm",
    T1W_out : "Tout",
    T1W_heat : "Theat"
}

def sig_name(signalnum):
    # debug: get signal name:
    sn = ""
    for x in [y for y in dir(signal) if y.startswith('SIG')]:
        sv = getattr(signal, x)
        if sv == signalnum:
            sn = x
            break
    else:
        sn = "%d" % signalnum

    return sn

class RRDLogDaemon(object):
    def __init__(self, args, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
        self._daemon = not args.no_daemon
        self._pidfile = args.pid_file
        self._exit = False
        self._pid = os.getpid()
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.dry_run = args.dry_run  # don't really send anything
        self.rrd = None

        log.debug(u"Narodmon UDP logger. Daemonize: %s, PID file '%s', pid: %d",
                  self._daemon, self._pidfile, self._pid)

        self.port = args.port

        signal.signal(signal.SIGTERM, self.term_handler)
        signal.signal(signal.SIGUSR1, self.usr1_handler)  # for reload

        if self.dry_run:
            log.info("DRY RUN. No data will be stored")

    def start(self):
        if self._daemon:
            try:
                with open(self._pidfile, 'r') as pf:
                    pid = int(pf.read().strip())
            except IOError:
                pid = None

            if pid:
                try:
                    with open("/proc/%d/status" % pid, 'r') as prc:
                        log.warning(u"Daemon already running. PID: %d", pid)
                        sys.exit(1)
                except IOError:
                    pass

            self.daemonize()

            log.debug("Daemon pid: %s", self._pid)

        self.run()

    def stop(self):
        try:
            with open(self._pidfile, 'r') as pf:
                pid = int(pf.read().strip())
        except IOError:
            pid = None

        try:
            while 1:
                os.kill(pid, signal.SIGTERM)
                time.sleep(0.1)
        except OSError as err:
            err = str(err)
            if err.find("such process") > 0:
                if os.path.exists(self._pidfile):
                    os.remove(self._pidfile)
            else:
                print err
                sys.exit(1)

    def restart(self):
        self.stop()
        self.start()

    def status(self):
        """
        Returns True and set self._pid to actual pid if daemon is running
        """
        try:
            with open(self._pidfile, 'r') as pf:
                pid = int(pf.read().strip())
        except IOError:
            pid = None

        if pid:
            try:
                with open("/proc/%d/status" % pid, 'r') as prc:
                    self._pid = pid
                    return True
            except IOError:
                pass

        return False

    def daemonize(self):
        """double fork for daemonize"""

        log.debug("Going to background")
        # fork #1
        try:
            pid = os.fork()
            if pid > 0:
                # exit 1-st parent
                sys.exit(0)
        except OSError as e:
            try:
                log.error("fork #1 failed: %d (%s)", e.errno, e.strerror)
            except:
                pass
            sys.stderr.write("fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

        # decouple from parent
        os.chdir("/")
        os.setsid()
        os.umask(0)

        # fork #2
        try:
            pid = os.fork()
            if pid > 0:
                # exit from 2-nd parent
                sys.exit(0)

        except OSError as e:
            try:
                log.error("fork #2 failed: %d (%s)", e.errno, e.strerror)
            except:
                pass
            sys.stderr.write("fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

        # redirect file descriptors
        #log.debug("redirect std files")
        try:
            sys.stdout.flush()
            sys.stderr.flush()
            si = file(self.stdin, 'r')
            so = file(self.stdout, 'a+')
            se = file(self.stderr, 'a+', 0)
            os.dup2(si.fileno(), sys.stdin.fileno())
            os.dup2(so.fileno(), sys.stdout.fileno())
            os.dup2(se.fileno(), sys.stderr.fileno())
        except:
            log.exception("Failed to redirect streams")

        # write pid file
        atexit.register(self.delpid)
        pid = str(os.getpid())
        #log.debug("Daemon pid: %s", pid)
        with open(self._pidfile, "w+") as f:
            f.write("%s\n" % pid)
        self._pid = pid

    def run(self):
        log.debug(u"Narodmon logger run.")
        while not self._exit:
            log.debug("run() loop...")
            try:
                self.run_app()
            except KeyboardInterrupt:
                if not self._daemon:
                    break
            except Exception as e:
                log.critical("EXCEPTION: %s", e)
                time.sleep(5)

    def delpid(self):
        log.debug(u"@exit: delete pid file")
        os.remove(self._pidfile)

    def usr1_handler(self, signalnum, frame):
        log.debug("Signal: %s", sig_name(signalnum))

    def term_handler(self, signalnum, frame):
        log.debug("Signal: %s", sig_name(signalnum))
        self._exit = True

# -------------------------------------------------

    def do_data(self, data):
            tt = time.time()
            _macs = []
            _vals = []

            if not data.startswith("#"):
                log.warn("Invalid data format")
                return

            arr = data.split('\n')
            log.debug("Found %s item(s)", len(arr)-2)

            for s in arr[1:]:
                if not s or s.startswith("##"):
                    break
                sens = s[1:].split('#')
                try:
                    mac = sens[0]
                    value = float(sens[1])
                    x0 = ""
                    ts = 0
                    if len(sens)==3:
                        x0 = sens[2]

                    if x0:
                        ts = int(x0)

                    log.debug("Sensor %s, value: %s, timestamp: %s (%s)", mac, value, ts, time.asctime(time.localtime(ts)))

                    try:

                        for _mac, _name in MAC_Names.items():
                            if mac == _mac:
                                _macs.append(_name)
                                _vals.append(value)

                    except:
                        log.exception("RRD not buffered. ")

                except IndexError:
                    log.warn("Inalid sensor format in `%s`", s)
            if _macs:
                try:
                    #log.debug("To log: %d @ %s", tt, _vals)
                    self.rrd.bufferValue(long(tt), *_vals);
                    self.rrd.update(template=':'.join(_macs), debug=False, dryRun=self.dry_run)
                except:
                    log.exception("RRD not updated")

    def serve_udp(self):

        if not self.rrd:
            log.error("RRD database not open")
            self._exit = True
            return

        log.info("Start UDP server...")
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((LOG_IP, self.port))

        while not self._exit:
            try:
                data, addr = sock.recvfrom(1024)
                log.debug("== Received data ==")
                log.debug("%r", data)
                self.do_data(data)

            except KeyboardInterrupt:
                log.debug("Stopped")
                self._exit = True
                break
            except Exception:
                log.exception("EXC")
                break
        sock.close()

    def run_app(self):
        """All app logic here
        """

        log.info("app started.")
        try:
            self.rrd = RRD(args.rrd, mode="r")
            log.debug("RRD: %s", self.rrd)
        except:
            log.exception("RRD database not open.")
            raise

        try:
            self.serve_udp()
        except:
            log.exception("UDP serve fail")
            raise


def makeRRD(fname):
    log.info("Create new RRD database `%s`", fname)
    rrd = RRD(filename=fname, start='now-7d', step=60,
            ds=[
                DataSource("Tam", "GAUGE", 900, 10, 50),
                DataSource("RHam", "GAUGE", 900, 0, 100),
                DataSource("Tbm", "GAUGE", 900, 10, 50),
                DataSource("Pbm", "GAUGE", 1800, 600, 800),
                DataSource("Tout", "GAUGE", 900, -50, 50),
                DataSource("Theat", "GAUGE", 900, 0, 95)
                ],
            rra=[
                RRA(cf="AVERAGE", xff=0.5, steps=1, rows=60),    # 1 hour every minute
                RRA(cf="AVERAGE", xff=0.5, steps=10, rows=144),  # 1 day every 10 minutes
                RRA(cf="AVERAGE", xff=0.5, steps=60, rows=24*7), # 1 week every hour
                RRA(cf="AVERAGE", xff=0.5, steps=60*4, rows=366*6), # 1 year every 4 hours
                RRA(cf="AVERAGE", xff=0.5, steps=60*12, rows=366*10*2), # 10 years every 12 hours

                RRA(cf="MIN", xff=0.5, steps=60, rows=24), # min per day, every hour
                RRA(cf="MAX", xff=0.5, steps=60, rows=24), # max per day, every hour

                RRA(cf="MIN", xff=0.5, steps=60*4, rows=366*6), # 1 year, every 6 hours
                RRA(cf="MAX", xff=0.5, steps=60*4, rows=366*6),

                RRA(cf="MIN", xff=0.5, steps=60*12, rows=366*10*2), # 10 years, every 12 hours
                RRA(cf="MAX", xff=0.5, steps=60*12, rows=366*10*2)
                ])
            #backend=bindings)

    return rrd

def import_fetch(args, rrd):
    """Import into RRD text file, previously exported by `rrdtool fetch`

    File format:

      1: DS names, deparated by multiple spaces
      2: Empty line
      3..: Timestamp: DS_value1 DS_value2 ...

    Note! Decimal separator on DS_value may not be '.'

    """
    log.info("Import series from `%s`", args.fetch_import)
    tpl = None
    i = 0
    try:
        with open(args.fetch_import, 'Ur') as f:
            for s in f.readlines():
                s = s.strip()
                if not s:
                    continue
                if not tpl:
                    while "  " in s: s = s.replace("  ", " ")
                    tpl = ":".join(s.strip(" ").split(" "))
                    log.info("Template: '%s'", tpl)
                else:
                    cells = s.split(" ")
                    try:
                        tt = float(cells[0][:-1])
                        cells = [x.replace(',','.') for x in cells[1:]]
                        log.debug("Buffer line %d %s", tt, cells)
                        i += 1

                        if not args.dry_run:
                            rrd.bufferValue(tt, *cells)

                        if (i>100):
                            log.info("Next 100 rows%s", " (DRY RUN)" if args.dry_run else "")
                            rrd.update(debug=False, dryRun=args.dry_run, template=tpl)
                            i = 0
                    except:
                        log.exception("Invalid cell format")
                        break
            if i:
                log.info("Update last %d row(s)", i)
                rrd.update(debug=False, dryRun=args.dry_run, template=tpl)

    except:
        log.exception("Import fail.")

def main(args):

    if not os.path.exists(args.rrd):
        if args.dry_run:
            log.warn("DRY RUN. Database not created.")
        else:
            log.info("")
            rrd = makeRRD(args.rrd)
            rrd.create()
            rrd = None

    if (args.fetch_import):
        rrd = RRD(args.rrd, mode="r")
        import_fetch(args, rrd)
        return

    snd = RRDLogDaemon(args)
    if args.kill:
        snd.stop()
    elif args.restart:
        snd.restart()
    elif args.status:
        if snd.status():
            log.debug("Daemon status: running, PID: %s", snd._pid)
            sys.exit(0)
        log.debug("Daemon is not running.")
        sys.exit(1)
    else:
        snd.start()

    #serve_udp(rrd)


if __name__ == "__main__":
    parser = ArgumentParser(description="Sensor receiver/logger. RRD db version")

    #TODO: add other options here

    setupcon.add_log_options(parser)

    parser.add_argument("--rrd", metavar="FILE", required=True, type=str, help="RRD database filename")
    parser.add_argument("--fetch-import", metavar="FILE", help="Import text FILE, previously exported by rrdtool fetch.")
    #TODO: add other options here
    parser.add_argument("-p", "--pid-file", help="Pid file (%(default)s)",
                        default=os.path.join("/var/run", os.path.basename(__file__)) + ".pid")
    parser.add_argument("-D", "--no-daemon", action="store_true", help="Does not daemonize")
    parser.add_argument("-c", "--config", help="Config file")
    parser.add_argument("--kill", action="store_true", help="Stop daemon")
    parser.add_argument("--restart", action="store_true", help="Restart daemon")
    parser.add_argument("--status", action="store_true", help="Check daemon status")
    parser.add_argument("-P", "--port", help="Logger UDP port", default=LOG_PORT)

    parser.add_argument("-n", "--dry_run", action="store_true", help="Do not send anything to narodmon")

    args = parser.parse_args()
    log = logging.getLogger()

    setupcon.setup_logging(args, '')

    # === end setup logging ==
    main(args)
