#!/usr/bin/env rrdcgi

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Temperature graph</title>
</head>
<body>
<h2>Daily temperature and humidity graph</h2>

<RRD::GRAPH png/daily_trh.png --lazy --slope-mode -h 1024 -w 1280 -A -Y --start now-1d
    --imginfo '<IMG SRC="png/%s" WIDTH="%lu" height="%lu" />'
    DEF:T1=db.rrd:Tam:AVERAGE
    DEF:T2=db.rrd:Tbm:AVERAGE
    DEF:RH=db.rrd:RHam:AVERAGE
     "AREA:RH#E0F0F0F0:RH%"
     LINE:RH#A0C0C0
     LINE:T2#E000E0:T2
     LINE:T1#E00000:T1
     >

</body>
</html>
