#!/bin/bash

rrdtool graph thermo-rh-12h.png \
    --start now-12hours --end now \
    -h 800 -w 1280 -A -Y -E \
    -t "12 hours temperatures" \
    DEF:T1=db.rrd:Tam:AVERAGE \
    DEF:T2=db.rrd:Tbm:AVERAGE \
    DEF:T3=db.rrd:Tout:AVERAGE \
    DEF:T4=db.rrd:Theat:AVERAGE \
    DEF:RH=db.rrd:RHam:AVERAGE \
     "AREA:RH#E0F0F0F0:RH%" \
     LINE:RH#A0C0C0 \
     LINE:T2#E000E0:"T BMP" \
     LINE:T1#E08000:"T AM" \
     LINE2:T3#8080FF:"T out" \
     LINE:T4#C00000:"T heat"

rrdtool graph thermo-rh-day.png \
    --start now-1day --end now \
    -h 800 -w 1280 -A -Y -E \
    -t "1 day temperatures" \
    DEF:T1=db.rrd:Tam:AVERAGE \
    DEF:T2=db.rrd:Tbm:AVERAGE \
    DEF:T3=db.rrd:Tout:AVERAGE \
    DEF:T4=db.rrd:Theat:AVERAGE \
    DEF:RH=db.rrd:RHam:AVERAGE \
     "AREA:RH#E0F0F0F0:RH%" \
     LINE:RH#A0C0C0 \
     LINE:T2#E000E0:"T BMP" \
     LINE:T1#E08000:"T AM" \
     LINE2:T3#8080FF:"T out" \
     LINE:T4#C00000:"T heat"

rrdtool graph thermo-rh-week.png \
    --start now-7day --end now \
    -h 800 -w 1280 -A -Y -E \
    -t "1 week temperatures" \
    DEF:T1=db.rrd:Tam:AVERAGE \
    DEF:T2=db.rrd:Tbm:AVERAGE \
    DEF:T3=db.rrd:Tout:AVERAGE \
    DEF:T4=db.rrd:Theat:AVERAGE \
    DEF:RH=db.rrd:RHam:AVERAGE \
     "AREA:RH#E0F0F0F0:RH%" \
     LINE:RH#A0C0C0 \
     LINE:T2#E000E0:"T BMP" \
     LINE:T1#E08000:"T AM" \
     LINE2:T3#8080FF:"T out" \
     LINE:T4#C00000:"T heat"

rrdtool graph thermo-rh-month.png \
    --start now-1month --end now \
    -h 800 -w 1280 -A -Y -E \
    -t "1 month temperatures" \
    DEF:T1=db.rrd:Tam:AVERAGE \
    DEF:T2=db.rrd:Tbm:AVERAGE \
    DEF:T3=db.rrd:Tout:AVERAGE \
    DEF:T4=db.rrd:Theat:AVERAGE \
    DEF:RH=db.rrd:RHam:AVERAGE \
     "AREA:RH#E0F0F0F0:RH%" \
     LINE:RH#A0C0C0 \
     LINE:T2#E000E0:"T BMP" \
     LINE:T1#E08000:"T AM" \
     LINE2:T3#8080FF:"T out" \
     LINE:T4#C00000:"T heat"

rrdtool graph pressure-day.png \
    --start now-1day \
    -h 800 -w 1280 \
    -A -Y \
    -t "1 day pressure" \
    DEF:P=db.rrd:Pbm:AVERAGE \
    CDEF:pn=P,UN,747,P,IF \
    CDEF:high=750,P,GT,UNKN,P,IF \
    CDEF:low=744,P,LT,UNKN,P,IF \
     HRULE:747#808080 \
     LINE2:P#00C080 \
     LINE2:high#C08080 \
     LINE2:low#0080C0

rrdtool graph pressure-week.png \
    --start now-7day \
    -h 800 -w 1280 \
    -A -Y \
    -t "1 week pressure" \
    DEF:P=db.rrd:Pbm:AVERAGE \
    CDEF:pn=P,UN,747,P,IF \
    CDEF:high=750,P,GT,UNKN,P,IF \
    CDEF:low=744,P,LT,UNKN,P,IF \
     HRULE:747#808080 \
     LINE2:P#00C080 \
     LINE2:high#C08080 \
     LINE2:low#0080C0

rrdtool graph pressure-month.png \
    --start now-1month \
    -h 800 -w 1280 \
    -A -Y \
    -t "1 month pressure" \
    DEF:P=db.rrd:Pbm:AVERAGE \
    CDEF:pn=P,UN,747,P,IF \
    CDEF:high=750,P,GT,UNKN,P,IF \
    CDEF:low=744,P,LT,UNKN,P,IF \
     HRULE:747#808080 \
     LINE2:P#00C080 \
     LINE2:high#C08080 \
     LINE2:low#0080C0


rrdtool graph all-day.png \
    --start now-1day \
    -h 800 -w 1280 -A -Y -E \
    --right-axis 1:730 --right-axis-label pressure \
    -t "1 day ALL" \
    DEF:T1=db.rrd:Tam:AVERAGE \
    DEF:T2=db.rrd:Tbm:AVERAGE \
    DEF:T3=db.rrd:Tout:AVERAGE \
    DEF:T4=db.rrd:Theat:AVERAGE \
    DEF:RH=db.rrd:RHam:AVERAGE \
    DEF:P=db.rrd:Pbm:AVERAGE \
    CDEF:pp=P,730,- \
     "AREA:RH#E0F0F0F0:RH%" \
     LINE:RH#A0C0C0 \
     LINE:T2#E000E0:"Temp BMP" \
     LINE:T1#A000A0:"Temp AM" \
     LINE2:T3#88F:"Temp Out" \
     LINE:T4#D00:"Temp heat" \
     LINE2:pp#80C080:Pressure

rrdtool graph all-week.png \
    --start now-7day \
    -h 800 -w 1280 -A -Y -E \
    -t "1 week ALL" \
    --right-axis 1:730 --right-axis-label pressure \
    DEF:T1=db.rrd:Tam:AVERAGE \
    DEF:T2=db.rrd:Tbm:AVERAGE \
    DEF:T3=db.rrd:Tout:AVERAGE \
    DEF:T4=db.rrd:Theat:AVERAGE \
    DEF:RH=db.rrd:RHam:AVERAGE \
    DEF:P=db.rrd:Pbm:AVERAGE \
    CDEF:pp=P,730,- \
     "AREA:RH#E0F0F0F0:RH%" \
     LINE:RH#A0C0C0 \
     LINE:T2#E000E0:"Temp BMP" \
     LINE:T1#A000A0:"Temp AM" \
     LINE2:T3#88F:"Temp Out" \
     LINE:T4#D00:"Temp heat" \
     LINE2:pp#80C080:Pressure

rrdtool graph all-month.png \
    --start now-1month \
    -h 800 -w 1280 -A -Y -E \
    --right-axis 1:730 --right-axis-label pressure \
    -t "1 month ALL" \
    DEF:T1=db.rrd:Tam:AVERAGE \
    DEF:T2=db.rrd:Tbm:AVERAGE \
    DEF:T3=db.rrd:Tout:AVERAGE \
    DEF:T4=db.rrd:Theat:AVERAGE \
    DEF:RH=db.rrd:RHam:AVERAGE \
    DEF:P=db.rrd:Pbm:AVERAGE \
    CDEF:pp=P,730,- \
     "AREA:RH#E0F0F0F0:RH%" \
     LINE:RH#A0C0C0 \
     LINE:T2#E000E0:"Temp BMP" \
     LINE:T1#A000A0:"Temp AM" \
     LINE2:T3#88F:"Temp Out" \
     LINE:T4#D00:"Temp heat" \
     LINE2:pp#80C080:Pressure
