#!/usr/bin/env python
# -*- coding: utf-8 -*-

#import sys
#import os
import logging
from argparse import ArgumentParser  # python 2.7+
from utils import setupcon
import time

import socket

log = None
LOG_PORT = 8283
LOG_IP = "0.0.0.0"


def do_data(data):
    if not data.startswith("#"):
        log.warn("Invalid dat format")
        return

    arr = data.split('\n')
    log.debug("Found %s item(s)", len(arr)-2)
    for s in arr[1:]:
        if not s or s.startswith("##"):
            break
        sens = s[1:].split('#')
        try:
            mac = sens[0]
            value = sens[1]
            x0 = ""
            ts = 0
            if len(sens)==3:
                x0 = sens[2]

            if x0:
                ts = int(x0)

            log.info("Sensor %s, value: %s, timestamp: %s (%s)", mac, value, ts, time.asctime(time.localtime(ts)))

        except IndexError:
            log.warn("Inalid sensor format in `%s`", s)


def serve_udp():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((LOG_IP, LOG_PORT))

    while True:
        try:
            data, addr = sock.recvfrom(1024)
            log.info("Received data")
            log.debug("%r", data)
            do_data(data)
        except KeyboardInterrupt:
            log.debug("Stopped")
            break
        except Exception:
            log.exception("")


def main(args):
    #TODO: parse args

    serve_udp()


if __name__ == "__main__":
    parser = ArgumentParser(description="Narodmon.ru like UDP logger")

    #TODO: add other options here

    setupcon.add_log_options(parser)

    #TODO: add other options here

    args = parser.parse_args()
    log = logging.getLogger()

    setupcon.setup_logging(args, '')

    #  ..test..
    log.info('narodmon.ru like test listener')
    log.debug('debug logging active')

    # === end setup logging ==
    main(args)
