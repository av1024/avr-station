# -*- coding: utf-8 -*-
"""
Colored console logging and utilites.
=====================================
"""
import sys
import os
import codecs
import copy
import logging
import logging.handlers
from time import time
from functools import wraps
import inspect

ansi = not sys.platform.lower().startswith("win")

class WrapsFormatter(logging.Formatter):
    """Try pass ``@wraps`` name hiding

    Usage (in own decorator:)::

        def my_decorator(param=val):
            def deco(fn):
                @wraps
                def wrapped(*args, **kwargs):
                    #...
                    logging.log("Some text", extra={'_wraps_name': fn.__name__})
                    #...

    .. note::
        Hardcoded stack offest for override caller detection
    """
    def format(self, record):
        if hasattr(record, "_wraps_name"):
            record.funcName = record._wraps_name
            st = inspect.stack()[10]  #!!! Hardcoded !!!
            record.funcName = st[3]
            record.lineno = st[2]
            record.pathname = st[1]
            record.module = os.path.splitext(os.path.basename(st[1]))[0]

        return super(WrapsFormatter, self).format(record)

_log = logging.getLogger(__file__)

#TODO: add global dict for timeit totals/items

_timeit_buf = []

def timeit(log_level=logging.DEBUG, log_args=False):
    """Function timing decorator.
    Log execution time in ms into logger "timeit"

    :param int log_level: Logging level (:mod:`logging.LOG_LEVEL`) to be used
    :param bool log_args: Log function argumets

    .. note::
        added extra params for custom formaftter used when :func:`basicConfig()` called
    """

    def deco(fn):
        # initialization. Called once per decorator assignment
        setattr(fn, "__min", 0)
        setattr(fn, "__max", 0)
        setattr(fn, "__sum", 0)
        setattr(fn, "__cnt", 0)
        _timeit_buf.append(fn)
        @wraps(fn)
        def wrapped(*args, **kwargs):
            if _log.isEnabledFor(log_level):
                s0 = ""
                if log_args:
                    s0 = u", ARGS: %s, KWARGS: %s" % (args, kwargs)
                t0 = time()
                r = fn(*args, **kwargs)
                t1 = (time() - t0)*1000

                fn.__sum += t1
                fn.__cnt += 1
                if t1 > fn.__max or fn.__max==0: fn.__max = t1
                if t1 < fn.__min or fn.__min==0: fn.__min = t1

                #
                _log.log(log_level, u"TimeIt %s: %0.3fms (min: %0.3fms, max: %0.3fms, avg: %0.3fms, ncalls: %d)%s",
                         fn.__name__, t1, fn.__min, fn.__max, fn.__sum/fn.__cnt, fn.__cnt,
                         s0, extra={'_wraps_name': fn.__name__})
                return r
            else:
                return fn(*args, **kwargs)
        return wrapped
    return deco

def timeit0(fn):
    """Empty timing decorator"""

    # initialization. Called once per decorator assignment
    setattr(fn, "__min", 0)
    setattr(fn, "__max", 0)
    setattr(fn, "__sum", 0)
    setattr(fn, "__cnt", 0)
    _timeit_buf.append(fn)
    @wraps(fn)
    def wrapped(*args, **kwargs):
        if _log.isEnabledFor(logging.DEBUG):
            t0 = time()
            r = fn(*args, **kwargs)
            t1 = (time() - t0)*1000

            fn.__sum += t1
            fn.__cnt += 1
            if t1 > fn.__max or fn.__max==0: fn.__max = t1
            if t1 < fn.__min or fn.__min==0: fn.__min = t1

            _log.debug(u"TimeIt %s: %0.3fms (min: %0.3fms, max: %0.3fms, avg: %0.3fms, ncalls: %d)",
                     fn.__name__, t1, fn.__min, fn.__max, fn.__sum/fn.__cnt, fn.__cnt,
                     extra={'_wraps_name': fn.__name__})
            return r
        else:
            return fn(*args, **kwargs)
    return wrapped

def timeit_stats(log_level=logging.INFO):
    """
    Print `timeit` summary statistics into log
    """
    _log.log(log_level, "=== Timeit summary ===")
    for fn in _timeit_buf:
        _log.log(log_level, u"%s(): min: %0.3fms, max: %0.3fms, avg: %0.3fms, ncalls: %d",
                 fn.__name__, fn.__min, fn.__max, fn.__sum/(fn.__cnt or 1.0), fn.__cnt)


def setup_console(sys_enc='utf-8', use_colorama=True):
    """
    Set sys.defaultencoding to `sys_enc` and update stdout/stderr writers to corresponding encoding

    .. note:: For Win32 the OEM console encoding will be used istead of `sys_enc`
    """
    global ansi
    reload(sys)
    try:
        if sys.platform.startswith("win"):
            import ctypes
            enc = "cp%d" % ctypes.windll.kernel32.GetOEMCP()
        else:
            enc = (sys.stdout.encoding if sys.stdout.isatty() else
                        sys.stderr.encoding if sys.stderr.isatty() else
                            sys.getfilesystemencoding() or sys_enc)

        if sys.getdefaultencoding().lower() != sys_enc.lower():
            sys.setdefaultencoding(sys_enc)

        if sys.stdout.isatty() and sys.stdout.encoding != enc:
            sys.stdout = codecs.getwriter(enc)(sys.stdout, 'replace')

        if sys.stderr.isatty() and sys.stderr.encoding != enc:
            sys.stderr = codecs.getwriter(enc)(sys.stderr, 'replace')

        if use_colorama and sys.platform.startswith("win"):
            try:
                from colorama import init
                init()
                ansi = True
            except:
                pass

    except:
        pass


class ColoredHandler(logging.StreamHandler):
    """
    Colored wrapper for console logging.

    Overriden StreamHandler with ANSI formatting codes.

    :param dict colors: Extra optional parameter with overriden colors.

      Contains { level: ANSI_STR }, ``ANSI_STR`` applied if ``level`` >= logging level

    Property ``colors`` can be changed after handler creation::

        for h in logging.getLogger().handlers:
            if isinstance(h, ColoredHandler):
                h.colors[5] = '\\x1b[34;1m' # Threading "SUB_DEBUG" level as bold blue

    ANSI_STR like:
      * ``"\\x1b[31m"`` - red text color
      * ``"\\x1b[33;1m"`` - bold/bright yellow color
      * ``"\\x1b[0m"`` - RESET to default console color

    ANSI COLORS (FG/BG):

      * 30/40: black
      * 31/41: red
      * 32/42: breen
      * 33/43: brown/yellow
      * 34/44: blue
      * 35/45: magenta
      * 36/46: cyan
      * 37/47: white/grey
    """
    def __init__(self, *args, **kwargs):
        logging.StreamHandler.__init__(self, *args, **kwargs)
        #super(ColoredHandler, self).__init__(*args, **kwargs)
        self.is_tty = False
        self.colors = kwargs.get('colors', {
            50: '\x1b[31;1m', # red/bold
            40: '\x1b[31m',   # red
            30: '\x1b[33m',   # brown
            20: '\x1b[32m',   # green
            10: '\x1b[35m',   # magenta
            0:  '\x1b[0m'     # default/grey
        })
        try:
            self.is_tty = self.stream.isatty()
        except:
            pass

    def emit(self, record):
        """
        Overriden method for colorize tty output
        """
        # Need to make a actual copy of the record
        # to prevent altering the message for other loggers
        if self.is_tty:
            myrecord = copy.copy(record)
            levelno = myrecord.levelno

            for lvl in sorted(self.colors, reverse=True):
                if levelno >= lvl:
                    color = self.colors[lvl]
                    break
            else:
                color = self.colors.get(0, '\x1b[0m')

            myrecord.msg = (u"%s%s%s" % (color, myrecord.msg, '\x1b[0m')).encode('utf-8')  # normal
            logging.StreamHandler.emit(self, myrecord)
        else:
            logging.StreamHandler.emit(self, record)


def basicConfig(**kwargs):
    """
    logging.basicConfig() replacement with some extensions:

    Extra arguments:
    :param int rotatecount: Use RotatingFileHandler with `rotatecount` rotation count
    :param int rotatesizek default=5Mb: Use RotatingFileHandler with `rotatesizek` rotation file size in Kb
    :param bool coloredtty default=True: Replace StreamHandler with colored version
    """
    logging._acquireLock()
    try:
        # slight extended copty of logging.basicConfig
        if len(logging.root.handlers) == 0:
            filename = kwargs.get('filename')
            strm = kwargs.get('stream')
            color = kwargs.get('coloredtty', True)
            fs = kwargs.get('format')  #, logging.BASIC_FORMAT)
            lvl = kwargs.get('level')
            if not fs and lvl:
                if lvl <= logging.DEBUG:
                    fs = u"%(asctime)-10s %(levelname)-8s: [%(funcName)s#%(lineno)s] %(message)s"
                else:
                    fs = u"%(asctime)-10s %(levelname)-8s: %(message)s"
            else:
                fs = logging.BASIC_FORMAT
            dfs = kwargs.get('datefmt', '%Y.%m.%d %H:%M:%S')
            fmt = WrapsFormatter(fs, dfs) #logging.Formatter(fs, dfs)

            if filename:
                mode = kwargs.get('filemode', 'a')
                cnt = kwargs.get('rotatecount', 1)
                sz = kwargs.get('rotatesizek', 1024 * 5)  # 5MB
                if cnt > 1:
                    hdlr = logging.handlers.RotatingFileHandler(filename,
                                                                mode,
                                                                sz * 1024,
                                                                cnt)
                else:
                    hdlr = logging.FileHandler(filename, mode)

                hdlr.setFormatter(fmt)
                logging.root.addHandler(hdlr)

            if not strm and color:
                strm = sys.stderr

            if strm:
                if color:
                    hdlr = ColoredHandler(strm)
                else:
                    hdlr = StreamHandler(strm)

                hdlr.setFormatter(fmt)
                logging.root.addHandler(hdlr)

            if lvl is not None:
                logging.root.setLevel(lvl)

    finally:
        logging._releaseLock()


try:
    import argparse

    def add_log_options(parser):
        """Add logging options for ArgumentParser as "logging" sub-group

        By-default `INFO` logging level returned, overriden by quet/debug/verbose switches

        Add `loglevel` and `log_file` properties.

        Add `-q/--quiet`, `-d/--debug`, `-v/--verbose`, `--log-file` options
        """
        #TODO: add log-level parameter with level name/value decoding (?)
        lg = parser.add_argument_group(u"logging")
        lg.add_argument("-q", "--quiet", action="store_const", dest="loglevel", const=logging.ERROR,
                        default=logging.INFO, help=u"Minimal logging.")
        lg.add_argument("-d", "--debug", action="store_const", dest="loglevel", const=logging.DEBUG,
                        default=logging.INFO, help=u"Debug logging.")
        lg.add_argument("-v", "--verbose", action="store_const", dest="loglevel", const=1,
                        default=logging.INFO, help=u"Maximum log detalisation")
        lg.add_argument("--log-file", metavar="FILE",
                        help=u"Also log to FILE")

    # === setup logging ===
    def setup_logging(args, loggername=None):
        """Process parsed by `ArgumentParser` parameters and setup default colored logging.

        Usage::

           import logging
           import argparser
           import setupcon

           log = logging.getLogger('my cool logger')
           #...
           p = argparse.ArgumentParser()
           setupcon.add_log_options(p)
           args = p.parse_args()
           #...
           setupcon.setup_logging(args, 'my cool logger')

        """
        if args and args.loglevel <= logging.DEBUG:
            fmt = u"%(asctime)-10s %(levelname)-8s: [%(funcName)s#%(lineno)s] %(message)s"
        else:
            fmt = u"%(asctime)-10s %(levelname)-8s: %(message)s"

        try:
            setup_console()
            if ansi:
                h = ColoredHandler()
            else:
                raise
        except:
            h = logging.StreamHandler()

        h.setFormatter(logging.Formatter(fmt, datefmt='%Y.%m.%d %H:%M:%S'))
        logging.getLogger(loggername).addHandler(h)

        if args and args.log_file:
            h = logging.FileHandler(args.log_file)
            h.setFormatter(logging.getLogger(loggername).handlers[0].formatter)
            logging.getLogger(loggername).addHandler(h)

        if args and args.loglevel:
            logging.getLogger(loggername).setLevel(args.loglevel)

        #  ..test..
        #log.debug('--debug--')
        #log.info('--info--')
        #log.warning('--warning--')
        #log.error('--error--')
        #log.critical('--critical--')

        # === end setup logging ==
except:
    pass
