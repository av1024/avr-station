/*------------------------------------------------------------------------/
/  Customized string handler for user console interface
/-------------------------------------------------------------------------/
/
/  Portions Copyright (C) 2011, ChaN, all right reserved.
/
/ * This software is a free software and there is NO WARRANTY.
/ * No restriction on use. You can use, modify and redistribute it for
/   personal, non-profit or commercial products UNDER YOUR RESPONSIBILITY.
/ * Redistributions of source code must retain the above copyright notice.
/
/
/ * Updated and customized for UART/W5100/LCD io and AVR memory support
/ * Rewritten by Alexander Voronin, av1024@gmail.com
/-------------------------------------------------------------------------*/

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "gcc_macro.h"
#include "zprintf.h"
#if _USE_UART
    #include "uart/uart.h"
#endif
#if _USE_W5100
    #include "eth/w5100ex.h"
#endif
#if _USE_LCD
    #include "lcd.h"
#endif



#if _USE_XFUNC_OUT
#include <stdarg.h>
typedef enum {
    Normal,
    Flash,
    Eeprom
} FMT_Memory;

void (*zfunc_out)(unsigned char);   /* Pointer to the output stream */
static char *outptr;
static char _handle; // W5100 active handle or UART_HANDLE for serial I/O
static const char *inptr;
static FMT_Memory _fmt_src;

/*----------------------------------------------*/
/* Put a character                              */
/*----------------------------------------------*/

void zputc (char c)
{
    if (_CR_CRLF && c == '\n') zputc('\r');     /* CR -> CRLF */

    if (outptr) {
        *outptr++ = (unsigned char)c;
        return;
    }

    if (zfunc_out) zfunc_out((unsigned char)c);
}

/*----------------------------------------------*/
/* Get a character from format string           */
/*----------------------------------------------*/
static char _next(void) {
    if (!inptr) return 0;
    switch(_fmt_src) {
        case Flash:
            return pgm_read_byte(inptr++);
        case Eeprom:
            return eeprom_read_byte((uint8_t*)inptr++);
        default:
            return *inptr++;
    }
}

/*----------------------------------------------*/
/* Put a null-terminated string                 */
/*----------------------------------------------*/

NOINLINE NONNULL void zputs (                    /* Put a string to the default device */
    const char* str             /* Pointer to the string */
)
{
    while (*str)
        zputc(*str++);
}

NOINLINE NONNULL void zputs_p (                  /* Put a string to the default device */
    const char* str             /* Pointer to the string */
)
{
    uint8_t c;
    while ((c=pgm_read_byte(str++)) != 0)
        zputc(c);
}

NOINLINE NONNULL void zputs_e(                  /* Put a string to the default device */
    const char* str             /* Pointer to the string */
)
{
    uint8_t c;
    while ((c=eeprom_read_byte((uint8_t *)str++)) != 0)
        zputc(c);
}

NOINLINE void zfputs (                   /* Put a string to the specified device */
    void(*func)(unsigned char), /* Pointer to the output function */
    const char* str             /* Pointer to the string */
)
{
    void (*pf)(unsigned char);


    pf = zfunc_out;     /* Save current output device */
    zfunc_out = func;   /* Switch output to specified device */
    while (*str)        /* Put the string */
        zputc(*str++);
    zfunc_out = pf;     /* Restore output device */
}


static void _handle_putc(uint8_t c) {
    #if _USE_W5100
    if (_handle < W5100_NUM_SOCKETS)
        sock_put(_handle, &c, 1);
    #endif
    #if _USE_UART
    if (_handle == UART_SOCKET) uart_putch(c);
    #endif
    #if _USE_LCD
        #if _LCD_NO_LF
            if ((_handle == LCD_SOCKET) && c != '\r') lcd_putc(c);
        #else
            if (_handle == LCD_SOCKET) lcd_putc(c);
        #endif
    #endif
}

/*----------------------------------------------*/
/* Formatted string output                      */
/*----------------------------------------------*/
/*  zprintf("%d", 1234);            "1234"
    zprintf("%6d,%3d%%", -200, 5);  "  -200,  5%"
    zprintf("%-6u", 100);           "100   "
    zprintf("%ld", 12345678L);      "12345678"
    zprintf("%04x", 0xA3);          "00a3"
    zprintf("%08LX", 0x123ABC);     "00123ABC"
    zprintf("%016b", 0x550F);       "0101010100001111"
    zprintf("%s", "String");        "String"
    zprintf("%-4s", "abc");         "abc "
    zprintf("%4s", "abc");          " abc"
    zprintf("%p", PSTR("String"));  "String"
    zprintf("%c", 'a');             "a"
    zprintf("%f", 10.0);            <zprintf lacks floating point support>
*/


static void zvprintf (
    const char* fmt,    /* Pointer to the format string */
    va_list arp         /* Pointer to arguments */
)
{
    unsigned int r, i, j, w, f;
    unsigned long v;
    char s[16], c, d, *p;

    inptr = fmt;

    for (;;) {
        c = _next();                    /* Get a char */
        if (!c) break;              /* End of format? */
        if (c != '%') {             /* Pass through it if not a % sequense */
            zputc(c); continue;
        }
        f = 0;
        c = _next();                    /* Get first char of the sequense */
        if (c == '0') {             /* Flag: '0' padded */
            f = 1; c = _next();
        } else {
            if (c == '-') {         /* Flag: left justified */
                f = 2; c = _next();
            }
        }
        for (w = 0; c >= '0' && c <= '9'; c = _next())  /* Minimum width */
            w = w * 10 + c - '0';
        if (c == 'l' || c == 'L') { /* Prefix: Size is long int */
            f |= 4; c = _next();
        }
        if (!c) break;              /* End of format? */
        d = c;
        if (d >= 'a') d -= 0x20;
        switch (d) {                /* Type is... */
        case 'S' :                  /* String */
            p = va_arg(arp, char*);
            for (j = 0; p[j]; j++) ;
            while (!(f & 2) && j++ < w) zputc(' ');
            zputs(p);
            while (j++ < w) zputc(' ');
            continue;
        case 'P' :                  /* String from Flash */
            p = va_arg(arp, char*);
            for (j = 0; p[j]; j++) ;
            while (!(f & 2) && j++ < w) zputc(' ');
            zputs_p(p);
            while (j++ < w) zputc(' ');
            continue;
        case 'C' :                  /* Character */
            zputc((char)va_arg(arp, int)); continue;
        case 'B' :                  /* Binary */
            r = 2; break;
        case 'O' :                  /* Octal */
            r = 8; break;
        case 'D' :                  /* Signed decimal */
        case 'U' :                  /* Unsigned decimal */
            r = 10; break;
        case 'X' :                  /* Hexdecimal */
            r = 16; break;
        default:                    /* Unknown type (passthrough) */
            zputc(c); continue;
        }

        /* Get an argument and put it in numeral */
        v = (f & 4) ? va_arg(arp, long) : ((d == 'D') ? (long)va_arg(arp, int) : (long)va_arg(arp, unsigned int));
        if (d == 'D' && (v & 0x80000000)) {
            v = 0 - v;
            f |= 8;
        }
        i = 0;
        do {
            d = (char)(v % r); v /= r;
            if (d > 9) d += (c == 'x') ? 0x27 : 0x07;
            s[i++] = d + '0';
        } while (v && i < sizeof(s));
        if (f & 8) s[i++] = '-';
        j = i; d = (f & 1) ? '0' : ' ';
        while (!(f & 2) && j++ < w) zputc(d);
        do zputc(s[--i]); while(i);
        while (j++ < w) zputc(' ');
    }
}


NOINLINE void zprintf (          /* Put a formatted string to the default device */
    const char* fmt,    /* Pointer to the format string */
    ...                 /* Optional arguments */
)
{
    va_list arp;

    _fmt_src = Normal;
    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);
}

NOINLINE void zprintf_p (            /* Put a formatted string to the default device */
    const __flash char* fmt,    /* Pointer to the format string */
    ...                 /* Optional arguments */
)
{
    va_list arp;

    _fmt_src = Flash;
    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);
}

NOINLINE void zprintf_e (            /* Put a formatted string to the default device */
    const char* fmt,    /* Pointer to the format string */
    ...                 /* Optional arguments */
)
{
    va_list arp;

    _fmt_src = Eeprom;
    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);
}


NOINLINE void zsprintf (         /* Put a formatted string to the memory */
    char* buff,         /* Pointer to the output buffer */
    const char* fmt,    /* Pointer to the format string */
    ...                 /* Optional arguments */
)
{
    va_list arp;


    outptr = buff;      /* Switch destination for memory */

    _fmt_src = Normal;
    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);

    *outptr = 0;        /* Terminate output string with a \0 */
    outptr = 0;         /* Switch destination for device */
}

NOINLINE void zsprintf_p (           /* Put a formatted string to the memory */
    char* buff,         /* Pointer to the output buffer */
    const __flash char* fmt,    /* Pointer to the format string */
    ...                 /* Optional arguments */
)
{
    va_list arp;


    outptr = buff;      /* Switch destination for memory */

    _fmt_src = Flash;
    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);

    *outptr = 0;        /* Terminate output string with a \0 */
    outptr = 0;         /* Switch destination for device */
}


NOINLINE void zsprintf_e (           /* Put a formatted string to the memory */
    char* buff,         /* Pointer to the output buffer */
    const char* fmt,    /* Pointer to the format string */
    ...                 /* Optional arguments */
)
{
    va_list arp;


    outptr = buff;      /* Switch destination for memory */

    _fmt_src = Eeprom;
    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);

    *outptr = 0;        /* Terminate output string with a \0 */
    outptr = 0;         /* Switch destination for device */
}


NOINLINE void zfprintf (                 /* Put a formatted string to the specified device */
    void(*func)(unsigned char), /* Pointer to the output function */
    const char* fmt,            /* Pointer to the format string */
    ...                         /* Optional arguments */
)
{
    va_list arp;
    void (*pf)(unsigned char);


    pf = zfunc_out;     /* Save current output device */
    zfunc_out = func;   /* Switch output to specified device */

    _fmt_src = Normal;
    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);

    zfunc_out = pf;     /* Restore output device */
}


NOINLINE void zfprintf_p (                   /* Put a formatted string to the specified device */
    void(*func)(unsigned char), /* Pointer to the output function */
    const __flash char* fmt,            /* Pointer to the format string */
    ...                         /* Optional arguments */
)
{
    va_list arp;
    void (*pf)(unsigned char);


    pf = zfunc_out;     /* Save current output device */
    zfunc_out = func;   /* Switch output to specified device */

    _fmt_src = Flash;
    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);

    zfunc_out = pf;     /* Restore output device */
}

NOINLINE void zfprintf_e (                   /* Put a formatted string to the specified device */
    void(*func)(unsigned char), /* Pointer to the output function */
    const char* fmt,            /* Pointer to the format string */
    ...                         /* Optional arguments */
)
{
    va_list arp;
    void (*pf)(unsigned char);


    pf = zfunc_out;     /* Save current output device */
    zfunc_out = func;   /* Switch output to specified device */

    _fmt_src = Eeprom;
    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);

    zfunc_out = pf;     /* Restore output device */
}

NOINLINE void zhprintf(
    uint8_t socket,
    const char *fmt,
    ...
)
{
    va_list arp;
    void (*pf)(unsigned char);

    pf = zfunc_out;
    zfunc_out = _handle_putc;
    _fmt_src = Normal;
    _handle = socket;

    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);

    zfunc_out = pf;     /* Restore output device */
}

NOINLINE void zhprintf_p(
    uint8_t socket,
    const __flash char *fmt,
    ...
)
{
    va_list arp;
    void (*pf)(unsigned char);

    pf = zfunc_out;
    zfunc_out = _handle_putc;
    _fmt_src = Flash;
    _handle = socket;

    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);

    zfunc_out = pf;     /* Restore output device */
}

NOINLINE void zhprintf_e(
    uint8_t socket,
    const char *fmt,
    ...
)
{
    va_list arp;
    void (*pf)(unsigned char);

    pf = zfunc_out;
    zfunc_out = _handle_putc;
    _fmt_src = Eeprom;
    _handle = socket;

    va_start(arp, fmt);
    zvprintf(fmt, arp);
    va_end(arp);

    zfunc_out = pf;     /* Restore output device */
}

void zvhprintf (
    unsigned char socket,
    const char* fmt,    /* Pointer to the format string */
    va_list arp         /* Pointer to arguments */
)
{
    void (*pf)(unsigned char);

    pf = zfunc_out;
    zfunc_out = _handle_putc;
    _fmt_src = Normal;
    _handle = socket;

    zvprintf(fmt, arp);
    zfunc_out = pf;
}

void zvhprintf_p (
    unsigned char socket,
    const __flash char* fmt,    /* Pointer to the format string */
    va_list arp         /* Pointer to arguments */
)
{
    void (*pf)(unsigned char);

    pf = zfunc_out;
    zfunc_out = _handle_putc;
    _fmt_src = Flash;
    _handle = socket;

    zvprintf(fmt, arp);
    zfunc_out = pf;
}

void zvhprintf_e (
    unsigned char socket,
    const char* fmt,    /* Pointer to the format string */
    va_list arp         /* Pointer to arguments */
)
{
    void (*pf)(unsigned char);

    pf = zfunc_out;
    zfunc_out = _handle_putc;
    _fmt_src = Eeprom;
    _handle = socket;

    zvprintf(fmt, arp);
    zfunc_out = pf;
}

/*----------------------------------------------*/
/* Dump a line of binary dump                   */
/*----------------------------------------------*/

NOINLINE void put_dump (
    const void* buff,       /* Pointer to the array to be dumped */
    unsigned long addr,     /* Heading address value */
    int len,                /* Number of items to be dumped */
    int width               /* Size of the items (DF_CHAR, DF_SHORT, DF_LONG) */
)
{
    int i;
    const unsigned char *bp;
    const unsigned short *sp;
    const unsigned long *lp;


    zprintf("%08lX ", addr);        /* address */

    switch (width) {
    case DW_CHAR:
        bp = buff;
        for (i = 0; i < len; i++)       /* Hexdecimal dump */
            zprintf(" %02X", bp[i]);
        zputc(' ');
        for (i = 0; i < len; i++)       /* ASCII dump */
            zputc((bp[i] >= ' ' && bp[i] <= '~') ? bp[i] : '.');
        break;
    case DW_SHORT:
        sp = buff;
        do                              /* Hexdecimal dump */
            zprintf(" %04X", *sp++);
        while (--len);
        break;
    case DW_LONG:
        lp = buff;
        do                              /* Hexdecimal dump */
            zprintf(" %08LX", *lp++);
        while (--len);
        break;
    }

    zputc('\n');
}

#endif /* _USE_XFUNC_OUT */



#if _USE_XFUNC_IN
unsigned char (*zfunc_in)(void);    /* Pointer to the input stream */

/*----------------------------------------------*/
/* Get a line from the input                    */
/*----------------------------------------------*/

NOINLINE int zgets (     /* 0:End of stream, 1:A line arrived */
    char* buff, /* Pointer to the buffer */
    int len     /* Buffer length */
)
{
    int c, i;


    if (!zfunc_in) return 0;        /* No input function specified */

    i = 0;
    for (;;) {
        c = zfunc_in();             /* Get a char from the incoming stream */
        if (!c) return 0;           /* End of stream? */
        if (c == '\r') break;       /* End of line? */
        if (c == '\b' && i) {       /* Back space? */
            i--;
            if (_LINE_ECHO) zputc(c);
            continue;
        }
        if (c >= ' ' && i < len - 1) {  /* Visible chars */
            buff[i++] = c;
            if (_LINE_ECHO) zputc(c);
        }
    }
    buff[i] = 0;    /* Terminate with a \0 */
    if (_LINE_ECHO) zputc('\n');
    return 1;
}


NOINLINE int zfgets (    /* 0:End of stream, 1:A line arrived */
    unsigned char (*func)(void),    /* Pointer to the input stream function */
    char* buff, /* Pointer to the buffer */
    int len     /* Buffer length */
)
{
    unsigned char (*pf)(void);
    int n;


    pf = zfunc_in;          /* Save current input device */
    zfunc_in = func;        /* Switch input to specified device */
    n = zgets(buff, len);   /* Get a line */
    zfunc_in = pf;          /* Restore input device */

    return n;
}


/*----------------------------------------------*/
/* Get a value of the string                    */
/*----------------------------------------------*/
/*  "123 -5   0x3ff 0b1111 0377  w "
        ^                           1st call returns 123 and next ptr
           ^                        2nd call returns -5 and next ptr
                   ^                3rd call returns 1023 and next ptr
                          ^         4th call returns 15 and next ptr
                               ^    5th call returns 255 and next ptr
                                  ^ 6th call fails and returns 0
*/

NOINLINE int zatoi (           /* 0:Failed, 1:Successful */
    char **str,     /* Pointer to pointer to the string */
    long *res       /* Pointer to the valiable to store the value */
)
{
    unsigned long val;
    unsigned char c, r, s = 0;


    *res = 0;

    while ((c = **str) == ' ') (*str)++;    /* Skip leading spaces */

    if (c == '-') {     /* negative? */
        s = 1;
        c = *(++(*str));
    }

    if (c == '0') {
        c = *(++(*str));
        switch (c) {
        case 'x':       /* hexdecimal */
            r = 16; c = *(++(*str));
            break;
        case 'b':       /* binary */
            r = 2; c = *(++(*str));
            break;
        default:
            if (c <= ' ') return 1; /* single zero */
            if (c < '0' || c > '9') return 0;   /* invalid char */
            r = 8;      /* octal */
        }
    } else {
        if (c < '0' || c > '9') return 0;   /* EOL or invalid char */
        r = 10;         /* decimal */
    }

    val = 0;
    while (c > ' ') {
        if (c >= 'a') c -= 0x20;
        c -= '0';
        if (c >= 17) {
            c -= 7;
            if (c <= 9) return 0;   /* invalid char */
        }
        if (c >= r) return 0;       /* invalid char for current radix */
        val = val * r + c;
        c = *(++(*str));
    }
    if (s) val = 0 - val;           /* apply sign if needed */

    *res = val;
    return 1;
}

#endif /* _USE_XFUNC_IN */
