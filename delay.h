// alternate version of delay_ms
#ifndef __DELAY_H__
#define __DELAY_H__

/**
 * Overriden delay functions.
 * Frees some bytes of memory (2+ per call) comparing to "inline" _delay_ms
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 * Date: 2013
 */

void delay1ms(uint8_t ms);
void delay10ms(uint8_t ts);
void delay100ms(uint8_t ds);

#endif // __DELAY_H__
