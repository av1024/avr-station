#ifndef __NTP_H__
#define __NTP_H__
/* NTP time query and conversion functions.
 * Partially optimized for avr-gcc 4.7.2 minimal size.
 *
 * UNIX timestamp to time struct code via Peter Dannegger, <danni@specs.de>
 *
 * Author: Alexander Voronin, <av1024@gmail.com>
 */

#include "../gcc_macro.h"

#define NTP_SOCK    0
#define NTP_PORT    123

// comment out for native NTP time (sec. since 01.01.1900)
#define NTP_AS_UNIXTIME

typedef struct {
    // UDP header
    uint8_t   ip[4];
    uint16_t  port; // NB! reversed byte order
    uint16_t  size; // NB! reversed byte order
    // NTP data
    uint8_t status; // LI + status
    uint8_t type;
    uint16_t precision;
    union {
        uint32_t est_error;
        struct {
            uint16_t est_error_int;
            uint16_t est_error_frac;
        };
    };
    union {
        uint32_t est_drift;
        struct {
            uint16_t est_drift_int;
            uint16_t est_drift_frac;
        };
    };
    char ref_clock_id[4];
    uint32_t ref_tstamp_int;
    uint32_t ref_tstamp_frac;

    uint32_t orig_tstamp_int;
    uint32_t orig_tstamp_frac;

    uint32_t recv_tstamp_int;
    uint32_t recv_tstamp_frac;

    uint32_t transm_tstamp_int;
    uint32_t transm_tstamp_frac;
} NTPv2Answer;

//Request NTPv2 time from remote server and store result in `timestamp`
bool NTPTime(uint8_t sock, const uint8_t *ip, uint32_t *timestamp);

#endif  // __NTP_H__
