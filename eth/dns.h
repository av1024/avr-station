#ifndef __DNS_H__
#define __DNS_H__
/* Simple DNS gethostbyname implementation.
 *
 * - Fill passed buffer with first matching IP in answer.
 * - Valid DNS record in answer is TYPE=A and CLASS=IN, all other will be ignored
 *
 * - NOTE: passed hostname WILL BE BROKEN after call! Inplace dot-to-chunk conversion used
 *
 * Usage:
 *
 *   uint8_t dns[] = {8, 8, 8, 8};
 *   uint8_t host = "habrahabr.ru";
 *   uint8_t ip[4];
 *
 *   gethostbyname(SOCK_NO, dns, host, ip);
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 */

#define DNS_SOCK    0

bool gethostbyname(uint8_t sock, const uint8_t *dns, uint8_t *hostname, uint8_t *ip);

#endif // __DNS_H__
