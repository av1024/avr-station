// w5100ex.c

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>

#include "../gcc_macro.h"
#include "../delay.h"
#include "w5100ex.h"

static uint16_t _ptr[4] = { 0, 0, 0, 0 }; // sock_put()  etc ptr

uint8_t NONNULL ip_valid(uint8_t const *ip) {
    if (!(ip[0] | ip[1] | ip[2] | ip[3])) return W5100_FAIL;
    if (ip[0]==0xFF && ip[1]==0xFF && ip[2]==0xFF && ip[3]==0xFF) return W5100_FAIL;
    return W5100_OK;
}

void  W51_write(uint16_t  addr, uint8_t data)
{
    W51_select();                                   // enable the W5100 chip
    W51_xchg(W5100_WRITE_OPCODE);                   // need to write a byte
    W51_xchg((addr & 0xff00) >> 8);             // send MSB of addr
    W51_xchg(addr & 0xff);                          // send LSB
    W51_xchg(data);                                 // send the data
    W51_deselect();                                 // done with the chip
}

uint8_t W51_read(uint16_t addr)
{
    register uint8_t val;

    W51_select();                                   // enable the W5100 chip
    W51_xchg(W5100_READ_OPCODE);                    // need to read a byte
    W51_xchg((addr & 0xff00) >> 8);             // send MSB of addr
    W51_xchg(addr & 0xff);                          // send LSB
    val = W51_xchg(0x00);                           // need to send a dummy char to get response
    W51_deselect();                                 // done with the chip
    return  val;                                // tell her what she's won
}

uint8_t  W51_init(void)
{
    #ifdef W5100_HAS_HARD_RESET
        reset();                        // if host provided a reset function, use it
    #else
        W51_write(W5100_MR, W5100_MR_SOFTRST);      // otherwise, force the w5100 to soft-reset
    #endif
    delay1ms(1);
    if (W51_read(W5100_MR) != 0xFF) return W5100_OK;
    return W5100_FAIL;
}

uint8_t NONNULL W51_write_ip(uint16_t reg, const uint8_t *buf) {
    register uint8_t i;

    for(i=0;i<4;i++) W51_write(reg+i, buf[i]);
    delay1ms(1);

    if (ip_valid(buf) != W5100_OK) return W5100_FAIL;
    for(i=0;i<4;i++) if (W51_read(reg+i) != buf[i]) return W5100_FAIL;;
    return W5100_OK;
}

uint8_t NONNULL W51_config(W5100_CFG  *cfg)
{
    register uint8_t i;
    if (cfg == 0)  return  W5100_FAIL;

    W51_write(W5100_RMSR, 0x55);                    // use default buffer sizes (2K bytes RX and TX for each socket
    W51_write(W5100_TMSR, 0x55);

    W51_write_ip(W5100_GAR, cfg->gtw_addr);

    for(i=0;i<6;i++) W51_write(W5100_SHAR+i, cfg->mac_addr[i]);
    delay1ms(1);

    if (W51_write_ip(W5100_SUBR, cfg->sub_mask) != W5100_OK) return W5100_FAIL;
    if (W51_write_ip(W5100_SIPR, cfg->ip_addr) != W5100_OK) return W5100_FAIL;

    return  W5100_OK;                               // everything worked, show success
}

uint8_t NONNULL W51_config_e(const W5100_CFG *ecfg) {
    W5100_CFG tmp;
    eeprom_read_block(&tmp, ecfg, sizeof(W5100_CFG));
    return W51_config(&tmp);
}

uint8_t NONNULL W51_config_p(const W5100_CFG *pcfg) {
    W5100_CFG tmp;
    memcpy_P(&tmp, pcfg, sizeof(W5100_CFG));
    return W51_config(&tmp);
}

/******************************************************************/
/*                   HIGH LEVEL FUNCTIONS                         */
/******************************************************************/

uint8_t w5100_init(void) {
    // SPI init
    DDR(SPI_PORT) |= (1<<SPI_SS) | (1<<SPI_SCK) | (1<<SPI_MOSI);
    SPI_PORT |= (1<<SPI_MISO) | (1<<SPI_SS); // pull-ups
    SPCR = (1<<SPE) | (1<<MSTR) | (0<<SPR0) | (0<<SPR1) | (1<<SPI2X);

    return W51_init();
}

void W51_select(void) {            // function for selecting w5100 chip
    SPI_PORT &= ~(1<<SPI_SS);
}

//TODO: add anti-lock version
uint8_t W51_xchg(uint8_t val) {     // function for exchanging byte with w5100
    SPDR = val;
    while   (!(SPSR & (1<<SPIF))) ;
    return SPDR;
}

void W51_deselect(void) {           // function for de-selecting w5100 chip
    SPI_PORT |= (1<<SPI_SS);
}

void sock_waitfor(uint16_t sockaddr) {
    while (W51_read(sockaddr))  ;   // loop until socket is closed (blocks!!)
}

void sock_close(uint8_t sock) {
    uint16_t  sockaddr;
    #ifdef W51_SAFE_SOCK_NO
        if (sock > W5100_NUM_SOCKETS) return;
    #endif
    sockaddr = W5100_SKT_BASE(sock) + W5100_CR_OFFSET;                // calc base addr for this socket

    W51_write(sockaddr, W5100_SKT_CR_CLOSE);    // tell chip to close the socket
    sock_waitfor(sockaddr); //BLOCKS
}

void sock_disconnect(uint8_t sock) {
    uint16_t            sockaddr;

    #ifdef W51_SAFE_SOCK_NO
        if (sock > W5100_NUM_SOCKETS) return;
    #endif

    sockaddr = W5100_SKT_BASE(sock) + W5100_CR_OFFSET;

    W51_write(sockaddr, W5100_SKT_CR_DISCON);       // disconnect the socket

    sock_waitfor(sockaddr); //BLOCKS
}


uint8_t sock_open(uint8_t  sock, uint8_t  eth_protocol, uint16_t  tcp_port)
{
    uint8_t retval;
    uint16_t sockaddr;

    retval = W5100_FAIL;                            // assume this doesn't work
    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return retval;  // illegal socket value is bad!
    #endif

    sockaddr =  W5100_SKT_BASE(sock);               // calc base addr for this socket

    if (W51_read(sockaddr+W5100_SR_OFFSET) != W5100_SKT_SR_CLOSED)    // Make sure we close the socket first
    {
        sock_close(sock);
    }

    W51_write(sockaddr+W5100_MR_OFFSET, eth_protocol);      // set protocol for this socket
    W51_write(sockaddr+W5100_PORT_OFFSET, ((tcp_port & 0xFF00) >> 8 ));     // set port for this socket (MSB)
    W51_write(sockaddr+W5100_PORT_OFFSET + 1, (tcp_port & 0x00FF));         // set port for this socket (LSB)
    W51_write(sockaddr+W5100_CR_OFFSET, W5100_SKT_CR_OPEN);                 // open the socket

    sock_waitfor(sockaddr+W5100_CR_OFFSET); //BLOCKS

    switch(retval=W51_read(sockaddr+W5100_SR_OFFSET)) {
        case W5100_SKT_SR_INIT:     // TCP open
        case W5100_SKT_SR_UDP:
        case W5100_SKT_SR_IPRAW:
        case W5100_SKT_SR_MACRAW:
        case W5100_SKT_SR_PPPOE:
            break;
        default:
            retval = W5100_FAIL;
            sock_close(sock);
            break;
    }

    return  retval;
}

void NONNULL sock_set_dest(uint8_t sock, const uint8_t *ip, const uint16_t port) {
    uint16_t sockaddr;
    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return;  // illegal socket value is bad!
    #endif
    sockaddr = W5100_SKT_BASE(sock);

    W51_write_ip(sockaddr+W5100_DIPR_OFFSET, ip);
    W51_write(sockaddr+W5100_DPORT_OFFSET, port >> 8);
    W51_write(sockaddr+W5100_DPORT_OFFSET+1, port & 0x00FF);
}

uint8_t sock_listen(uint8_t sock) {
    uint8_t retval;
    uint16_t sockaddr;

    retval = W5100_FAIL;                            // assume this doesn't work
    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return retval;  // illegal socket value is bad!
    #endif

    sockaddr =  W5100_SKT_BASE(sock);               // calc base addr for this socket
    if (W51_read(sockaddr+W5100_SR_OFFSET) == W5100_SKT_SR_INIT)    // if socket is in initialized state...
    {
        W51_write(sockaddr+W5100_CR_OFFSET, W5100_SKT_CR_LISTEN);       // put socket in listen state
        sock_waitfor(sockaddr+W5100_CR_OFFSET); //BLOCKS

        if (W51_read(sockaddr+W5100_SR_OFFSET) == W5100_SKT_SR_LISTEN)  retval = W5100_OK;  // if socket state changed, show success
        else  sock_close(sock);                    // not in listen mode, close and show an error occurred
    }
    return  retval;
}

uint16_t  sock_recv_size(uint8_t  sock)
{
    uint16_t                    val;
    uint16_t                    sockaddr;

    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return 0;  // illegal socket value is bad!
    #endif
    sockaddr = W5100_SKT_BASE(sock);                        // calc base addr for this socket
    val = W51_read(sockaddr+W5100_RX_RSR_OFFSET);// & 0xff;
    val = (val << 8) | W51_read(sockaddr+W5100_RX_RSR_OFFSET + 1);
    return  val;
}

//NB: use only if really needed 1 char!!!
uint8_t sock_getch(uint8_t sock) {
    uint8_t b;
    if (sock_recv(sock, &b, 1) == W5100_OK) return b;
    return 0;
}

uint8_t sock_recv(uint8_t sock, uint8_t  *buf, uint16_t  buflen) {
    uint16_t sockaddr;
    uint16_t ptr;
    uint16_t offaddr;
    uint16_t realaddr;

    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return W5100_FAIL;  // illegal socket value is bad!
    #endif
    sockaddr = W5100_SKT_BASE(sock);
    ptr = W51_read(sockaddr+W5100_RX_RD_OFFSET); // RX read pointer (MSB)
    offaddr = (ptr << 8) | W51_read(sockaddr+W5100_RX_RD_OFFSET+1); // MSB && calc

    if (buf != NULL) {
        ptr = (W5100_RX_BUF_MASK+1) * sock;
        //increase W51 buffer pointers
        while (buflen)
        {
            buflen--;
            realaddr = W5100_RXBUFADDR + ptr + (offaddr & W5100_RX_BUF_MASK); // auto overflow recalc

            *buf = W51_read(realaddr);
            offaddr++;
            buf++;
        }
    } else {
        offaddr += buflen;
    }

    W51_write(sockaddr+W5100_RX_RD_OFFSET, offaddr >> 8);    // update RX read offset (MSB)
    W51_write(sockaddr+W5100_RX_RD_OFFSET + 1, offaddr & 0x00FF);      // update LSB

    // Now Send the RECV command
    W51_write(sockaddr+W5100_CR_OFFSET, W5100_SKT_CR_RECV);         // issue the receive command
    _delay_us(5);                                           // wait for receive to start

    return W5100_OK;
}

uint8_t sock_recv_skip(uint8_t sock, uint16_t count) {
    return sock_recv(sock, NULL, count);
}

uint8_t sock_recv_flush(uint8_t sock) {
    uint16_t sz = sock_recv_size(sock);
    if (sz) return sock_recv_skip(sock, sz);
    return W5100_OK;
}

uint16_t sock_tx_free(uint8_t sock) {
    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return 0;  // illegal socket value is bad!
    #endif
    uint16_t sockaddr = W5100_SKT_BASE(sock);
    uint16_t txsize = W51_read(sockaddr+W5100_TX_FSR_OFFSET);        // make sure the TX free-size reg is available
    uint8_t tmp = W51_read(sockaddr+W5100_TX_FSR_OFFSET + 1);
    return (txsize << 8) | tmp;
}

void sock_tx_flush(uint8_t sock) {
    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return;  // illegal socket value is bad!
    #endif

    for(uint8_t i=0;i<255;i++) {
        if (sock_tx_free(sock) > W5100_TX_BUF_MASK) break;
        delay1ms(1);
    }
    delay1ms(1);
}

uint8_t sock_wait_txbuf(uint8_t sock, uint16_t buflen) {

    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return W5100_FAIL;  // illegal socket value is bad!
    #endif

    uint16_t timeout = 0;
    uint16_t txsize;

    txsize = sock_tx_free(sock);
    timeout = 0;
    while (txsize < buflen)
    {
        delay1ms(1);

        txsize = sock_tx_free(sock);

        if (timeout++ > 1000)                       // if max delay has passed...
        {
            return  W5100_FAIL;                     // show failure
        }
    }
    return W5100_OK;
}

uint8_t NONNULL sock_send(uint8_t  sock, const uint8_t  *buf, uint16_t  buflen) {

    while(buflen > 255) {
        if (sock_put(sock, buf, 255) != W5100_OK) return W5100_FAIL;
        buflen -= 255;
    }

    if (sock_put(sock, buf, (uint8_t)(buflen & 0xFF)) != W5100_OK) return W5100_FAIL;

    sock_put_done(sock);

    return  sock_do_send(sock);
}

uint8_t sock_do_send(uint8_t sock) {
    uint16_t sockaddr;

    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return W5100_FAIL;  // illegal socket value is bad!
    #endif
    sockaddr = W5100_SKT_BASE(sock);


    W51_write(sockaddr+W5100_CR_OFFSET, W5100_SKT_CR_SEND); // start the send on its way

    sock_waitfor(sockaddr+W5100_CR_OFFSET); //BLOCKS


    /*W51_write(sockaddr+W5100_IR_OFFSET, 0);
    uint8_t r;
    while(!(r=W51_read(sockaddr+W5100_IR_OFFSET))) ; // BLOCKS until send or timeout

    if (r & W5100_SKT_IR_SEND_OK) return W5100_OK;
    */
    sock_tx_flush(sock);
    return W5100_OK;
}

// update TX_WR_OFFSET after `sock_put` calls
uint8_t sock_put_done(uint8_t sock) {
    uint16_t offaddr;
    uint16_t sockaddr;

    #ifdef W51_SAFE_SOCK_NO
        if (sock >= W5100_NUM_SOCKETS)  return W5100_FAIL;  // illegal socket value is bad!
    #endif

    sockaddr = W5100_SKT_BASE(sock);            // calc base addr for this socket
    offaddr = ((uint16_t)W51_read(sockaddr+W5100_TX_WR_OFFSET)) << 8;
    offaddr |= W51_read(sockaddr+W5100_TX_WR_OFFSET+1);

    offaddr += _ptr[sock];
    _ptr[sock] = 0;

    W51_write(sockaddr+W5100_TX_WR_OFFSET, offaddr >> 8);    // send MSB of new write-pointer addr
    W51_write(sockaddr+W5100_TX_WR_OFFSET + 1, (offaddr & 0x00FF));     // send LSB

    return  W5100_OK;
}

// alternate version w/o TX_WR_OFFSET update
uint8_t NONNULL sock_put(uint8_t sock, const uint8_t *buf, uint8_t count) {
    uint16_t sockaddr;
    uint16_t offaddr;
    uint16_t realaddr;
    uint16_t ptr;

    if (count == 0
        #ifdef W51_SAFE_SOCK_NO
        || sock >= W5100_NUM_SOCKETS
        #endif
        )  return  W5100_FAIL;      // ignore illegal requests

    sockaddr = W5100_SKT_BASE(sock);            // calc base addr for this socket
    if (sock_wait_txbuf(sock, count) != W5100_OK) {
            sock_disconnect(sock);                  // can't connect, close it down
            return  W5100_FAIL;                     // show failure
    }


    offaddr = W51_read(sockaddr+W5100_TX_WR_OFFSET);

    offaddr = (offaddr << 8) | W51_read(sockaddr+W5100_TX_WR_OFFSET + 1);

    offaddr += _ptr[sock];
    _ptr[sock] += count;

    ptr = (W5100_TX_BUF_MASK+1) * sock;
    while (count)
    {
        count--;
        realaddr = W5100_TXBUFADDR + ptr + (offaddr & W5100_TX_BUF_MASK);     // calc W5100 physical buffer addr for this socket

        W51_write(realaddr, *buf);                  // send a byte of application data to TX buffer

        offaddr++;                                  // next TX buffer addr
        buf++;                                      // next input buffer addr
    }

    return W5100_OK;
}


uint8_t sock_status(uint8_t sock) {
    uint16_t sockaddr = W5100_SKT_BASE(sock);
    return W51_read(sockaddr + W5100_SR_OFFSET);
}

uint8_t sock_ir(uint8_t sock) {
    uint16_t sockaddr = W5100_SKT_BASE(sock);
    return W51_read(sockaddr + W5100_IR_OFFSET);
}

uint8_t NONNULL sock_send_p(uint8_t sock, const char *progstr) {

    if (sock_put_p(sock, progstr) != W5100_OK) return W5100_FAIL;
    if (_ptr[sock]) {
        sock_put_done(sock);
        sock_do_send(sock);
    }
    return W5100_OK;
}

uint8_t NONNULL sock_put_p(uint8_t sock, const char *progstr) {
    uint8_t c;
    while ((c=pgm_read_byte(progstr++))) {
        if (sock_put(sock, &c, 1) != W5100_OK) return W5100_FAIL;
    }
    return W5100_OK;
}
