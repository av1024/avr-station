//ntp.c

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <string.h>

#include "../gcc_macro.h"
#include "../delay.h"
#include "w5100ex.h"
#include "ntp.h"

bool NTPTime(uint8_t sock, const uint8_t *ip, uint32_t *timestamp) {
    NTPv2Answer ntp;
    uint8_t retval = false;

    if (ip_valid(ip) != W5100_OK) return false;

    //ZEROVAR(ntp);
    memset(&ntp, 0, sizeof(ntp));
    ntp.ip[0] = 0x1B; // set flags in _RAW_ buffer to request NTP time

    // Set src port > 1024 because firewall blocks
    if (sock_open(sock, W5100_SKT_MR_UDP, 50000+NTP_PORT) != W5100_FAIL) {
        sock_set_dest(sock, ip, NTP_PORT);

        if (sock_send(sock, (uint8_t*)&ntp, 48) == W5100_OK) {
            for(uint8_t i=0;i<50;i++) {
                if (sock_recv_size(sock) >= 56) {
                    if (sock_recv(sock, (uint8_t*)&ntp, 56) == W5100_OK) {
                        uint32_t tmp;
                        uint8_t *x = (uint8_t*)&ntp.transm_tstamp_int,
                                *y = (uint8_t*)&tmp;
                        x += 3;
                        for(uint8_t j=0;j<4;j++) *y++ = *x--;
                        *timestamp = tmp;
                        #ifdef NTP_AS_UNIXTIME
                            (*timestamp) -= 0x83AA7E80UL; // diff between 01.01.1900 and 01.01.1970 (sec.)
                        #endif
                        retval = true;
                        break;
                    }
                }
                delay1ms(100);
            }
        }

    }
    sock_close(sock);
    return retval;
}
