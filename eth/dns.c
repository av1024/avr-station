/**
 * Simple basic DNS client implementation
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 */

#include <avr/io.h>
#include <string.h>

#include "../gcc_macro.h"
#include "../delay.h"
#include "w5100ex.h"


/* DNS MESSAGE FORMAT (http://tools.ietf.org/html/rfc1035):
    +---------------------+
    |        Header       |
    +---------------------+
    |       Question      | the question for the name server
    +---------------------+
    |        Answer       | RRs answering the question
    +---------------------+
    |      Authority      | RRs pointing toward an authority
    +---------------------+
    |      Additional     | RRs holding additional information
    +---------------------+

The header section is always present.  The header includes fields that
specify which of the remaining sections are present, and also specify
whether the message is a query or a response, a standard query or some
other opcode, etc.


The header contains the following fields:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      ID                       |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    QDCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ANCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    NSCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ARCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

Question section format
~~~~~~~~~~~~~~~~~~~~~~~

The question section is used to carry the "question" in most queries,
i.e., the parameters that define what is being asked.  The section
contains QDCOUNT (usually 1) entries, each of the following format:

                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /                     QNAME                     /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     QTYPE                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     QCLASS                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

QNAME:
    size byte
    [size] name_data
    ...
    size byte=0 as terminator

QTYPE:
    word

QCLASS:
    word


Resource record format

The answer, authority, and additional sections all share the same
format: a variable number of resource records, where the number of
records is specified in the corresponding count field in the header.
Each resource record has the following format:
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /                                               /
    /                      NAME                     /
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      TYPE                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     CLASS                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      TTL                      |
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                   RDLENGTH                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
    /                     RDATA                     /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 */


#define DNS_HDR_QR              0b10000000      /* 0: query, 1: response */
#define DNS_HDR_OPCODE_MASK     0b01111000
#define DNS_HDR_OPCODE_QUERY    0b00000000      /* a standard query */
#define DNS_HDR_OPCODE_IQUERY   0b00001000      /* inverse query */
#define DNS_HDR_OPCODE_STATUS   0b00010000      /* server status request */
#define DNS_HDR_AA              0b00000100      /* authoritative answer */
#define DNS_HDR_TC              0b00000010      /* message truncated */
#define DNS_HDR_RD              0b00000001      /* recursion desired */

#define DNS_HDR_RA              0b10000000      /* recursion available */
#define DNS_HDR_Z_MASK          0b01110000      /* reserved, should be zero */
#define DNS_HDR_RCODE_MASK      0b00001111      /* response code mask */
#define DNS_HDR_RCODE_NOERROR   0
#define DNS_HDR_RCODE_FORMAT    1
#define DNS_HDR_RCODE_SERVER    2
#define DNS_HDR_RCODE_NAME      3
#define DNS_HDR_RCODE_NOTIMPL   4
#define DNS_HDR_RCODE_REFUSED   5

#define DNS_PORT        53
#define DNS_QUERY_ID    0x5566

#define HTONS(x) ((((uint16_t)x) >> 8) | (((uint16_t)x) << 8))

// NOTE: network byte order, LSB first !!!
typedef struct {
    union {
        struct {
            uint16_t id;
            uint8_t flags_lo;
            uint8_t flags_hi;
            uint16_t qd_count; // number of entries in question section
            uint16_t an_count; // number of records in aswer section
            uint16_t ns_count; // number of name server resource records in authority section
            uint16_t ar_count; // number of records in additional section
        };
        uint8_t buf[12];
    };
} DNSHeader;

//INPLACE convert dotted string into DNS QName sequence
//return 1-st [size] byte of 1-st label
static NONNULL uint8_t text2qname(uint8_t *str) {
    register uint8_t r = 0;
    register uint8_t *s = (uint8_t*)0;
    while(*str) {
        if (*str=='.') {
            s = str;
            *s = 0;
        } else {
            if (!s) r++;
            (*s)++;
        }
        str++;
    }
    return r;
}

static bool _skip_qname(uint8_t sock) {

    uint8_t n = sock_getch(sock);

    while(n) {
        if ((n & 0xc0) == 0xc0) {
            if (!sock_getch(sock)) return false;
            return true;
        }
        if (n > sock_recv_size(sock)) return false;
        if (sock_recv(sock, (void*)0, n) != W5100_OK) {
            return false;
        }
        n = sock_getch(sock);
    }
    return true;
}

static NONNULL bool _read_rr(uint8_t sock, uint8_t count, uint8_t *dest_ip) {
    uint8_t buf[10];
    uint16_t *rz = (uint16_t*)&buf[8];

    for(uint8_t i=0;i<count;i++) {
        if (!_skip_qname(sock)) return false;

        if (sock_recv(sock, buf, 10) != W5100_OK) return false;

        if (buf[1]==1 && buf[3]==1) {

            sock_recv(sock, dest_ip, 4); //OPTIMIZED: always OK because `sock` is ok
            return true;
        }

        sock_recv_skip(sock, HTONS((*rz)));
    }
    return false;
}

bool NONNULL gethostbyname(uint8_t sock, const uint8_t *dns, uint8_t *hostname, uint8_t *ip) {

    DNSHeader hdr;
    uint8_t x;
    uint8_t tail[4] = "\x00\x01\x00\x01"; // QTYPE=A, QCLASS=1, used as read buffer

    if (ip_valid(dns) != W5100_OK) return false;

    // prepare query
    //ZEROVAR(hdr);
    memset(&hdr, 0, sizeof(hdr));
    hdr.id = DNS_QUERY_ID;

    hdr.qd_count = HTONS(1);
    hdr.flags_lo = DNS_HDR_RD; // turn on recursion because don't want to decode CNAME's
    x = text2qname(hostname);

    if (sock_open(sock, W5100_SKT_MR_UDP, DNS_PORT) == W5100_FAIL) return false;
    sock_set_dest(sock, dns, DNS_PORT);

    sock_put(sock, (uint8_t *)&hdr, sizeof(hdr));

    // put hostname
    sock_put(sock, &x, 1);
    x=0;
    while(hostname[x]) x++;
    x++;
    sock_put(sock, hostname, x);

    // put CLASS=A, TYPE=IN
    sock_put(sock, tail, 4);

    sock_put_done(sock);

    //send
    if (sock_do_send(sock) == W5100_OK) {

        for(uint8_t i=0;i<50;i++) {
            if (sock_recv_size(sock) >= sizeof(hdr)+6) break;
            delay10ms(5); // 250ms
        }

        if (sock_recv_size(sock) >= sizeof(hdr)+6) {
            // get header
            sock_recv(sock, (uint8_t *)&hdr, 8); // skip IP + port + size
            sock_recv(sock, (uint8_t *)&hdr, sizeof(hdr));

            if ((hdr.flags_hi & DNS_HDR_RCODE_MASK) == DNS_HDR_RCODE_NOERROR) {;
                hdr.qd_count = HTONS(hdr.qd_count);
                hdr.an_count = HTONS(hdr.an_count);
                hdr.ns_count = HTONS(hdr.ns_count);
                hdr.ar_count = HTONS(hdr.ar_count);

                // QD
                for(uint8_t i=0;i<hdr.qd_count;i++) {
                    if (!_skip_qname(sock)) {
                        sock_close(sock);
                        return false;
                    }
                    sock_recv(sock, tail, 4); // TYPE, CLASS
                }

                // AN
                if (_read_rr(sock, hdr.an_count, ip)) {
                    sock_close(sock);
                    return true;
                }

                // NS
                if (_read_rr(sock, hdr.ns_count, ip)) {
                    sock_close(sock);
                    return true;
                }

                // AR
                if (_read_rr(sock, hdr.ar_count, ip)) {
                    sock_close(sock);
                    return true;
                }

            }
        }
    };
    sock_close(sock);
    return false;
}
