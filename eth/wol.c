// wol.c

#include <avr/io.h>

#include "../gcc_macro.h"
#include "w5100ex.h"
#include "wol.h"

// direct-put version
bool WOL(uint8_t sock, const uint8_t *toIP, const uint8_t *toMAC) {

    bool r = false;
    uint8_t b = 0xFF;

    if (sock_open(sock, W5100_SKT_MR_UDP, 1024+WOL_PORT) == W5100_SKT_SR_UDP) {
        sock_set_dest(sock, toIP, WOL_PORT);

        for(uint8_t i=0;i<6;i++) {
            sock_put(sock, &b, 1);
        }

        for(uint8_t i=0;i<16;i++) {
            sock_put(sock, toMAC, 6);
        }

        sock_put_done(sock);
        sock_do_send(sock);
        r = true;
    }
    sock_close(sock);

    return r;

}
