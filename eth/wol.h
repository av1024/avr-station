#ifndef __WOL_H__
#define __WOL_H__

/** UDP WOL sending
 *
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 */
#define WOL_PORT    9

/* Send WakeOnLan packet via UDP.
 *
 *
 */
bool WOL(uint8_t sock, const uint8_t *toIP, const uint8_t *toMAC);

#endif // __WOL_H__
