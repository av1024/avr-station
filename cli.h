#ifndef __CLI__
#define __CLI__
/**
 * Command-Line Interface.
 *
 * Planning: to be used as telnet and UART console
 *
 * Usage:
 *  1. Define command name and (optional) help texts as flash strings.
 *
 *   static const __flash uint8_t _cmd1[] = "cmd1";
 *   static const __flash uint8_t _cmd2[] = "command2";
 *   ...
 *   const __flash uint8_t * const __flash cliCommands[] = {
 *   _cmd1,
 *   _cmd2,
 *  ...
 *   };
 *
 *   static const __flash uint8_t help1[] = "help cmd1";
 *   const __flash uint8_t * const __flash cliHelps[] = {
 *    _help1,
 *    _help2...
 *   };
 *
 *   2. Define functions compatible with CLIFunction typedef:
 *
 *   bool onCmd1(uint8_t *args) {
 *     uart_pprint("CMD1: ");
 *     uart_print(args);
 *     return true;
 *   }
 *
 *   3. Define cliFuncs array and fill it with links to command functions.
 *      NOTE about trailing zero after all commands
 *
 *   CLIFunction * const __flash cliFuncs[] = {
 *     &onCmd1,
 *     ...
 *     0
 *   };
 *
 *   4. in main loop:
 *    - get command s
 *    - call cli_command(s) and check result if nedded
 *
 * Note: Uses GCC4.7+ qualifier for Flash memory allocation
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7+, compile with -std=gnu99
 * Hardware: ATMega328p
 * Date: 2013
 */

#if (__GNUC__ * 100 + __GNUC_MINOR__) < 407
#error "This library requires AVR-GCC 4.7 or later, update to newer AVR-GCC compiler !"
#endif

//comment out to disable per-command help strings support
#define CLI_USE_HELP

//max total length of command with arguments
#define CLI_MAXCMDSIZE  30


//! function prototype to be called for supported commands
typedef bool CLIFunction(uint8_t handle, uint8_t *args);


// can be used in serial i/o
typedef struct {
    uint8_t cmdbuf[CLI_MAXCMDSIZE+1];
    uint8_t len;
    bool ready;
} CLIData;


extern const __flash uint8_t * const __flash cliCommands[];
extern CLIFunction * const __flash cliFuncs[];
#ifdef CLI_USE_HELP
extern const __flash uint8_t * const __flash cliHelps[];
#endif


// --- support functions ---

//!returns index of function for command or 0xFF if invalid command
uint8_t cli_cmd_index(uint8_t *cmd);

//! INPLACE!! split cmdline, find relevant cmd in cliCommands and execute with rest of cmdline
bool cli_command(uint8_t handle, uint8_t *cmdline);

/** INPLACE split cmdline
 * Returns actual count of args (may be greater than max_args!)
 *
 * - str: string to be changed
 * - args[]: pointer to result array at least `max_args` of uint8_t*
 * - delim: args delimiter
 * - max_args: size of args[]
 */
uint8_t cli_split_args(uint8_t *str, uint8_t *args[], uint8_t delim, uint8_t max_args);

#endif // __CLI__
