#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>

#include <stdlib.h>
#include "../gcc_macro.h"
#include "../delay.h"

#include "uart/uart.h"
#include "uart/strutil.h"
#include "eth/w5100ex.h"
#include "eth/ntp.h"
#include "eth/wol.h"
#include "eth/dns.h"

#include "../cli_io.h"
#include "../cli.h"

#include "xprintf.h"

#define TELNET_SOCK     1
#define TELNET_PORT     23

#define EFL_ETH      0x01    /* ethernet OK */
#define EFL_TELNET   0x02    /* telnet socket connected */

uint8_t EFlags = 0; // ethernet i/o flags

/******************************************/
//  CLI
/******************************************/

void telnet_putch(uint8_t ch) {
    sock_put(TELNET_SOCK, &ch, 1);
    if (ch == '\n') {
        sock_put_done(TELNET_SOCK);
        sock_do_send(TELNET_SOCK);
    }
}

void serial_putch(uint8_t ch) {
    uart_putch(ch);
    if (ch == '\n') uart_tx_flush();
}


bool NONNULL onHelp(uint8_t handle, uint8_t *args) {
    if (*args) {
        uint8_t h = cli_cmd_index(args);
        if (h != 0xFF) //cli_writeln_p(handle, (const char*)cliHelps[h]);
            xfprintf_p(&telnet_putch, PSTR("%p\n"), cliHelps[h]);
        else {
            /*cli_pwrite(handle, "No help for '");
            cli_write(handle, args);
            cli_pwriteln(handle, "'");*/
            xfprintf_p(&telnet_putch, PSTR("No help for '%s'\n"), args);
        }
    } else {
        uint8_t i=0;
        const void *c=cliFuncs[i];
        while(c) {
            /*
            cli_write_p(handle, (const char*)cliCommands[i]);
            cli_pwrite(handle, ":  ");
            cli_writeln_p(handle, (const char*)cliHelps[i]);
            */
            xfprintf_p(&telnet_putch, PSTR("\t%p: \t%p\n"), cliCommands[i], cliHelps[i]);
            c = cliFuncs[++i];
        }
    }
    return true;
}

bool onQuit(uint8_t handle, uint8_t UNUSED *args) {
    cli_pwriteln(handle, "Good bye!");
    if (handle < 4) sock_disconnect(handle);
    return true;
}


bool onNTP(uint8_t handle, UNUSED uint8_t *args) {
    //cli_pwriteln(handle, "Try get NTP time");
    xfprintf_p(&telnet_putch, PSTR("Try get NTP time"));
    uint8_t host[] = "pool.ntp.org";
    uint8_t dns[4] = { 10, 0, 0, 1 };
    uint8_t ntpip[4];
    uint32_t tstamp;

    //cli_pwrite(handle, "gethosybyname: ");
    xfprintf_p(&telnet_putch, PSTR("gethostbyname (%s): "), host);
    if (gethostbyname(0, dns, host, ntpip)) {
        /*cli_pwrite(handle, "ok: ");
        cli_write_ip(handle, ntpip);
        cli_pwrite(handle, ", get NTP time ");
        */
        xfprintf_p(&telnet_putch, PSTR("ok: %d.%d.%d.%d, get NTP time "), ntpip[0], ntpip[1], ntpip[2], ntpip[3]);
        if (NTPTime(0, ntpip, &tstamp)) {
            //cli_pwriteln(handle, "ok.");
            xfprintf_p(&telnet_putch, PSTR("ok.\n"));
            Time t;
            gettime(tstamp, &t);

/*            cli_write(handle, "Timestamp: ");
            char s[16];

            ltoa(tstamp, s, 10);
            cli_writeln(handle, s);

            cli_write(handle, word2str(t.year)); cli_write(handle, "-");
            cli_write(handle, zero_pad2(word2str(t.month))); cli_write(handle, "-");
            cli_write(handle, zero_pad2(word2str(t.day))); cli_write(handle, " ");
            cli_write(handle, zero_pad2(word2str(t.hour))); cli_write(handle, ":");
            cli_write(handle, zero_pad2(word2str(t.minute))); cli_write(handle, ":");
            cli_writeln(handle, zero_pad2(word2str(t.second)));
*/
            static const __flash char _c[] = "%p%ld\n";
            static const __flash char _d[] = "%04u-%02u-%02u %02u:%02u:%02u\n";
            xfprintf_p(&telnet_putch, _c, PSTR("Timestamp: "), tstamp);
            //xsprintf(s, "%ld", tstamp);
            //cli_writeln(handle, s);
            xfprintf_p(&telnet_putch, _d, t.year, t.month, t.day, t.hour, t.minute, t.second);
            //cli_writeln(handle, s);

        } else {
            cli_pwriteln(handle, "fail.");
        }
    } else {
        cli_pwriteln(handle, "fail.");
    }

    return true;
}

bool onWOL(uint8_t handle, UNUSED uint8_t *args) {

    return true;
}


uint8_t clibuf[CLI_MAXCMDSIZE+1];
uint8_t cliptr=0;


static const __flash uint8_t _cmd_help[] = "help";
static const __flash uint8_t _cmd_quit[] = "quit";
static const __flash uint8_t _cmd_ntp[] = "ntp";
static const __flash uint8_t _cmd_wol[] = "wol";
const __flash uint8_t * const __flash cliCommands[] = {
       _cmd_help,
       _cmd_quit,
       _cmd_ntp,
       _cmd_wol
};

static const __flash uint8_t _hlp_help[] = "this help";
static const __flash uint8_t _hlp_quit[] = "disconect";
static const __flash uint8_t _hlp_ntp[] = "Get time from pool.ntp.org";
static const __flash uint8_t _hlp_wol[] = "Send WOL packet";
const __flash uint8_t * const __flash cliHelps[] = {
    _hlp_help,
    _hlp_quit,
    _hlp_ntp,
    _hlp_wol
};

CLIFunction * const __flash cliFuncs[] = {
    &onHelp,
    &onQuit,
    &onNTP,
    &onWOL,
    0
};
/************* MAIN CODE ******************/


const __flash W5100_CFG __flash pcfg= {
    { 0x00, 0x08, 0xDC, 0x41, 0x57, 0x30 }, // mac, 00:08:DC == WizNet
    { 10, 0, 0, 101 },
    //{192, 168, 231, 101},
    { 255, 255, 255, 0 },
    //{192, 168, 231, 1}
    { 10, 0, 0, 1 }
};

void setup(void) {
    w5100_init();
    uart_init(38400);

    asm volatile("sei");
    delay100ms(20);

    if (W51_config_p(&pcfg) == W5100_OK) EFlags |= EFL_ETH;

}

// returns true if prompt should be printed
bool onRecv(void) {
    // received packet with erminateing CRLF
    uint16_t rsz = sock_recv_size(TELNET_SOCK);

    while(rsz--) {
        uint8_t c = sock_getch(TELNET_SOCK);
        if (c > 0x40 && c < 0x5B) c += 32; // ASCII LoCase
        if (c == 9) c = ' '; // Tab -> space
        if (c == 0x0D) continue;
        if (c == 0x0A) {
            break;
        }
        if (cliptr <= CLI_MAXCMDSIZE) {
            clibuf[cliptr] = c;
        }
        cliptr++;
    }
    if (cliptr) {
        if (cliptr <= CLI_MAXCMDSIZE) {
            if (cli_command(TELNET_SOCK, clibuf)) cli_pwriteln(TELNET_SOCK, "OK");
            else cli_pwriteln(TELNET_SOCK, "ERROR");

        } else {
            cli_pwrite(TELNET_SOCK, "Bad Command. Max length ");
            cli_write(TELNET_SOCK, (uint8_t *)word2str(CLI_MAXCMDSIZE));
            cli_pwrite(TELNET_SOCK, " chars (");
            cli_write(TELNET_SOCK, (uint8_t *)word2str(cliptr));
            cli_pwriteln(TELNET_SOCK, ")");
        }

        cliptr = 0;
        ZEROVAR(clibuf);
        return true;
    }
    return false; // no data, no prompt print
}

void sock_loop(void) {
    uint16_t rsz;

    switch (sock_status(TELNET_SOCK)) {
        case W5100_SKT_SR_CLOSED:
            uart_pprint("Open TCP socket ");

            EFlags &= ~EFL_TELNET;
            if (sock_open(TELNET_SOCK, W5100_SKT_MR_TCP, TELNET_PORT) != W5100_FAIL) {
                uart_pprint("ok.\n");

                sock_listen(TELNET_SOCK);
            } else {
                uart_pprint("fail.\n");
            }
            break;

        case W5100_SKT_SR_ESTABLISHED:
            if ((EFlags & EFL_TELNET) == 0) {
                uart_pprint("CONNECTED\n");

                cli_pwriteln(TELNET_SOCK, "AVR Station TTY\n\r");
                cli_prompt(TELNET_SOCK);
                EFlags |= EFL_TELNET;
                cliptr = 0;
            }
            if ((rsz=sock_recv_size(TELNET_SOCK))!=0) {
                if (onRecv())
                    cli_prompt(TELNET_SOCK);
            }
            break;

        case W5100_SKT_SR_FIN_WAIT:
        case W5100_SKT_SR_CLOSING:
        case W5100_SKT_SR_TIME_WAIT:
        case W5100_SKT_SR_CLOSE_WAIT:
        case W5100_SKT_SR_LAST_ACK:
            uart_pprint("*CLOSING*\n");

            sock_close(TELNET_SOCK);
            EFlags &= ~EFL_TELNET;
            break;

        default:
            EFlags &= ~EFL_TELNET;
            break;
    }
}

void main(void) {
    setup();
    uart_pprint("= CLI TEST =\n");
    while (!(EFlags & EFL_ETH)) {
        uart_pprint("Eth fail. reinit. \n");
        setup();
        delay100ms(150); // 15s
    }


    for (;;) {
        sock_loop();
        delay1ms(1);
    }

}
