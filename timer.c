// main.c
// AVR project

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include "timer.h"
#include "gcc_macro.h"

volatile Timer_t timer = { 0 };
/*
volatile uint8_t timer_S = 0;
volatile uint8_t timer_M = 0;
volatile uint8_t timer_H = 0;
volatile uint8_t timer_MS = 0;
volatile uint8_t timer_Flags = 0;
*/
volatile uint32_t unix_timestamp = 1387792006UL; // unix timestamp

void init_timer(void) {
    // TIMER1 10ms init
    #ifdef TIMER0_10MS_COUNTER
        TCCR0A = (1<<WGM01) | (0<<WGM00); // CTC mode w/o ext. pin out
        TCCR0B = (0<<WGM12)
                 | (1<<CS02) | (0<<CS01) | (1<<CS00); // Fclk/1204 prescaler, CTC on OCR0A
        OCR0A = 156; // 10ms per toggle (50Hz) compare counter, required vlaue: 156.25
        TCNT0 = 0;
        TIMSK0 |= (1<<OCIE0A); // start timer
    #elif defined(TIMER1_10MS_COUNTER)
        TCCR1A = (0<<WGM11) | (0<<WGM10); // CTC mode w/o ext. pin out
        TCCR1B = (0<<WGM13) | (1<<WGM12)
                 | (1<<CS12) | (0<<CS11) | (0<<CS10); // Fclk/256 prescaler, CTC on OCR1A
        OCR1A = 624; // 10ms per toggle (50Hz) compare counter
        TCNT1 = 0;
        TIMSK1 |= (1<<OCIE1A); // start timer
    #else
        #error No 10ms timer defined
    #endif
}

void restart_timer(void) {
    cli();
    #ifdef TIMER0_10MS_COUNTER
        TCNT0 = 0;
    #else
        TCNT1 = 0;
    #endif
    //ZEROVAR(timer);
    memset((void*)&timer, 0, sizeof(timer));
    sei();
}

// 10ms timer
#ifdef TIMER0_10MS_COUNTER
ISR(TIMER0_COMPA_vect) {
#elif defined(TIMER1_10MS_COUNTER)
ISR(TIMER1_COMPA_vect) {
#endif
    timer.Flags |= TFLAGS_10MS;
    timer.MS++; // 10ms++

    if (timer.MS == 50)
        timer.Flags |= TFLAGS_500MS;

    if (timer.MS >= 100) {
        timer.MS = 0;
        timer.S++;
        unix_timestamp++;
        timer.Flags |= TFLAGS_SEC | TFLAGS_500MS;
    }
    if (timer.S>59) {
        timer.S = 0;
        timer.M++;
        timer.Flags |= TFLAGS_MIN;
    }
    if (timer.M > 59) {
        timer.M = 0;
        timer.H++;
        timer.Flags |= TFLAGS_HOUR;
    }
    if (timer.H > 23) {
        timer.H = 0;
        timer.Flags |= TFLAGS_DAY;
    }
}
