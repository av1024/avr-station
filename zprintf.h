/** Combined printf implementation.
 * Note: zatoi similar to strtok, not atol actually (internal atol less than zatoi by 120bytes)
 *
 * Reimplemented version.
 *
 * - Flash/EEPROM format string location added
 * - Extra '%p' macro for in-Flash strings added
 * - Internal UART/LCD/Ethernet selection via 8bit "socket" number
 *
 * vsprintf code by (C)ChaN, 2011
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 */

/*------------------------------------------------------------------------*/
/* Universal string handler for user console interface  (C)ChaN, 2011     */
/*------------------------------------------------------------------------*/
/*----------------------------------------------*/
/* Formatted string output                      */
/*----------------------------------------------*/
/*  zprintf("%d", 1234);            "1234"
    zprintf("%6d,%3d%%", -200, 5);  "  -200,  5%"
    zprintf("%-6u", 100);           "100   "
    zprintf("%ld", 12345678L);      "12345678"
    zprintf("%04x", 0xA3);          "00a3"
    zprintf("%08LX", 0x123ABC);     "00123ABC"
    zprintf("%016b", 0x550F);       "0101010100001111"
    zprintf("%s", "String");        "String"
    zprintf("%-4s", "abc");         "abc "
    zprintf("%4s", "abc");          " abc"
(+) zprintf("%p", PSTR("String"));  "String"
    zprintf("%c", 'a');             "a"
    zprintf("%f", 10.0);            <zprintf lacks floating point support>
*/

#ifndef __ZPRINTF_H__
#define __ZPRINTF_H__

#include <stdarg.h>

#define _USE_XFUNC_OUT  1   /* 1: Use output functions */
#define _CR_CRLF        0   /* 1: Convert \n ==> \r\n in the output char */

#define _USE_XFUNC_IN   1   /* 1: Use input function */
#define _LINE_ECHO      1   /* 1: Echo back input chars in xgets function */

#define _USE_UART       1   /* 1: Use UART for output */
#define _USE_W5100      1   /* 1: Use W5100 socket for output */
#define _USE_LCD        1   /* 1: Use LCD module for output */
#define _LCD_NO_LF      1   /* 1: Exclude \r from LCD utput even _CR_CRLF enabled */

#define UART_SOCKET 4
#define LCD_SOCKET  5


#if _USE_XFUNC_OUT
#define zdev_out(func) zfunc_out = (void(*)(unsigned char))(func)
extern void (*zfunc_out)(unsigned char);
void zputc (char c);
void zputs (const char* str);
void zfputs (void (*func)(unsigned char), const char* str);

//void xvprintf (unsigned char is_pstr, const char* fmt, va_list arp );

void zprintf (const char* fmt, ...);
void zsprintf (char* buff, const char* fmt, ...);
void zfprintf (void (*func)(unsigned char), const char* fmt, ...);
void zhprintf (unsigned char socket, const char*  fmt, ...);
void zvhprintf (unsigned char socket, const char* fmt, va_list arp);

void zprintf_p (const __flash char* fmt, ...);
void zsprintf_p (char* buff, const __flash char* fmt, ...);
void zfprintf_p (void (*func)(unsigned char), const __flash char* fmt, ...);
void zhprintf_p (unsigned char socket, const __flash char*  fmt, ...);
void zvhprintf_p (unsigned char socket, const __flash char* fmt, va_list arp);

void zprintf_e (const char* fmt, ...);
void zsprintf_e (char* buff, const char* fmt, ...);
void zfprintf_e (void (*func)(unsigned char), const char* fmt, ...);
void zhprintf_e (unsigned char socket, const char*  fmt, ...);
void zvhprintf_e (unsigned char socket, const char* fmt, va_list arp);

void put_dump (const void* buff, unsigned long addr, int len, int width);
#define DW_CHAR     sizeof(char)
#define DW_SHORT    sizeof(short)
#define DW_LONG     sizeof(long)
#endif

#if _USE_XFUNC_IN
#define zdev_in(func) zfunc_in = (unsigned char(*)(void))(func)
extern unsigned char (*zfunc_in)(void);
int zgets (char* buff, int len);
int zfgets (unsigned char (*func)(void), char* buff, int len);
int zatoi (char** str, long* res);
#endif

#endif // __ZPRINTF_H__
