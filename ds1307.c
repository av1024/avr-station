/** DS1307 RTC handling routines
 *
 * Note: Maximum allowed I2C speed 100kHz (i2c standard) by documentation
 * Note2: De facto worked with I2C speed <= 400kHz (i2c fast).
 */

#include <avr/io.h>
#include <util/twi.h>
#include "i2c.h"
#include "ds1307.h"
#include "datetime.h"

#define DS1307_ADDR_W 0b11010000
#define DS1307_ADDR_R 0b11010001


uint8_t byte2bcd(uint8_t val) {
    register uint8_t tmp=0;
    //OPTIMIZED: compiler code has same size
/*
    asm volatile("\n"

                 "L1_%=: " "subi    %1, 10\n\t"
                 "   "     "brcs    L_%=\n\t"
                           "subi    %0, -16\n\t"
                           "rjmp    L1_%=\n\t"
                 "L_%=: "  "subi    %1, -10\n\t"
                           "add     %1,%0\n\t"
                 :"+r" (tmp), "+r" (val)
                 );
    return val;
    */

    while(val > 10) {
        tmp += 0x10;
        val -= 10;
    }
    return tmp + val;

}

uint8_t bcd2byte(uint8_t bcd)
{
   register uint8_t hex = 10;   // set up to be multiplied by MSD

   hex *= (bcd & 0xF0) >> 4;   // set 10s
   hex += (bcd & 0x0F);      // add 1s
   return hex;
}

bool rtc_set_time(uint8_t h, uint8_t m, uint8_t s) {
    bool nret = false;
    if (!i2c_start(DS1307_ADDR_W)) return nret;
    if (i2c_write(0x00)) // addr 0: seconds
        if (i2c_write(byte2bcd(s)))
            if (i2c_write(byte2bcd(m)))
                if (i2c_write(byte2bcd(h) & ~0x40)) nret = true; // +24H bit

    i2c_stop();
    return nret;
}

bool rtc_set_date(uint8_t y, uint8_t m, uint8_t d) {
    bool nret = false;
    if (!i2c_start(DS1307_ADDR_W)) return nret;
    if (i2c_write(0x04)) // addr 4: day
        if (i2c_write(byte2bcd(d)))
            if (i2c_write(byte2bcd(m)))
                if (i2c_write(byte2bcd(y))) nret = true; // 0..99
    i2c_stop();
    return nret;
}

bool rtc_init(void) {
    bool nret = false;
    uint8_t x = 0;
    if (!i2c_start(DS1307_ADDR_W)) return nret;

    nret = true;
    i2c_write(0x00);
    i2c_start(DS1307_ADDR_R);
    i2c(I2C_RECEIVE_NACK);
    x = TWDR;
    x &= 0x7F;
    i2c_stop();

    i2c_start(DS1307_ADDR_W);
    i2c_write(0x00);
    i2c_write(x);   // OSC start (bit 7==0)
    i2c_stop();

    i2c_start(DS1307_ADDR_W);
    i2c_write(0x07);
    i2c_write(0x00); // disable output, pull pin down
    i2c_stop();

    return nret;
}

bool rtc_get_bcd(DS1307_Datetime *data) {
    bool nret = false;
    if (!i2c_start(DS1307_ADDR_W)) return nret;
    if (i2c_write(0x00)) {
        if (i2c_start(DS1307_ADDR_R)) {
            for(uint8_t i=0;i<7;i++) {
                uint8_t st;
                if (i<6) st = i2c(I2C_RECEIVE_ACK);
                else st = i2c(I2C_RECEIVE_NACK);
                if (st != TW_MR_DATA_ACK && st != TW_MR_DATA_NACK) break;
                data->buf[i] = TWDR;
            }
            nret = true;
        }
    }
    i2c_stop();
    return nret;
}

void rtc_bcd2bin(DS1307_Datetime *data) {
    for(uint8_t i=0;i<7;i++) data->buf[i] = bcd2byte(data->buf[i]);
}


bool rtc_time(Time *t) {
    DS1307_Datetime dt;
    if (!rtc_get_bcd(&dt)) return false;
    rtc_bcd2bin(&dt);
    t->year = 2000 + dt.year;
    t->month = dt.month;
    t->day = dt.day;
    t->hour = dt.hour;
    t->minute = dt.minute;
    t->second = dt.second;
    return true;
}
