#include "ds18x20.h"

bool  DS18x20_StartMeasure(void)
{
    //Reset, skip ROM and start temperature conversion
    if (!OW_Reset()) return 0;
    OW_WriteByte(OW_CMD_SKIPROM);
    OW_WriteByte(THERM_CMD_CONVERTTEMP);
    return 1;
}

bool  DS18x20_StartMeasureAddressed(uint8_t* rom)
{
    //Reset and start temperature conversion
    if (!OW_Reset()) return 0;
    OW_MatchROM(rom);
    OW_WriteByte(THERM_CMD_CONVERTTEMP);
    return 1;
}

#ifdef DS18X20_CHECK_CRC
#define CRC8INIT    0x00
#define CRC8POLY    0x18              //0X18 = X^8+X^5+X^4+X^0

static inline uint8_t crc8(uint8_t *data_in, uint16_t number_of_bytes_to_read )
{
    uint8_t   crc;
    uint16_t    loop_count;
    uint8_t   bit_counter;
    uint8_t   data;
    uint8_t   feedback_bit;

    crc = CRC8INIT;

    for (loop_count = 0; loop_count != number_of_bytes_to_read; loop_count++)
    {
        data = data_in[loop_count];

        bit_counter = 8;
        do {
            feedback_bit = (crc ^ data) & 0x01;
            if (feedback_bit==0x01) crc = crc ^ CRC8POLY;

            crc = (crc >> 1) & 0x7F;
            if (feedback_bit==0x01) crc = crc | 0x80;

            data = data >> 1;
            bit_counter--;
        }
        while (bit_counter > 0);
    }
    return crc;
}
#endif

bool  DS18x20_ReadData(uint8_t *rom, uint8_t *buffer)
{
    //Reset, skip ROM and send command to read Scratchpad
    if (!OW_Reset()) return 0;
    if (rom[0]==0)  OW_WriteByte(OW_CMD_SKIPROM);
    else OW_MatchROM(rom);
    OW_WriteByte(THERM_CMD_RSCRATCHPAD);

#ifdef DS18X20_CHECK_CRC
    uint8_t   buff[9] = {1,2,3,4,5,6,7,8,9};
    for (uint8_t i=0; i<9; i++) buff[i] = OW_ReadByte();
    buffer[0] = buff[0]; buffer[1] = buff[1];
    if (crc8(buff, 9)) return 0;    // если контрольная сумма не совпала, возвращаем ошибку
#else
    //Read Scratchpad (only 2 first bytes)
    buffer[0] = OW_ReadByte(); // Read TL
    buffer[1] = OW_ReadByte(); // Read TH
#endif

    return 1;
}

void DS18x20_ConvertToThemperature(uint8_t* data, uint8_t* themp)
{
    //Store temperature integer digits and decimal digits
    //AV: NB! result temperature stored as positive number with character sign in themp[0]
    themp[1] = data[0]>>4;
    themp[1] |= (data[1]&0x07)<<4;
    //Store decimal digits
    themp[2] = (data[0] & 0x0f); 
    //AV: was *= 5, actual: *= 6.25
    //themp[2] *= 6;
    if (data[1]>0xFB){
        themp[1] = 127-themp[1];
        themp[0] = '-';
	if (themp[2]) themp[2] = 16 - themp[2];
	else themp[1]++;
    }
    else if((data[0]==0x00)&&(data[1]==0x00)) themp[0] = ' '; else themp[0] = '+';
    themp[2] = (themp[2] * 10) >> 4; // AV: auto-round to 0.1: 0.51 -> 5

}

DS18x20Value DS18x20_ConvertToTemp(uint8_t* data) {
    DS18x20Value r;
    r.dec = (data[0] >> 4) | ((data[1] & 0x07) << 4);
    r.frac = (data[0] & 0x0F);
    if (data[1] > 0xFB) {
	r.dec = -(127 - r.dec);
	if (r.frac) r.frac = 16 - r.frac;
	else r.dec--;
    }
    r.frac = (r.frac * 10) >> 4; // NB: 0.5 -> 5, 0.12 -> 1
    return r;
}
