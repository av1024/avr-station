// main.c
// AVR project


#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>

#include "gcc_macro.h"
#include "cli.h"

//bool (*cli_print)(const char *str);
//bool (*cli_print_p)(const char *str);


//!returns index of function for command or 0xFF if invalid command
uint8_t cli_cmd_index(uint8_t *cmd) {
    uint8_t i=0;
    const void * c = cliFuncs[i];
    while(c) {
        c = cliFuncs[i];
        if (!c) break;

        if (strcmp_P((const char*)cmd, (const char*)cliCommands[i])==0) return i;
        i++;
    };
    return 0xFF;
}

//
bool cli_command(uint8_t handle, uint8_t *cmdline) {
    uint8_t *args = cmdline;
    while(*args) {
        if (*args==' ') {
            *args++ = 0; // split cmd/arguments if exist
            uint8_t i = cli_cmd_index(cmdline);
            if (i != 0xFF) {
                return cliFuncs[i](handle, args);
            }
            return false;
        }
        args++;
    }
    // cmd w/o args
    uint8_t i = cli_cmd_index(cmdline);
    if (i != 0xFF) {
        return cliFuncs[i](handle, args);
    }
    return false;
}

// INPLACE split cmdline
// Returns actual count of args (may be greater than max_args!)
uint8_t cli_split_args(uint8_t *str, uint8_t *args[], uint8_t delim, uint8_t max_args) {
    register uint8_t cnt=0;
    register uint8_t i=0;
    if (!str || !*str) return cnt;
    args[cnt++] = str;
    while(*str && str[i]) {
        i++;
        if (str[i-1] == delim) {
            if (cnt<max_args) {
                args[cnt] = &str[i];
                str[i-1] = 0;
            }
            cnt++;
        }
    }
    return cnt;
}

