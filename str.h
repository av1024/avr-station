#ifndef __STR_H__
#define __STR_H__

// in-Flash string definitions

static const __flash char ccDOT = '.';
static const __flash char ccSPACE = ' ';
static const __flash char ccZERO = '0';
static const __flash char ccDDOT = ':';
static const __flash char ccALARM = 0xED; // Bell symbol on CYR LCD


static const __flash char csWELCOME[] = "= Meteo Sender =";
static const __flash char csUART_BOOT_WELCOME[] = "\nPress <Enter> to cmdline\n";
static const __flash char csUART_EXIT_CLI[] = "\nEXIT cmdline.";
static const __flash char csCLI_WELCOME[] = "AVR Station TTY\n\r";
static const __flash char csNTP_MAY_OVERRIDE[] = " NOTE: NTP may override changes!\n";
static const __flash char csRESETIP[] = "RESET IP";
static const __flash char csSENS_P[] = "Sens P: ";
static const __flash char csRTC[] = "RTC: ";
static const __flash char csETH[] = "Eth: ";

static const __flash char csOK[] = "OK";
static const __flash char csCANCEL[] = "CANCEL";
static const __flash char csFAIL[] = "FAIL";
static const __flash char csRH[] = "%RH";
static const __flash char csCsp[] = "C ";
static const __flash char csMM[] = "mm";
static const __flash char cs1W[] = "1wire: ";

static const __flash char csCR[] = "\n";
static const __flash char csFMTWORD[] = "%u";
static const __flash char csFMTS[] = "%s";
static const __flash char csFMTP[] = "%p";
static const __flash char csFMTSCR[] = "%s\n";
static const __flash char csFMTPCR[] = "%p\n";
static const __flash char csFMT02X[] = "%02x";

#endif // __STR_H__
