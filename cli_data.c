// command and help definitions

#include <avr/pgmspace.h>

/***********
 * Command names
 ***********/

static const __flash uint8_t _ip[] = "ip";
static const __flash uint8_t _mask[] = "mask";
static const __flash uint8_t _gw[] = "gw";
static const __flash uint8_t _mac[] = "mac";
static const __flash uint8_t _dns[] = "dns";

static const __flash uint8_t _ntp[] = "ntp"; // + "sync", "tz", "host"

static const __flash uint8_t _nm[] = "nm";
//static const __flash uint8_t _nmport[] = "nmport";
static const __flash uint8_t _log[] = "log";
//static const __flash uint8_t _logport[] = "logport";

static const __flash uint8_t _lirc[] = "lirc";
//static const __flash uint8_t _lircport[] = "lircport";

static const __flash uint8_t _time[] = "time";
static const __flash uint8_t _date[] = "date";
static const __flash uint8_t _tstamp[] = "tstamp";

static const __flash uint8_t _nec[] = "nec";

static const __flash uint8_t _mon[] = "mon";
static const __flash uint8_t _sens[] = "sens";
static const __flash uint8_t _quit[] = "quit";
static const __flash uint8_t _reset[] = "reset";
static const __flash uint8_t _help0[] = "help";
static const __flash uint8_t _help[] = "?";

const __flash uint8_t * const __flash cliCommands[] = {
    _ip,
    _mask,
    _gw,
    _mac,
    _dns,

    _ntp,

    _nm,
    _log,
    _lirc,

    _date,
    _time,
    _tstamp,

    _nec,

    _mon,
    _sens,

    _quit,
    _reset,
    _help0,
    _help
};

static const __flash uint8_t _h_ip[] = "[new ip]";
static const __flash uint8_t _h_mask[] = "[new mask]";
static const __flash uint8_t _h_gw[] = "[new gateway]";
static const __flash uint8_t _h_mac[] = "print MAC";
static const __flash uint8_t _h_dns[] = "[new DNS ip]";

static const __flash uint8_t _h_ntp[] = "[sync|host [ip or hostname]|tz [in minutes]]";
static const __flash uint8_t _h_nm[] = "narodmon.ru [ip_or_host:port]";
static const __flash uint8_t _h_log[] = "local nm logger [ip_or_host:port]";
static const __flash uint8_t _h_lirc[] = "UDP lirc dest [ip_or_host:port]";

static const __flash uint8_t _h_date[] = "[new date, format as printed]";
static const __flash uint8_t _h_time[] = "[new time hh:mm:ss]";
static const __flash uint8_t _h_tstamp[] = "print unix timesatamp";

static const __flash uint8_t _h_nec[] = "send NEC IR [hh |rh ]*10, hh-hex cmd, rh- repeat h times";

static const __flash uint8_t _h_mon[] = "(telnet) switch to monitor mode";
static const __flash uint8_t _h_sens[] = "[clr]-clear EEPROM, [ID on|off|nm+|nm-]-edit sensor with ID";

static const __flash uint8_t _h_quit[] = "exit command mode";
static const __flash uint8_t _h_reset[] = "restart device";
static const __flash uint8_t _h_help[] = "this help, [cmd]-help for cmd";

const __flash uint8_t * const __flash cliHelps[] = {
     _h_ip,
     _h_mask,
     _h_gw,
     _h_mac,
     _h_dns,

     _h_ntp,
     _h_nm,
     _h_log,
     _h_lirc,

     _h_date,
     _h_time,
     _h_tstamp,

     _h_nec,

     _h_mon,
     _h_sens,

     _h_quit,
     _h_reset,
//     _h_help,
     _h_help
};
