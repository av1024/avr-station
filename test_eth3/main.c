#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>

#include <stdlib.h>
#include "../gcc_macro.h"
#include "../delay.h"

#include "uart/uart.h"
#include "uart/strutil.h"
#include "eth/w5100ex.h"
#include "eth/ntp.h"
#include "eth/wol.h"
#include "eth/dns.h"

#include "../cli_io.h"
#include "../cli.h"
#include "../timer.h"
#include "../eep.h"

// === MAIN EXTERNS ==

const __flash char cmdPrompt[] = "AVR> ";

#define TELNET_SOCK     1
#define TELNET_PORT     23

uint8_t clibuf[CLI_MAXCMDSIZE+1];
uint8_t cliptr=0;


#define EFL_ETH     0x01    /* ethernet OK */
#define EFL_TELNET  0x02    /* telnet socket connected */
#define EFL_TELMON  0x04    /* telnet socket in monitor mode */
#define EFL_NTP     0x08    /* NTP sync OK */
uint8_t EFlags = 0;

#define SFL_RTC     0x01    /* RTC Present */
#define SFL_BMP     0x02    /* BMP085 valid */
#define SFL_DHT     0x04    /* AM2302 read success */
#define SFL_1W      0x08    /* 1wire devices found */
#define SFL_UARTCMD 0x10    /* UART in CLI mode now */

uint8_t SFlags = 0;

#define UART_CLI_TIMEOUT_MINS   1
uint8_t UART_CLI_Countdown = 0; // If==0 then switch back into "monitor" mode
CLIData cli;

// ==========================

void setup(void) {
    uart_init(57600);
    if (w5100_init() == W5100_OK) {
        if (W51_config_e(&eeETH) == W5100_OK) EFlags |= EFL_ETH;
    }
    init_timer();
    asm volatile("sei");
}

void onMinute(void) {
    if (UART_CLI_Countdown) {
        UART_CLI_Countdown--;
        if (!UART_CLI_Countdown) {
            uart_pprint("\nEXIT CLI MODE\n");
        }
    }
    else {
        SFlags &= ~SFL_UARTCMD;
    }
}

void onSecond(void) {
    if (!(EFlags & EFL_ETH) && (timer.S % 10 == 0)) {
        if (w5100_init() == W5100_OK) {
           if (W51_config_e(&eeETH) == W5100_OK) EFlags |= EFL_ETH;
        }
    }
}


void _log_p(const char *pstr) {
    if (!(SFlags & SFL_UARTCMD)) {
        uart_print_p(pstr);
        uart_println();
    }
    if (EFlags & EFL_TELMON) {
        cli_writeln_p(TELNET_SOCK, pstr);
    }
}

void onSerial(void) {
    while (!uart_rx_empty()) {
        if (SFlags & SFL_UARTCMD) UART_CLI_Countdown = UART_CLI_TIMEOUT_MINS;

        uint8_t c = uart_getch();
        if (c > 0x40 && c < 0x5B) c += 32; // ASCII LoCase
        if (c == 9) c = ' '; // Tab -> space
        switch(c) {
            case 8:
                if (cli.len) {
                    cli.len--;
                    cli.cmdbuf[cli.len]=0;
                    cli_write(UART_SOCK, (uint8_t*)"\x08");
                    cli_flush(UART_SOCK);
                }
                break;

            case '\r':
                if (SFlags & SFL_UARTCMD) {
                    cli.cmdbuf[cli.len]=0;
                    //cli.ready = true;

                    //cli_pwriteln(UART_SOCK, "");
                    cli_write_cr(UART_SOCK);
                } else {
                    SFlags |= SFL_UARTCMD;
                    UART_CLI_Countdown = UART_CLI_TIMEOUT_MINS;
                }
                cli_write_p(UART_SOCK, cmdPrompt);
                cli_flush(UART_SOCK);
                break;

            case '\n':
                break;

            default:
                if (cli.len <= CLI_MAXCMDSIZE) {
                    if (c >= ' ') {
                        cli_putch(UART_SOCK, c);
                        cli.cmdbuf[cli.len++] = c;
                    }
                }
        }
    }

}

//true -> print CLI prompt
bool onTelnetRecv(void) {
    return false;
}

void onEth(void) {
    uint16_t rsz;

    switch (sock_status(TELNET_SOCK)) {
        case W5100_SKT_SR_CLOSED:
            _log_p(PSTR("Open TCP socket "));

            EFlags &= ~EFL_TELNET;
            if (sock_open(TELNET_SOCK, W5100_SKT_MR_TCP, TELNET_PORT) != W5100_FAIL) {
                _log_p(PSTR("ok."));

                sock_listen(TELNET_SOCK);
            } else {
                _log_p(PSTR("fail."));
            }
            break;

        case W5100_SKT_SR_ESTABLISHED:
            if ((EFlags & EFL_TELNET) == 0) {
                _log_p(PSTR("CONNECTED"));

                cli_pwriteln(TELNET_SOCK, "AVR Station TTY\n\r");
                cli_prompt(TELNET_SOCK);
                EFlags |= EFL_TELNET;
                cliptr = 0;
            }
            if ((rsz=sock_recv_size(TELNET_SOCK))!=0) {
                if (onTelnetRecv())
                    cli_prompt(TELNET_SOCK);
            }
            break;

        case W5100_SKT_SR_FIN_WAIT:
        case W5100_SKT_SR_CLOSING:
        case W5100_SKT_SR_TIME_WAIT:
        case W5100_SKT_SR_CLOSE_WAIT:
        case W5100_SKT_SR_LAST_ACK:
            _log_p(PSTR("*CLOSING*"));

            sock_close(TELNET_SOCK);
            EFlags &= ~EFL_TELNET;
            break;

        default:
            EFlags &= ~EFL_TELNET;
            break;
    }
}

void main(void) {
    setup();
    uart_pprint("\nTest combined CLI mode.\nPress <Enter> to enter serial CLI mode.\nor Connect via telnet.\n");
    delay100ms(20);
    for(;;) {
        if (!uart_rx_empty()) onSerial();
        onEth();

        if (timer.Flags & TFLAGS_SEC) onSecond();
        if (timer.Flags & TFLAGS_MIN) onMinute();
        timer.Flags &= ~(TFLAGS_SEC | TFLAGS_MIN);
    }
}
