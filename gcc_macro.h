#ifndef __GCC_MACRO__
#define __GCC_MACRO__

/**
 * Some readability and optimization related defines
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 * Date: 2013
 */

#if !defined(bool)
    typedef unsigned char bool __attribute__((__mode__(__QI__)));
    #define true    1
    #define false   0
#endif

#define INLINE   inline __attribute__((__always_inline__))
#define NOINLINE __attribute__((__noinline__))
#define NONNULL __attribute__((__nonnull__))
#define PURE __attribute__((__pure__))

// https://www.mikrocontroller.net/topic/65923#530326
/*
 Note! May grow optimized code depend on compiler switch
 Usage:
 StructA *ptr = &struct;
 PRELOAD("y", ptr);
 ptr->a ...
*/
#define PRELOAD(reg,var) \
  __asm__ __volatile (";PRELOAD " reg " with " #var : "=" reg (var) : "0" (var))

// avoid -Wunised-variable def
#define UNUSED __attribute__((__unused__))

#define __nop() do {__asm__ __volatile__("nop"); } while(0)

// uint32 a = uint32 b optimized version
#define COPY32BIT(a,b) uint8_t *__x_tmp = (uint8_t *) &a, \
                               *__y_tmp = (uint8_t *) &b; \
                               for(register uint8_t __i_tmp=0;__i_tmp<4;__i_tmp++) *__x_tmp++ = *__y_tmp++;


#define DDR(x) (*(&x - 1))      /* address of data direction register of port x */
#if defined(__AVR_ATmega64__) || defined(__AVR_ATmega128__)
    /* on ATmega64/128 PINF is on port 0x00 and not 0x60 */
    #define PIN(x) ( &PORTF==&(x) ? _SFR_IO8(0x00) : (*(&x - 2)) )
#else
    #define PIN(x) (*(&x - 2))    /* address of input register of port x          */
#endif


#endif // __GCC_MACRO__
