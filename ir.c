/**
 * IR sender/recvceiver base code.
 *
 * IR Sender uses Timer2 and defined by IRSENDxxx pin, Interrupt-driven send
 * IR Receiver uses TImer2 and INT0 (PD2, arduino 2) input, Interrupt-driven receive
 *
 * Auto-switch INT0/Timer2 mode on send command
 * Undef IR_RECEIVE if receiver not used for disable INT0 setup switching
 * Undef IR_SEND if sender code is not used
 *
 * Usage:
 *
 * ir_init();
 * ...
 * ir_start(); // (re-) start receiving
 * for(;;) {
    if (ir_available()) {
        ...

        ir_start(); // clean processed code
    }
    if (SOME_CONDITION) {
        while(ir_busy()) ;
        ir_sendNEC8(0, 0xCC, 0); // ir_start() called internally on send complete
    }
 }
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include "gcc_macro.h"
#include "delay.h"

#include "ir.h"


/* ======= IR Sender ======= */
#ifdef IR_SEND
// 36036Hz @ 16MHz crystal -> 221
#define TIMER2_INIT 221
#define TICK_COUNTER 41


//OPTIMIZED: 0,1,2,_4_ (-2byte of code)
#define DONE 0
#define PREAMBLE 1
#define SEND_BYTE 2
#define WAIT_REPEAT 4
#define SEND_REPEAT 5

typedef struct  {
    uint8_t mode; // FSM mode
    uint8_t nCnt; // 36kHz to quant (562.5us) counter
    uint8_t qCnt; // quant counter
    uint8_t qRep; // repeat counter
    uint8_t qBit; // bit to be send counter
    uint8_t qByte; // byte index counter
    uint8_t buf[4]; // all data to be send
} irsend_t;

static volatile irsend_t irs;
#endif  // IR_SEND

/* ======= IR Receiver ====== */
#ifdef IR_RECEIVE
typedef struct {
    uint8_t started;
    uint8_t ready;
    uint8_t repeated;
    union {
        uint32_t code;
        uint8_t code_bytes[4];
        struct {
            uint16_t code_lo;
            uint16_t code_hi;
        };
    };
    union {
        uint32_t buf;
        uint8_t buf_bytes[4];
    };
} ir_t;

static volatile ir_t ir;
#endif  // IR_RECEIVE

/************************************************/
/*             IR RECEIVER code                 */
/************************************************/
// timer timeout prescaler ~150ms
#define NEC_MAXLEN  20
// bit maxtime counter ~ 1.216ms @ 16MHz/1024 -> "0" level pulse
#define PULSE_THRESH  19
// "repeat" code
#define RPT_THRESH 38
// "start" code
#define START_THRESH 62


#ifdef IR_RECEIVE
ISR(INT0_vect) {
  register uint8_t d;


  if (ir.started) {
    d = TCNT2 - NEC_MAXLEN;
    ir.buf <<= 1;
    if (d > PULSE_THRESH) ir.buf_bytes[0] |= 1; //OPTIMIZED: ir.buf |= 1;

    if (d > RPT_THRESH && d < START_THRESH) {
      ir.repeated = 1;

      //OPTIMIZED: ir.buf = ir.code;
      COPY32BIT(ir.buf, ir.code);

    } else
        ir.repeated = 0;
  }
  else {
    ir.started = 1;
    TIMSK2 = (1<<TOIE2); // start timer
  }

  TCNT2 = NEC_MAXLEN; // Timeout
}

ISR(TIMER2_OVF_vect) {
  register uint8_t d;
  uint8_t *y = (uint8_t*)&ir.buf, *x = (uint8_t*)&ir.code;

  if (ir.buf) {
    ir.ready = 1;

    for(d=0;d<4;d++) { //OPTIMIZED:
      *x++ = *y; //ir.code = ir.buf;
      *y++ = 0; // ir.buf = 0
    }
  }

  ir.started = 0;
  TCNT2 = NEC_MAXLEN;
}


void ir_start(void) {
  ir.ready = 0;
  ir.repeated = 0;

  TCCR2A = 0; // overflow mode
  TCCR2B = (1<<CS20) | (1<<CS21) | (1<<CS22); // clock 1024 -> 64us
  TIMSK2 = (1<<TOIE2); // enable OVF interrupt
  TCNT2 = NEC_MAXLEN;
  EIMSK |= (1<<INT0);  // enable INT0
}

// check for ir receiving in progress
INLINE uint8_t ir_busy(void) {
    return ir.started;
}

// check if any code received
INLINE uint8_t ir_available(void) {
    return ir.ready;
}

// check if last received code is valid NEC code
// address and code with inverted bytes
uint8_t ir_isNEC8(void) {
    if ((ir.code_bytes[0]^ir.code_bytes[1]) != 0xFF) return 0;
    if ((ir.code_bytes[2]^ir.code_bytes[3]) != 0xFF) return 0;
    return 1;
}

// get addres portion of last received code
INLINE uint8_t ir_NEC8addr(void) {
    return ir.code_bytes[3];
}

// get code portion of last received code
INLINE uint8_t ir_NEC8code(void) {
    return ir.code_bytes[1];
}

// is last code is repeat
INLINE uint8_t ir_isRepeat(void) {
    return ir.repeated;
}

// last received code
INLINE uint32_t ir_code(void) {
    return ir.code;
}


// last received code as array
INLINE volatile uint8_t * ir_code_bytes(void) {
    return ir.code_bytes;
}

#endif

void ir_init(void) {
#ifdef IR_SEND
  // output pin
  IRSEND_DDR |= (1<<IRSEND);
#endif
#ifdef IR_RECEIVE
  // setup INT0
  DDRD &= ~(1<<PD2);    // INT0 pin as input
  EICRA |= (1<<ISC01) | (1<<ISC00); // rising edge as interrupt
#endif
}

/************************************************/
/*             IR SENDER code                   */
/************************************************/
#ifdef IR_SEND
// send 8bit cmd to 8bit addr
void ir_sendNEC8(uint8_t addr, uint8_t cmd, uint8_t repeats) {

    TIMSK2 = 0; // disable overflow interrupts
#ifdef IR_RECEIVE
    EIMSK &= ~(1<<INT0); // disable IR receiver pin interrupt
#endif

    //ZEROVAR(irs);
    memset((void*)&irs, 0, sizeof(irs));

    irs.qRep = repeats;
    irs.buf[0] = addr;
    irs.buf[1] = ~addr;
    irs.buf[2] = cmd;
    irs.buf[3] = ~cmd;
    irs.mode = PREAMBLE;

    // (re-)init and start timer

    TCCR2A = (0<<COM2A1) | (0<<COM2A0) // disable HW output OC2A
             | (0<<COM2A1) | (0<<COM2A0)// disable HW output OC2B
             | (1<<WGM21) | (0<<WGM20); // CTC mode, use COMPA ISR
    TCCR2B = (0<<FOC2A) | (0<<FOC2B) // HW outputs not used
             | (0<<WGM22) // CTC mode
             | (1<<CS20) | (0<<CS21) | (0<<CS22); // No clock prescaler, timer started
    TCNT2  = 0;
    OCR2A = TIMER2_INIT; // freq 36036 Hx @ 16MHz crystal
    TIMSK2 = (0<<OCIE1B) | (1<<OCIE1A) // enable compare A INT
             | (0<<TOIE2); // disable overflow interrupt

}

ISR(TIMER2_COMPA_vect) {
    //IRQ triggered every state change
    register uint8_t to_out = 0; // if 1 then toggle output pin
    register uint8_t bitVal;
    static uint8_t bc = 0;

    irs.nCnt++;
    if(irs.nCnt == TICK_COUNTER) { // {1/36036*2} * 40: approx. 562.5us time piece
        irs.nCnt = 0;
        irs.qCnt++;
        bc++;
    }

    switch(irs.mode) {
        case DONE:
            to_out = 0;
            break;

        case PREAMBLE:
            if (irs.qCnt < 16) to_out = 1; // 9ms
            if (irs.qCnt > 23) // 9ms + 4.5ms
            {
                bc = 0;
                irs.mode = SEND_BYTE;
            }
            break;

        case SEND_BYTE:
            bitVal = irs.buf[irs.qByte] & 0x80;
            if (bc < 1) to_out = 1; // 1 tick "up" (~562.5us)
            else
                {
                    if (irs.qByte==4) {
                        irs.mode = WAIT_REPEAT;
                        break;
                    }
                    // in ticks: bc > 1 for "0", bc > 3 for "1"
                    if ((!bitVal && (bc > 1)) || (bitVal && (bc > 3))) {
                        irs.qBit++;
                        bc = 0;
                        //irs.buf[irs.qByte] = irs.buf[irs.qByte] << 1;
                        irs.buf[irs.qByte] <<= 1;
                        if (irs.qBit > 7) {
                            irs.qBit = 0;
                            irs.qByte++;
                        }
                    }
                }
            break;

        case WAIT_REPEAT:
            if (irs.qCnt > 192) { // 108ms from previous sequence start
                irs.qCnt = 0;
                irs.mode = SEND_REPEAT;
                if (!irs.qRep) {
                  irs.mode = DONE;
                  #ifdef IR_RECEIVE
                    ir_start();
                  #endif
                }
                else irs.qRep--;

            }
            break;

        case SEND_REPEAT:
            if (irs.qCnt < 16) to_out = 1;
            if (irs.qCnt == 20) to_out = 1;
            if (irs.qCnt > 20)  irs.mode = WAIT_REPEAT;
            break;

    }
    if (to_out) IRSEND_PIN |= (1<<IRSEND);
    else IRSEND_PORT &= ~(1<<IRSEND);

    //TCNT2 = 0;
}

INLINE uint8_t ir_sendComplete(void) {
    return irs.mode == DONE;
}
#endif  // IR_SEND
