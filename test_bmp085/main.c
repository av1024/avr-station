// test/design code for bmp085 pressure sensor

#include <avr/io.h>
#include <stdlib.h>

#include "gcc_macro.h"
#include "delay.h"
//#include <util/delay.h>

#include "i2c.h"
#include "bmp085.h"

#define TEST
#define DEBUG

#ifdef DEBUG
    #include "uart/uart.h"
    #include "zprintf.h"
    #define log(fmt, ...) zhprintf_p(UART_SOCKET, PSTR(fmt), __VA_ARGS__)
    //extern BMP085_eeprom_t BMP085_eeprom;
    //extern BMP085_data_t BMP085_data;
#endif

#ifdef TEST
void print_int16(int16_t x);
void print_int32(int32_t x);
#endif


#ifdef TEST
/*
void print_int16(int16_t x) {
    char buf[7];
    itoa(x, buf, 10);
    uart_print(buf);
}

void print_int32(int32_t x) {
    char buf[15];
    ltoa(x, buf, 10);
    uart_print(buf);
}

void print_uint16(uint16_t x) {
    char buf[7];
    utoa(x, buf, 10);
    uart_print(buf);
}
*/


void main(void) {
    //uint8_t st;
    uart_init(57600);
    i2c_init();
    bmp085_init();
    asm volatile("sei");

    uart_pprint("I2C BMP085 Test\n");

    uint8_t cc=0;
    //ZEROVAR(BMP085_eeprom);

    for(;;) {
        if (cc) {
            /*
            uart_pprint("\nUncalibrated temp: ");
            int16_t t = bmp085_read_t();
            print_int16(t);
            uart_pprint("\nCalibrated temp: ");
            print_int16(convert_temp(t));
            */
            if (bmp085_readAll()) {
                //log("\nRaw read:\nTemp: %d\nPress: %lu", BMP085_data.ut, BMP085_data.up);
/*
 *              uart_pprint("\nRaw read:\nThempearure: ");
                char c[15];
                itoa(BMP085_data.ut, c, 10);
                uart_print(c);
                uart_pprint("\nPressure: ");
                */
                /*
                uint32_t z = BMP085_data.msb;
                z <<= 16;
                z |= BMP085_data.lsb << 8;
                z |= BMP085_data.xlsb;
                z >>= (8-BMP085_OSS);//(((uint32_t)BMP085_data.msb << 16) + ((uint16_t)BMP085_data.lsb << 8) + BMP085_data.xlsb) >> (8-BMP085_OSS);
                */

                //ultoa(BMP085_data.up, c, 10);
                //uart_print(c);

/*                // MATH TEST VALUES FROM DATASHEET
                BMP085_data.ut = 27898;
                BMP085_data.up = 23843;
*/
                //convert_temp(BMP085_data.ut);
                bmp085_calculate();
                log("\nconverted read:\nTemp: %d\nPress: %lu", BMP085Values.temperature, BMP085Values.pressure);
                /*uart_pprint("Themperature: ");
                uart_print_dec(BMP085Values.temperature / 10);
                uart_putch('.');
                uart_print_dec(BMP085Values.temperature % 10);
                uart_pprint("C\nPressure: ");
                print_int32(BMP085Values.pressure);
                */
                #ifdef BMP085_PRESS_PA
                    uart_pprint(" Pa\n");
                #endif
                #ifdef BMP085_PRESS_HPA
                    uart_pprint(" hPa\n");
                #endif
                #ifdef BMP085_PRESS_MMHG
                    uart_pprint(" mm Hg\n");
                #endif
            }
        } else {
            cc = bmp085_readEEPROM();
            if (cc) {
/*
                uart_pprint("\nEEPROM:\nAC1: ");
                print_int16(BMP085_eeprom.ac1);
                uart_pprint("\nAC2: ");
                print_int16(BMP085_eeprom.ac2);
                uart_pprint("\nAC3: ");
                print_int16(BMP085_eeprom.ac3);
                uart_pprint("\nAC4: ");
                print_uint16(BMP085_eeprom.ac4);
                uart_pprint("\nAC5: ");
                print_uint16(BMP085_eeprom.ac5);
                uart_pprint("\nAC6: ");
                print_uint16(BMP085_eeprom.ac6);
                uart_pprint("\nB1: ");
                print_int16(BMP085_eeprom.b1);
                uart_pprint("\nB2: ");
                print_int16(BMP085_eeprom.b2);
                uart_pprint("\nMB: ");
                print_int16(BMP085_eeprom.mb);
                uart_pprint("\nMC: ");
                print_int16(BMP085_eeprom.mc);
                uart_pprint("\nMD: ");
                print_int16(BMP085_eeprom.md);
*/
                uart_pprint("\nEEPROM read ok.\n");
/*                 // MATH TEST VALUES FROM DATASHEET
                BMP085_eeprom.ac1 = 408;
                BMP085_eeprom.ac2 = -72;
                BMP085_eeprom.ac3 = -14383;
                BMP085_eeprom.ac4 = 32741;
                BMP085_eeprom.ac5 = 32757;
                BMP085_eeprom.ac6 = 23153;
                BMP085_eeprom.b1 = 6190;
                BMP085_eeprom.b2 = 4;
                BMP085_eeprom.mb = 32768;
                BMP085_eeprom.mc = -8711;
                BMP085_eeprom.md = 2868;
*/
            } else
                uart_pprint("EEPROM read fail.\n");

        }

        //uart_putch('.');
        delay100ms(50);
    }

}
#endif
