// cli_io.c

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <string.h>
#include <stdarg.h>

#include "uart/strutil.h"
#include "cli_io.h"
#include "eth/ntp.h"
#include "lcd.h"
#include "datetime.h"
#include "str.h"

#define UART_SOCKET 4
#define LCD_SOCKET  5

static const __flash char cliPrompt[] = "AVR> ";


void cli_putch(uint8_t handle, uint8_t ch) {
    if (handle < UART_SOCKET) sock_put(handle, &ch, 1);
    else if (handle == UART_SOCKET) uart_putch(ch);
        else if (handle == LCD_SOCKET) lcd_putc(ch);
}

void cli_write(uint8_t handle, const uint8_t *str) {
    //cli_pprintf(handle, "%s", str);
    while(*str) cli_putch(handle, *str++);
}

void cli_write_p(uint8_t handle, const __flash char *str) {
    //cli_pprintf(handle, "%p", str);
    // -10bytes
    uint8_t c;
    while((c=*str++)) cli_putch(handle, c);
}

void cli_write_e(uint8_t handle, const uint8_t *estr) {
    uint8_t c;
    while((c=eeprom_read_byte(estr++)))
        cli_putch(handle, c);
}

void cli_writeln(uint8_t handle, const uint8_t *str) {
    //cli_write(handle, str);
    //cli_write_p(handle, CRLF);
    cli_printf_p(handle, csFMTSCR, str);
    cli_flush(handle);
}

void cli_writeln_p(uint8_t handle, const __flash char *str) {
    cli_write_p(handle, str);
    cli_write_p(handle, csCR);
    //cli_pprintf(handle, "%p\n", str);
    cli_flush(handle);
}

void cli_write_cr(uint8_t handle) {
    cli_write_p(handle, csCR);
    cli_flush(handle);
}

void cli_flush(uint8_t handle) {
    if (handle < 4) {
        sock_put_done(handle);
        sock_do_send(handle);
    } else {
        uart_tx_flush();
    }
}

void cli_prompt(uint8_t handle) {
    //cli_write_p(handle, cliPrompt);
    cli_printf_p(handle, csFMTP, cliPrompt);
    cli_flush(handle);
}

void cli_write_ip(uint8_t handle, const uint8_t *ip) {
    //cli_pprintf(handle, "%u.%u.%u.%u", ip[0], ip[1], ip[2], ip[3]);
    for(uint8_t i=0;i<4;i++) {
        cli_printf_p(handle, csFMTWORD, ip[i]);
        if (i<3) cli_putch(handle, '.');
    }
}

void cli_write_ip_e(uint8_t handle, const uint8_t *ip) {
    //cli_pprintf(handle, "%u.%u.%u.%u", ip[0], ip[1], ip[2], ip[3]);
    for(uint8_t i=0;i<4;i++) {
        cli_printf_p(handle, csFMTWORD, eeprom_read_byte(&ip[i]));
        if (i<3) cli_putch(handle, '.');
    }
}

void cli_write_mac(uint8_t handle, const uint8_t *mac) {
    for(uint8_t i=0;i<6;i++) {
        cli_printf_p(handle, csFMT02X, mac[i]);
        if (i < 5) cli_putch(handle, '-');
    }
}

void cli_write_mac_e(uint8_t handle, const uint8_t *mac) {
    for(uint8_t i=0;i<6;i++) {
        cli_printf_p(handle, csFMT02X, eeprom_read_byte(&mac[i]));
        if (i < 5) cli_putch(handle, '-');
    }
}

void cli_write_date(uint8_t handle, const Time *t) {
    #if DATE_FORMAT==DATE_YMD
        cli_pprintf(handle, "%u%c", t->year, DATE_SEPARATOR);
        cli_pprintf(handle, "%02u%c%02u ", t->month, , DATE_SEPARATOR, t->day);
    #elif DATE_FORMAT==DATE_DMY
        cli_pprintf(handle, "%02u%c%02u", t->day, DATE_SEPARATOR, t->month);
        cli_pprintf(handle, "%c%u ", DATE_SEPARATOR, t->year);
    #elif DATE_FORMAT==DATE_MDY
        cli_pprintf(handle, "%02u%c%02u", t->month, DATE_SEPARATOR, t->daye);
        cli_pprintf(handle, "%c%u ", DATE_SEPARATOR, t->year);
    #else
        #error No date format specified
    #endif
}

void cli_write_time(uint8_t handle, const Time *t) {
    cli_pprintf(handle, "%02u:%02u:%02u ",
                t->hour,
                t->minute,
                t->second);
}

void cli_write_datetime(uint8_t handle, const Time *t) {
    /*cli_pprintf(handle, "%u-%02u-%02u %02u:%02u:%02u",
                t->year,
                t->month,
                t->day,
                t->hour,
                t->minute,
                t->second);
                */
    /*
    cli_pprintf(handle, "%u-", t->year);
    cli_pprintf(handle, "%02u-%02u ", t->month, t->day);
    cli_pprintf(handle, "%02u:%02u:%02u ",
                t->hour,
                t->minute,
                t->second);
    */
    cli_write_date(handle, t);
    cli_write_time(handle, t);
}

void _log(const void* str) {
    /*uint8_t b[2];
    b[0] = (SFlags & SFL_UARTCMD) ? 0xFF : UART_SOCKET;
    b[1] = (EFlags & EFL_TELMON) ? TELNET_SOCKET : 0xFF;
    for(uint8_t i=0;i<2;i++) {
        cli_write_datetime(b[i], &dt);
        cli_pprintf(b[i], " %s\n", str);
        cli_flush(b[i]);
    }*/

    if (!(SFlags & SFL_UARTCMD)) {
        cli_write_datetime(UART_SOCKET, &dt);
        cli_printf_p(UART_SOCKET, csFMTSCR, str);
        cli_flush(UART_SOCKET);
    }
    if (EFlags & EFL_TELMON) {
        cli_write_datetime(TELNET_SOCKET, &dt);
        cli_printf_p(TELNET_SOCKET, csFMTSCR, str);
        cli_flush(TELNET_SOCKET);
    }

}

void _log_p(const void* pstr) {
    /*uint8_t b[2];
    b[0] = (SFlags & SFL_UARTCMD) ? 0xFF : UART_SOCKET;
    b[1] = (EFlags & EFL_TELMON) ? TELNET_SOCKET : 0xFF;
    for(uint8_t i=0;i<2;i++) {
        cli_write_datetime(b[i], &dt);
        cli_pprintf(b[i], " %p\n", pstr);
        cli_flush(b[i]);
    }*/

    if (!(SFlags & SFL_UARTCMD)) {
        cli_write_datetime(UART_SOCKET, &dt);
        cli_printf_p(UART_SOCKET, csFMTPCR, pstr);
        cli_flush(UART_SOCKET);
    }
    if (EFlags & EFL_TELMON) {
        cli_write_datetime(TELNET_SOCKET, &dt);
        cli_printf_p(TELNET_SOCKET, csFMTPCR, pstr);
        cli_flush(TELNET_SOCKET);
    }

}

//FIXME: test generated size when used
void _log_mac_p(const void* pstr, const uint8_t *mac) {
    uint8_t b[2];
    b[0] = (SFlags & SFL_UARTCMD) ? 0xFF : UART_SOCKET;
    b[1] = (EFlags & EFL_TELMON) ? TELNET_SOCKET : 0xFF;
    for(uint8_t i=0;i<2;i++) {
        cli_write_datetime(b[i], &dt);
        cli_pprintf(b[i], " %p ", pstr);
        cli_write_mac(b[i], mac);
        cli_write_p(b[i], csCR);
        cli_flush(b[i]);
    }
    /*
    if (!(SFlags & SFL_UARTCMD)) {
        cli_write_datetime(UART_SOCKET, &dt);
        cli_pprintf(UART_SOCKET, " %p ", pstr);
        cli_write_mac(UART_SOCKET, mac);
        cli_putch(UART_SOCKET, '\n');
        cli_flush(UART_SOCKET);
    }
    if (EFlags & EFL_TELMON) {
        cli_write_datetime(TELNET_SOCKET, &dt);
        cli_pprintf(TELNET_SOCKET, " %p ", pstr);
        cli_write_mac(TELNET_SOCKET, mac);
        cli_putch(TELNET_SOCKET, '\n');
        cli_flush(TELNET_SOCKET);
    }
    */
}

void _log_byte_mac_p(const void* pstr, uint8_t x, const uint8_t *mac) {
    /*
    if (!(SFlags & SFL_UARTCMD)) {
        cli_write_datetime(UART_SOCKET, &dt);
        cli_printf_p(UART_SOCKET, pstr, x);
        cli_write_mac(UART_SOCKET, mac);
        cli_putch(UART_SOCKET, '\n');
        cli_flush(UART_SOCKET);
    }
    if (EFlags & EFL_TELMON) {
        cli_write_datetime(TELNET_SOCKET, &dt);
        cli_printf_p(TELNET_SOCKET, pstr, x);
        cli_write_mac(TELNET_SOCKET, mac);
        cli_putch(TELNET_SOCKET, '\n');
        cli_flush(TELNET_SOCKET);
    }
    */
    uint8_t b[2];
    b[0] = (SFlags & SFL_UARTCMD) ? 0xFF : UART_SOCKET;
    b[1] = (EFlags & EFL_TELMON) ? TELNET_SOCKET : 0xFF;
    for(uint8_t i=0;i<2;i++) {
        cli_write_datetime(b[i], &dt);
        cli_printf_p(b[i], pstr, x);
        cli_write_mac(b[i], mac);
        cli_putch(b[i], '\n');
        cli_flush(b[i]);
    }
}

void _logf(const char *fmt, ...) {
    va_list arp;

    if (!(SFlags & SFL_UARTCMD)){
        cli_write_datetime(UART_SOCKET, &dt);
        va_start(arp, fmt);
        zvhprintf(UART_SOCKET, fmt, arp);
        va_end(arp);
        cli_putch(UART_SOCKET, '\n');
        cli_flush(UART_SOCKET);
    }
    if (EFlags & EFL_TELMON) {
        cli_write_datetime(TELNET_SOCKET, &dt);
        va_start(arp, fmt);
        zvhprintf(TELNET_SOCKET, fmt, arp);
        va_end(arp);
        cli_putch(TELNET_SOCKET, '\n');
        cli_flush(TELNET_SOCKET);
    }
}

void _logf_p(const __flash char *fmt, ...) {

    va_list arp;

    if (!(SFlags & SFL_UARTCMD)){
        cli_write_datetime(UART_SOCKET, &dt);
        va_start(arp, fmt);
        zvhprintf_p(UART_SOCKET, fmt, arp);
        va_end(arp);
        cli_putch(UART_SOCKET, '\n');
        cli_flush(UART_SOCKET);
    }
    if (EFlags & EFL_TELMON) {
        cli_write_datetime(TELNET_SOCKET, &dt);
        va_start(arp, fmt);
        zvhprintf_p(TELNET_SOCKET, fmt, arp);
        va_end(arp);
        cli_putch(TELNET_SOCKET, '\n');
        cli_flush(TELNET_SOCKET);
    }

}
