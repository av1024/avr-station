/** I2C simple custom implementation
 *
 * Based on varyous code from i-net ^)
 */

#include <avr/io.h>
#include <util/twi.h>
#include <util/delay.h>
#include "i2c.h"


void i2c_init(void) {
    I2C_DDR &= ~((1<<I2C_SDA) | (1<<I2C_SCL));
    #ifdef I2C_INTERNAL_PULLUPS
        I2C_PORT |= (1<<I2C_SDA) | (1<<I2C_SCL);
    #endif

    // set bitrate
    TWSR = I2C_TWSR;
    TWBR = I2C_TWBR;
}

uint8_t i2c(uint8_t action){
    switch(action){
        case I2C_START:
        case I2C_RESTART:
            TWCR = (1<<TWSTA) | (1<<TWEN) | (1<<TWINT);// Если нужно прерывание | (1<<TWIE);
            break;
        case I2C_STOP:
            TWCR = (1<<TWSTO) | (1<<TWEN) | (1<<TWINT);// | (1<<TWIE);
            break;
        case I2C_TRANSMIT:
            TWCR = (1<<TWEN) | (1<<TWINT);// | (1<<TWIE);
            break;
        case I2C_RECEIVE_ACK:
            TWCR = (1<<TWEN) | (1<<TWINT) | (1<<TWEA);//| (1<<TWIE);
            break;
        case I2C_RECEIVE_NACK:
            TWCR = (1<<TWEN) | (1<<TWINT);// | (1<<TWIE);
            break;
        }

    #ifdef I2C_SAFE_WAIT
        if (action != I2C_STOP)
            for(uint8_t i=0;i<255;i++) {
                if (TWCR & (1<<TWINT)) return (TWSR & 0xF8);
                _delay_us(2);
            }
        return 0; // BUS_FAIL
    #else
        if (action != I2C_STOP) while (!(TWCR & (1<<TWINT)));
        return (TWSR & 0xF8);
    #endif

}

bool i2c_start(uint8_t address) {
    uint8_t st;
    st = i2c(I2C_START);
    // start condition
    if ((st != TW_START) && (st != TW_REP_START)) return 0;

    // send address
    TWDR = address;
    st = i2c(I2C_TRANSMIT);
    if ( (st != TW_MT_SLA_ACK) && (st != TW_MR_SLA_ACK) ) return 0;
    return 1;
}

void i2c_stop(void) {
    i2c(I2C_STOP);
}

bool i2c_write(uint8_t data) {
    TWDR = data;
    if (i2c(I2C_TRANSMIT) != TW_MT_DATA_ACK) return 0;

   return 1;
}

/*
bool i2c_read_buf(uint8_t addr, uint8_t *buf, uint8_t count) {
    if (!i2c_start(addr)) return 0;
    for(uint8_t i=0;i<count;i++) {
        uint8_t st;
        if (i<count-1) st = i2c(I2C_RECEIVE_ACK);
        else st = i2c(I2C_RECEIVE_NACK);
        if (st != TW_MR_DATA_ACK && st != TW_MR_DATA_NACK) return 0;
        buf[i] = TWDR;
    }
    return 1;
}
*/

uint8_t i2c_readAck(void) {
    i2c(I2C_RECEIVE_ACK);
    return TWDR;
}

uint8_t i2c_readNAck(void) {
    i2c(I2C_RECEIVE_NACK);
    return TWDR;
}

uint16_t i2c_read_word(uint8_t addr) {
    uint8_t msb, lsb;
    uint16_t data;

    if (!i2c_start(addr)) return 0;
    if (i2c(I2C_RECEIVE_ACK) != TW_MR_DATA_ACK) return 0;
    msb = TWDR;
    if (i2c(I2C_RECEIVE_NACK) != TW_MR_DATA_NACK) return 0;
    lsb = TWDR;
    data = (uint16_t)msb << 8 | lsb;

    return data;
}
