//cli_funcs.c

#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <avr/wdt.h>

#include "gcc_macro.h"
#include "delay.h"
#include "cli.h"
#include "datetime.h"
#include "eep.h"
#include "cli_io.h"
#include "cli_funcs.h"
#include "timer.h"
#include "ds1307.h"
#include "ir.h"
#include "str.h"


#define Reset_AVR() wdt_enable(WDTO_4S); while(1) {}

uint8_t UART_CLI_countdown = 0; // 0: switch UART CLI to "monitor" mode

bool _dummy(uint8_t handle, UNUSED uint8_t *args) {
    cli_pwriteln(handle, "NOT IMPLEMENTED");
    return true;
}

void _selfhelp(uint8_t handle, const void*ptr) {
    uint8_t i=0;
    const void *c=cliFuncs[i];
    while(c) {
        if (c==ptr) {
            cli_pwrite(handle, " ");
            cli_write_p(handle, (char*)cliCommands[i]);
            cli_pwrite(handle, " ");
            cli_writeln_p(handle, (char*)cliHelps[i]);
            break;
        }
        c = cliFuncs[++i];
    }
}

// hex str to byte
uint8_t x2i(const char*s) {
    uint8_t b=0;

    if(isxdigit(*s)) b = toupper(*s++) - '0';
    else return b;
    if (b > 9) b -= 7;
    if (isxdigit(*s)) {
        uint8_t c = toupper(*s++) - '0';
        b = b << 4 | ((c > 9) ? c - 7 : c);
    }
    return b;
}


void _upd_ip(uint8_t handle, uint8_t *text_ip, void *ee_ip) {
    uint8_t ip[4];
        if (host2ip(text_ip, ip)) {
            eeprom_update_block(&ip, ee_ip, 4);
            cli_pwrite(handle, "EEPROM updated.\nAfter restart ");
        } else {
            cli_pwrite(handle, "Not a valid IPv4.\nCurrent ");
    }
}

bool _update_host_port(uint8_t handle, uint8_t *args, void *ee_host, void*ee_port) {
    bool hok = true;
    uint16_t port;
    uint8_t *hostport[2];
    uint8_t i = cli_split_args(args, hostport, ':', 2); // host:port

    if (i != 2) {
        cli_pwrite(handle, "host:port required\n");
        return false;
    }

    port = (uint16_t)atoi((char*)hostport[1]);

    if (EFlags & EFL_ETH) {
        for(uint8_t j=0;j<3;j++) {
            uint8_t host[MAX_HOSTNAME+1];
            uint8_t ip[4];
            memcpy(host, hostport[0], MAX_HOSTNAME+1);
            if (hostbyname(host, ip)) {
                if (ip_valid(ip) == W5100_OK) {
                    hok = true;
                    break;
                }
            }
        }
    } else {
        cli_pwriteln(handle, "WARN: Host can't be checked.");
        hok = true;
    }
    if (!hok || port==0) {
        cli_pwriteln(handle, "Invalid host/port");
        return false;
    }

    eeprom_update_block(hostport[0], ee_host, MAX_HOSTNAME+1);
    eeprom_write_word(ee_port, port);
    return true;
}


bool _str2date(uint8_t *str, Time *t) {
    int16_t y,m,d;
    uint8_t *dd[3];
    uint8_t i = cli_split_args(str, dd, DATE_SEPARATOR, 3);
    if (i != 3) return false;
    #if DATE_FORMAT==DATE_YMD
        y = atoi((const char*)dd[0]);
        m = atoi((const char*)dd[1]);
        d = atoi((const char*)dd[2]);
    #elif DATE_FORMAT==DATE_DMY
        y = atoi((const char*)dd[2]);
        m = atoi((const char*)dd[1]);
        d = atoi((const char*)dd[0]);
    #elif DATE_FORMAT==DATE_MDY
        y = atoi((const char*)dd[2]);
        m = atoi((const char*)dd[0]);
        d = atoi((const char*)dd[1]);
    #else
        #error DATE_FORMAT undefined (see datetime.h)
    #endif
    if (y<2013 || m<1 || m>12 || d<1) return false;
    if (d>DayOfMonth[m-1]) return false;
    t->year = y;
    t->month = m;
    t->day = d;
    return true;
}

bool _str2time(uint8_t *str, Time *t) {
    int16_t h,m,s;
    uint8_t *dd[3];
    uint8_t i = cli_split_args(str, dd, ':', 3);
    if (i>3 || i<2) return false;
    h = atoi((const char*)dd[0]);
    m = atoi((const char*)dd[1]);
    if (i==2) s = t->second;
    else s = atoi((const char*)dd[2]);
    if (h<0 || h>23 || m<0 || m>59 || s<0 || s>59) return false;
    t->hour = h;
    t->minute = m;
    t->second = s;
    return true;
}

bool NONNULL _onIP(uint8_t handle, uint8_t *args) {
    if (*args) { // set new IP
        _upd_ip(handle, args, &eeETH.ip_addr[0]);
    }
    //cli_pwrite(handle, "IP: ");
    cli_write_ip_e(handle, &eeETH.ip_addr[0]);
    return true;
}

bool NONNULL _onMASK(uint8_t handle, uint8_t *args) {
    if (*args) { // set new IP
        _upd_ip(handle, args, &eeETH.sub_mask[0]);
    }
    //cli_pwrite(handle, "MASKIP: ");
    cli_write_ip_e(handle, &eeETH.sub_mask[0]);
    return true;
}

bool NONNULL _onGW(uint8_t handle, uint8_t *args) {
    if (*args) { // set new IP
        _upd_ip(handle, args, &eeETH.gtw_addr[0]);
    }
    //cli_pwrite(handle, "MASKIP: ");
    cli_write_ip_e(handle, &eeETH.gtw_addr[0]);
    return true;
}

//TODO: set new MAC
bool NONNULL _onMAC(uint8_t handle, uint8_t *args) {
    if (*args) {
        _dummy(handle, args);
    }
    cli_write_mac_e(handle, &eeETH.mac_addr[0]);
    return true;
}

bool NONNULL _onDNS(uint8_t handle, uint8_t *args) {
    if (*args) { // set new IP
        _upd_ip(handle, args, eeDNS);
    }
    //cli_pwrite(handle, "MASKIP: ");
    cli_write_ip_e(handle, eeDNS);
    return true;
}


bool NONNULL _onNTP(uint8_t handle, uint8_t *args) {
    uint8_t *ss[2];
    uint8_t i;
    if (*args) {
        i = cli_split_args(args, ss, ' ', 2);
        if (i > 2) return false;
        if (strcmp_P((const char*)ss[0], PSTR("sync"))==0) {
            EFlags &= ~EFL_NTP;
            return true;
        }
        if (strcmp_P((const char*)ss[0], PSTR("host"))==0) {
            if (i==2) {
                _dummy(handle, args);
                //return true;
            }
            cli_write_e(handle, eeNTP);
            return true;
        }
        if (strcmp_P((const char*)ss[0], PSTR("tz"))==0) {
            if (i==2) {
                int16_t _tz = atoi((char*)ss[1]);
                eeprom_write_word((uint16_t*)&eeNTPOFFSET, (uint16_t)_tz);
            }
            int16_t _tz = (int16_t)eeprom_read_word((uint16_t*)&eeNTPOFFSET);
            cli_pprintf(handle, "%d", _tz);
            return true;
        }
    } else {
        //cli_pwrite(handle, "sync, host or tz");
        _selfhelp(handle, &_onNTP);
        return false;
    }
    return false;
}

bool NONNULL _onNM(uint8_t handle, uint8_t *args) {
    if (*args) {
        if (!_update_host_port(handle, args, eeNARODMON, &eeNARODMON_PORT)) return false;
    }
    cli_write_e(handle, eeNARODMON);
    cli_pprintf(handle, ":%u", eeprom_read_word(&eeNARODMON_PORT));
    return true;
}


bool NONNULL _onLOG(uint8_t handle, uint8_t *args) {
    if (*args) {
        if (!_update_host_port(handle, args, eeLOGGER, &eeLOGGER_PORT)) return false;
    }
    cli_write_e(handle, eeLOGGER);
    cli_pprintf(handle, ":%u", eeprom_read_word(&eeLOGGER_PORT));
    return true;
}

bool NONNULL _onLIRC(uint8_t handle, uint8_t *args) {
    if (*args) {
        if (!_update_host_port(handle, args, eeLIRC, &eeLIRC_PORT)) return false;
    }
    cli_write_e(handle, eeLIRC);
    cli_pprintf(handle, ":%u", eeprom_read_word(&eeLIRC_PORT));
    return true;
}


bool NONNULL _onDATE(uint8_t handle, uint8_t *args) {
    if (*args) {
        if (_str2date(args, &dt)) {
            cli_pwrite(handle, "Set date ");
            unix_timestamp = mktime(&dt) - 60L*(int16_t)eeprom_read_word((uint16_t*)&eeNTPOFFSET);
            rtc_set_date(dt.year-2000, dt.month, dt.day);
            if (EFlags & EFL_ETH)
                cli_write_p(handle, csNTP_MAY_OVERRIDE);
        } else
            return false;
    }
    cli_write_date(handle, &dt);
    return true;
}

bool NONNULL _onTIME(uint8_t handle, uint8_t *args) {
    if (*args) {
        if (_str2time(args, &dt)) {
            cli_pwrite(handle, "Set time ");
            unix_timestamp = mktime(&dt) - 60L*(int16_t)eeprom_read_word((uint16_t*)&eeNTPOFFSET);
            rtc_set_time(dt.hour, dt.minute, dt.second);
            if (EFlags & EFL_ETH)
                cli_write_p(handle, csNTP_MAY_OVERRIDE);
        }
    }
    cli_write_time(handle, &dt);

    return true;
}

bool NONNULL _onTSTAMP(uint8_t handle, UNUSED uint8_t *args) {
    cli_pprintf(handle, "%ld", unix_timestamp);
    return true;
}


// nec hh [hh] [rh]
// hh - hex code
// rh - repeat count (h - hex)
// max 10 hh or rh
bool NONNULL _onNEC(uint8_t handle, uint8_t *args) {
    char *hh[10];
    if (*args) {
        uint8_t cnt = cli_split_args(args, (uint8_t**)hh, ' ', 10);
        if (cnt > 9) cnt = 9;
        for(uint8_t i=0;i<cnt;i++) {
            if (toupper(*hh[i])=='R') return false;
            uint8_t c = x2i(hh[i]);
            uint8_t r = 0;
            if (i<(cnt-1)) {
                if (toupper(hh[i+1][0])=='R') {
                    r = x2i(&hh[i+1][1]);
                    i++;
                }
            }
            while(ir_busy()) ;
            ir_sendNEC8(0, c, r);
            //cli_pprintf(handle, "sent x%02x %d repeat(s)\n", c, r);
            delay100ms(2);
        }
        return true;
    }
    //cli_pwrite(handle, "hex code(s) required");
    _selfhelp(handle, &_onNEC);
    return false;
}

bool NONNULL _onMON(uint8_t handle, uint8_t UNUSED *args) {
    if (handle >= UART_SOCKET) {
        cli_pwriteln(handle, "Only for telnet");
        return false;
    }
    cli_pwrite(handle, "Monitor mode. ");
    cli_writeln_p(handle, csUART_BOOT_WELCOME);
    EFlags |= EFL_TELMON;
    return true;
}

bool NONNULL _onSENS(uint8_t handle, uint8_t *args) {
    char *ss[2];
    uint8_t i;
    if (*args) {
        /*
        sens clr - clear EEPROM & set all present sensors as new
        sens id on|off - enable/disable sensor
        sens id nm+|nm- - flag naronmon
         */
        i = cli_split_args(args, (uint8_t**)ss, ' ', 2);
        if (i==1 && strcmp_P(ss[0], PSTR("clr"))==0) {
            for(uint8_t i=0;i<SENSOR_COUNT;i++) {
                SensorData *x = &sensors[i];
                eeprom_update_byte(&eeSENSORS[i].id, 0);
                x->id |= SENS_FLAG_NEW;
            }
            cli_pwriteln(handle, "Sensor EEPROM clear");
            return true;
        } else
            if (i==2) {
                uint8_t id = atoi(ss[0]);
                SensorData *x = find_sensor_by_id(id);
                if (x) {
                    if (strcmp_P(ss[1], PSTR("on"))==0) {
                        x->flags |= SENS_FLAG_ON;
                        store_sensor(x);
                        return true;
                    }
                    if (strcmp_P(ss[1], PSTR("off"))==0) {
                        x->flags &= ~SENS_FLAG_ON;
                        store_sensor(x);
                        return true;
                    }
                    if (strcmp_P(ss[1], PSTR("nm+"))==0) {
                        x->flags |= SENS_FLAG_NAROD;
                        store_sensor(x);
                        return true;
                    }
                    if (strcmp_P(ss[1], PSTR("nm-"))==0) {
                        x->flags &= ~SENS_FLAG_NAROD;
                        store_sensor(x);
                        return true;
                    }
                }
            }

        _selfhelp(handle, &_onSENS);
        return false;
    } else {
        // print all sensors
        // ID in_sensors flags mac
        cli_pwriteln(handle, "id  mac    present   flags  value");
        for(uint8_t i=0;i<SENSOR_COUNT;i++) {
            SensorID id;
            SensorData *x;
            uint8_t f;
            eeprom_read_block(&id, &eeSENSORS[i], sizeof(SensorID));
            if (!id.id) continue; // empty
            f = id.flags;
            x = find_sensor_by_mac(id.mac);

            cli_pprintf(handle, "%02u ", id.id);
            cli_write_mac(handle, id.mac);
            if (x) {
                cli_pwrite(handle, " + ");
                f = x->flags;
            } else {
                cli_pwrite(handle, " - ");
            };
            if (f & SENS_FLAG_OK) cli_pwrite(handle, "k");
            else cli_pwrite(handle, ".");
            if (f & SENS_FLAG_ON) cli_pwrite(handle, "o");
            else cli_pwrite(handle, ".");
            if (f & SENS_FLAG_NAROD) cli_pwrite(handle, "n");
            else cli_pwrite(handle, ".");

            if (x)
                cli_pprintf(handle, " %d.%u", x->val.intg, x->val.frac);
            cli_pwrite(handle, "\n");
        }
        cli_pwriteln(handle, " Flags:\n k: Ok\n o: On\n n: narodmon.ru on");
    }
    return true;
}

bool NONNULL _onHelp(uint8_t handle, uint8_t *args) {
    if (*args) {
        #ifdef CLI_USE_HELP
            uint8_t h = cli_cmd_index(args);
            if (h != 0xFF) {
                cli_write_p(handle, (const char*)cliCommands[h]);
                cli_pwrite(handle, " ");
                cli_writeln_p(handle, (const char*)cliHelps[h]);
            }
            else {
                cli_pprintf(handle, "No help for '%s'\n", args); //OPT: less code than 3-x cli_write
            }
        #else
            cli_pwriteln(handle, "No help compiled.");
        #endif
    } else {
        uint8_t i=0;
        const void *c=cliFuncs[i];
        while(c) {
            #ifdef CLI_USE_HELP
                _selfhelp(handle, c);
            #else
                cli_pwrite(handle, " ");
                cli_writeln_p(handle, (const char*)cliCommands[i]);
            #endif
            c = cliFuncs[++i];
        }
    }
    return true;
}

bool NONNULL _onQuit(uint8_t handle, uint8_t UNUSED *args) {
    cli_pwriteln(handle, "Good bye!");
    if (handle < UART_SOCKET) sock_disconnect(handle);
    if (handle == UART_SOCKET) UART_CLI_countdown = 0;
    return true;
}

bool NONNULL _onReset(uint8_t handle, uint8_t UNUSED *args) {
    cli_pwriteln(handle, "RESTART");
    Reset_AVR();
    return true;
}

CLIFunction * const __flash cliFuncs[] = {

    &_onIP, // _ip
    &_onMASK, // mask
    &_onGW, // gw
    &_onMAC, // mac
    &_onDNS, // dns

    &_onNTP, // ntp

    &_onNM, // nm host:port
    &_onLOG, // log host:port

    &_onLIRC, // lirc host:port

    &_onDATE, // date
    &_onTIME, // time
    &_onTSTAMP, // tstamp

    &_onNEC, // nec

    &_onMON, // telnet monitor mode
    &_onSENS, // show/change sensor flags

    &_onQuit,
    &_onReset,
    &_onHelp,
    0
};
