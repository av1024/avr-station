Multi-control station
=====================

Atmega328 (arduino) with ethernet shield and various sensors.

Planning:

 1. [ok] 1-wire DS18B20
 2. [ok] RTC (DS1307, i2c)
 3. [ok] AM2302 (DHT22)
 4. [ok] BMP085 (i2c)
 5. [ok] IR receiver TSOP31236
 6. [ok] IR sender (NEC proto)
 7. [ok] LCD 4x16 (HD44780-based) (raw)
 8. Push-buttons for LCD control (shield board for LCD)

 9. Send logging to narodmon.ru via arduino ethernet shield (W5100)
 10. [partial] Send WOL from IR button
 11. Auto LED strip control via IR
 ...
 12. Send/receive something via UDP
 13. Translate IR into UDP for some remote lirc (??? for Synology lirc)
 14. [partial] CLI via UART/UDP
 15. [ok] NTP time query/sync
 16. [ok] DNS support

Third-party libraries used:

 - LCD by Peter Fleury [link](http://jump.to/fleury) because ~40 bytes overhead on simple puts
 - AM2302 (DHT22) via funkytransistor's AVR Freaks post [link](http://www.avrfreaks.net/index.php?name=PNphpBB2&file=viewtopic&p=974797)
 - w5100.h header for register i/o
 - onewire.h extended/updated version of [link](http://kibermaster.net/rabota-s-shinoy-1-wire-podklyuchenie-termodatchika-ds18b20-k-avr/)
 - DS18x20.h may be based on same site as onewire.h but with fixes/updates/extensions

Arduino pin (atmega pin) assignment:

 - D0 /RX/    (PD0) - UART RX, Reset IP on boot
 - D2 /INT0/  (PD2) - IR TSOP312x input
 - D3 /INT1/  (PD3) - LCD bit4, pushbutton multiplex
 - D4         (PD4) - LCD bit5, pushbutton multiplex
 - D5         (PD5) - LCD bit6, pushbutton multiplex
 - D6         (PD6) - LCD bit7, pushbutton multiplex
 - D7         (PD7) - LCD E
 - D8         (PB0) - LCD RW
 - D9         (PB1) - LCD RS
 - D10 /SS/   (PB2) - Ethernet shield
 - D11 /MOSI/ (PB3) - Ethernet shield
 - D12 /MISO/ (PB4) - Ethernet shield
 - D13 /SCK/  (PB5) - Ethernet shield

 - A0         (PC0) - LCD LED output
 - A1         (PC1) - AM2302 input
 - A2         (PC2) - 1-wire
 - A3         (PC3) - IR LED output
 - A4 /SDA/   (PC4) - DS1307, BMP085 SDA
 - A5 /SCL/   (PC5) - DS1307, BMP085 SCL

 I2C bus speed: 400kHz (max for DS1307)
