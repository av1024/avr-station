#ifndef __DS1307_H__
#define __DS1307_H__

/**
 * Dallas DS1307 interface functions.
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 * Hardware: ATMega328p
 * Date: 2013
 */

#include "gcc_macro.h"
#include "datetime.h"

typedef union {
    struct {
        uint8_t second;
        uint8_t minute;
        uint8_t hour;
        uint8_t wkday;
        uint8_t day;
        uint8_t month;
        uint8_t year;
    };
    uint8_t buf[7];
} DS1307_Datetime;

uint8_t byte2bcd(uint8_t val);
uint8_t bcd2byte(uint8_t bcd);
bool rtc_set_time(uint8_t h, uint8_t m, uint8_t s);
bool rtc_set_date(uint8_t y, uint8_t m, uint8_t d);
bool rtc_init(void);
bool rtc_get_bcd(DS1307_Datetime *data);
void rtc_bcd2bin(DS1307_Datetime *data);

bool rtc_time(Time *t);

#endif // __DS1307_H__
