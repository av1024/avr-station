#ifndef __SENSORS_H__
#define __SENSORS_H__

// common sensor handling routines

#include <avr/io.h>
#include "onewire.h"


//NOTE: abs(SUM(avg)) SHOULD be less than 32767 because 16-bit accumulator
#define SENS_AVG_16BIT
#define SENS_AVG_SIZE   10
#define SENS_AVG_INVALID    0x5A    /* 0x5A5A as invlaid value */
#define SENS_AVG_INVALID_VALUE  0x5A5A
#define SENS_BMP_BASE   700     /* store -127..127 offset to avoid 32bit math */

#define SENS_FLAG_OK    0x01    /* sensor is OK/VALID */
#define SENS_FLAG_ON    0x02    /* sensor enabled */
#define SENS_FLAG_NAROD 0x04    /* sensor should be sent to narodmon.ru */
#define SENS_FLAG_NEW   0x08    /* sensor is not in EEPROM (for 1-wire) */

#define SENS_DHT_ID_T   1
#define SENS_DHT_ID_RH  2
#define SENS_BMP_ID_T   3
#define SENS_BMP_ID_P   4
#define SENS_1W_MIN_ID  5

#define SENS_DHT_MAC_T      { 'A', 'M', 0x23, 0x02, 0x00, 0x01 }
#define SENS_DHT_MAC_RH     { 'A', 'M', 0x23, 0x02, 0x00, 0x02 }
#define SENS_BMP_MAC_T      { 'B', 'M', 'P', 0x85, 0x00, 0x01 }
#define SENS_BMP_MAC_P      { 'B', 'M', 'P', 0x85, 0x00, 0x02 }

// AVG direction
#define SENS_DIR_UP         0xD9 /* ^ */
#define SENS_DIR_DOWN       0xDA /* v */
#define SENS_DIR_NONE       ' '

#define SENSOR_COUNT    (4 + OW_MAXDEVICES)

typedef struct {
    int16_t intg;
    uint8_t frac;
} SensorValue;

typedef struct {
    uint8_t id;
    uint8_t flags;  // hi_nibble==previous, lo_nibble==current
    uint8_t mac[6];
} SensorID;

typedef struct {
    //SensorID; // -fms-extensions in CFLAGS required
    uint8_t id;
    uint8_t flags;  // hi_nibble==previous, lo_nibble==current
    uint8_t mac[6];
    SensorValue val;
    int16_t avg[SENS_AVG_SIZE];
} SensorData;


extern SensorData sensors[SENSOR_COUNT];
extern uint8_t ow_cnt;
extern uint8_t ow_ids[OW_MAXDEVICES][OW_ROMCODE_SIZE];

SensorData * add_sensor(uint8_t id, uint8_t *mac);
SensorData * find_sensor_by_id(uint8_t id);
SensorData * find_sensor_by_mac(uint8_t *mac);
void update_sensor(SensorData *x, int16_t intg, uint8_t frac);
void update_avg(SensorData *x); // update AVG[] based on last sensor value
SensorValue avg_sensor(SensorData *x);
uint8_t dir_sensor(SensorData *x); // return SENS_DIR_xxx based on x->avg[] data.

bool load_sensor(SensorData *x); // Try load settings from EEPROM by MAC if exists
bool store_sensor(SensorData *x); // Try update/add settings to EEPROM if has room

void update_AM2302(void);
void update_BMP(void);
void update_DS18x20(void);

#endif  // __SENSORS_H__
