// main.c
// AVR project
// LCD + keyboard multiplexing test
// buttons connected in series with 10k resistor from DB4..DB7 to ground
// DDR(LCD_PORT) &= ~(LCD_DATA_MASK) required before button sensing

#include <avr/io.h>
#include <util/delay.h>

#include "gcc_macro.h"
#include "delay.h"
#include "lcd.h"


void setup(void) {
    lcd_init(LCD_DISP_ON);
    //lcd_putch('!');
    //lcd_putch('@');
    lcd_puts("hello\n1:\n2:");

    asm volatile ("sei");
}

inline uint8_t dec2asc(uint8_t n) {
    return n | 0x30;
}

void lcd_print_dec(uint8_t n, bool lead_zero) {
    uint8_t h=0,d=0;
    while(n>9) {
        d++;
        n-=10;
        if (d==10) {
            h++;
            d=0;
        }
    }
    if (lead_zero) {
        if (!d && !h) lcd_putc('0');
    }
    if (h) lcd_putc(dec2asc(h));
    if (d || (!d && h)) lcd_putc(dec2asc(d));
    lcd_putc(dec2asc(n));
}

void main(void) {
    uint8_t h=0,m=0,s=0;
    uint16_t ms=0;
    uint8_t b0=0, b1=0;
    uint8_t c=0;
    setup();

    for(;;) {
        ms++;
        delay1ms(1);


        DDR(LCD_DATA0_PORT) &= ~(1<<LCD_DATA0_PIN);
        DDR(LCD_DATA1_PORT) &= ~(1<<LCD_DATA1_PIN);

        LCD_DATA0_PORT &= ~(1<<LCD_DATA0_PIN);
        LCD_DATA1_PORT &= ~(1<<LCD_DATA1_PIN);


        if (PIN(LCD_DATA0_PORT) & (1<<LCD_DATA0_PIN)) {
            if (b0 < 50) b0++;
        } else b0 = 0;

        if (PIN(LCD_DATA1_PORT) & (1<<LCD_DATA1_PIN)) {
            if (b1 < 50) b1++;
        } else b1 = 0;


        if (ms == 1 || ms == 501) {
            // process "buttons"
            lcd_gotoxy(3,1);
            if (b0 == 50) {
                c++;
                lcd_putc('1');

            } else lcd_putc('0');

            lcd_gotoxy(3,2);
            if (b1 == 50) {
                c--;
                lcd_putc('1');

            } else lcd_putc('0');

            // print "symbol
            lcd_gotoxy(11,0);
            lcd_puts("   ");
            lcd_gotoxy(11,0);
            lcd_print_dec(c,0);
            lcd_putc(' ');
            lcd_gotoxy(15,0);
            lcd_putc(c);


            // print "clock"
            lcd_gotoxy(0,3);
            lcd_print_dec(h,true);
            lcd_putc(':');
            lcd_print_dec(m,true);
            if (ms == 1) lcd_putc(':'); else lcd_putc(' ');
            lcd_print_dec(s,true);
        }

        // "Clock" update
        if (ms==1000) {
            s++;
            if (s > 59) {
                s = 0;
                m++;
            }
            if (m > 59) {
                m = 0;
                h++;
            }
            if (h > 23) h= 0;
            ms = 0;
        }
    }
}
