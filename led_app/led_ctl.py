#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test Telnet app for remote control IR sender
"""
import sys
import os
import logging
from argparse import ArgumentParser  # python 2.7+
import socket
import time

from utils import setupcon

log = None

AVR_IP  = "10.0.0.101"
AVR_PORT = 23
BUF_SIZE = 512

LED_PWR = "02"
LED_DYI1 = "30" # minimal Green
LED_DYI2 = "B0" # minimal Red
LED_DYI3 = "70" # minimal Blue
LED_DYI4 = "10" # minimal White
LED_DYI5 = "90" # minimal Orange (3xRed+1Green)
LED_DYI6 = "50" # moderate Orange (13R+4G)

LED_R_UP = "28"
LED_R_DN = "08"
LED_G_UP = "A8"
LED_G_DN = "88"
LED_B_UP = "68"
LED_B_DN = "48"

LED_ORANGE = "0A"
LED_B = "A2"

LED_REPEAT_15 = "rF"

def connect():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((AVR_IP, AVR_PORT))
        time.sleep(0.25)
        tmp = s.recv(BUF_SIZE)
        log.debug("Connect: %s", tmp)
        return s
    except:
        log.exception("Conection failed")
        return None

def disconnect(sock):
    log.debug("Send 'quit'")
    sock.send("\n")
    time.sleep(0.5)
    tmp = sock.recv(BUF_SIZE)
    sock.send("quit\n")
    time.sleep(0.5)
    sock.close()

def nec(sock, code):

    sock.send("nec %s\n" % (code,))
    tmp = ""
    for z in xrange(10):
        time.sleep(0.1)
        tmp += sock.recv(BUF_SIZE)
        if ("AVR>" in tmp):
            break
    tmp = tmp.replace("AVR>","").strip()
    log.debug("NEC: %s => %s", code, tmp)
    if ("OK" in tmp): return True

    sock.send("nec %s\n" % (code,))
    tmp = ""
    for z in xrange(10):
        time.sleep(0.1)
        tmp += sock.recv(BUF_SIZE)
        if ("AVR>" in tmp):
            break
    tmp = tmp.replace("AVR>","").strip()
    log.debug("NEC(resend): %s => %s", code, tmp)
    if ("OK" in tmp): return True
    return False

def go_sleep(sock):
    log.debug("Sleep")
    nec(sock, LED_ORANGE)
    nec(sock, LED_DYI3)
    nec(sock, "%s %s" % (LED_R_DN, LED_REPEAT_15))
    nec(sock, "%s %s" % (LED_R_DN, LED_REPEAT_15))
    nec(sock, "%s %s" % (LED_G_DN, LED_REPEAT_15))
    nec(sock, "%s %s" % (LED_G_DN, LED_REPEAT_15))
    nec(sock, "%s %s" % (LED_B_DN, LED_REPEAT_15))
    nec(sock, "%s %s" % (LED_B_DN, LED_REPEAT_15))
    nec(sock, LED_B_DN)
    nec(sock, LED_G_DN)
    nec(sock, LED_R_DN)
    nec(sock, LED_B_UP)
    nec(sock, LED_DYI3)
    nec(sock, LED_PWR)

def go_morning(sock):
    """Morning sun imitation"""
    DEF_SLEEP = 60
    CMD = "68 68 a8 a8 28 28 28 28 28 28 28 28 a8 a8 a8 48 48 28 28 28 28 28 28 a8 a8 28 28 28 a8 a8 28 28 28 a8 a8 28 28 28 28 28 28 a8 a8 a8 a8 a8 28 28 28 28 28 68 68 68 68 68 68 68 12 12 12 12 12 22"
    log.debug("Morning")
    nec(sock, LED_PWR)
    nec(sock, LED_DYI3)
    
    for c in CMD.split(" "):
        #log.debug("C: %s", c)
        time.sleep(DEF_SLEEP)
        nec(sock, c)
        
    return

    for i in xrange(3):
        time.sleep(DEF_SLEEP)
        nec(sock, LED_B_UP)
        # B: 4 of 32

    for i in xrange(5):
        time.sleep(DEF_SLEEP)
        nec(sock, LED_R_UP)
        # R: 5 of 32

    for i in xrange(5):
        time.sleep(DEF_SLEEP)
        nec(sock, LED_G_UP)
        # G: 5 of 32

def main(args):

    s = connect()
    try:
        if args.sleep:
            go_sleep(s)
            return

        if args.nec:
            tmp = ("%s00" % (args.nec,))[0:2]
            log.info("Send NEC '%s'", tmp)
            nec(s, tmp)
            return

        if args.morning:
            go_morning(s)
            return

    finally:
        if s:
            disconnect(s)

if __name__ == "__main__":
    parser = ArgumentParser(description="LED CMD SENDER")

    #TODO: add other options here

    setupcon.add_log_options(parser)

    #TODO: add other options here
    parser.add_argument("--sleep", action="store_true", help="Power off sequence")
    parser.add_argument("--nec", metavar="HEX", type=str, help="Send HEX code via NEC proto")
    parser.add_argument("--morning", action="store_true", help="Morning sun sequence")

    args = parser.parse_args()
    log = logging.getLogger()

    setupcon.setup_logging(args, '')


    # === end setup logging ==
    main(args)
