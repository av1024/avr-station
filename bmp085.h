#ifndef __BMP085_H__
#define __BMP085_H__

/* BMP085 handling lib.
 *
 * Usage:
 *
 *  Change pin definitions and pressure units in this file and in i2c.h if needed
 *
 *  #include "i2c.h"
 *  #include "bmp085.h"
 *
 *  // setup
 *  i2c_init();
 *  bmp085_init();
 *
 *  sei();
 *  bmp085_readEEPROM();
 *
 *  // main loop
 *  if (bmp085_readAll()) {
 *    bmp085_calculate();
 *    ...
 *    print(BMP085_Values.temperature / 10, '.', BMP085_Values.temperature % 10)
 *    print(BMP085_Values.pressure)
 *  }
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 * Hardware: ATMega328p
 * Date: 2013
 */


#include "gcc_macro.h"

// OSS==2 uses less code than others
/* OSS values
  0: ultra low power, no oversampling, 4.5ms conversion max, 0.06hPa noise, +12 bytes of code
  1: standard, 2x oversampling, 7.5ms conversion max, 0.05hPa noise, +28 bytes of code
  2: high resolution, 4x oversampling, 13.5ms conversion max, 0.04hPa noise, +0 bytes of code
  3: ultra high resolution, 8x oversampling, 25.5ms conversion max, 0.03hPa noise, +8 bytes of code
*/
#define BMP085_OSS          2
#define BMP085_OSS_DELAY    14

// Define if EOC pin is connected for minify sampling time
//#define BMP085_EOC  PC0
#ifdef BMP085_EOC
    #define BMP085_EOC_PORT PORTC
    #define BMP085_EOC_PIN PINC
    #define BMP085_EOC_DDR DDRC
#endif

// Define BMP085_PRESS_MMHG for auto-conversion Pa to mm Hg
// NOTE! BMP085_PRESS_PA implies 32bit variable to store
#define BMP085_PRESS_MMHG
//#define BMP085_PRESS_HPA
//#define BMP085_PRESS_PA

// Comment out to disable Pressure fration part (save 16 bytes of code)
#define BMP085_PRESS_FRAC

typedef struct {
    struct {
        int8_t intg;
        uint8_t frac;
    } t; // temperature
    struct {
        #ifdef BMP085_PRESS_PA
            uint32_t intg; // in Pa
        #else
            #if defined(BMP085_PRESS_HPA) || defined(BMP085_PRESS_MMHG)
                #ifdef BMP085_PRESS_FRAC
                    uint16_t intg; // in hPa or mmHg depend on defines
                    uint8_t frac;
                #else
                    union {
                        uint16_t intg; // in hPa or mmHg depend on defines
                        uint8_t frac;
                    };
                #endif
            #else
                #error "No pressure storage units defined"
            #endif
        #endif
    } p; // pressure
} BMP085_Values_t;

typedef struct {
    union {
        struct {
            int16_t ac1;
            int16_t ac2;
            int16_t ac3;
            uint16_t ac4;
            uint16_t ac5;
            uint16_t ac6;
            int16_t b1;
            int16_t b2;
            int16_t mb;
            int16_t mc;
            int16_t md;
        };
        uint8_t buf[22];
        uint16_t buf16[11];
    };
} BMP085_eeprom_t;

typedef struct {
    int16_t ut;
    uint32_t up;
} BMP085_data_t;

extern BMP085_Values_t BMP085Values;

void bmp085_init(void);
bool bmp085_readEEPROM(void);
bool bmp085_startMeasureT(void); // initiate measuremets
bool bmp085_startMeasureP(void);
void bmp085_waitMeasureT(void); // wait for measure complete via EOC or hardcoded delay
void bmp085_waitMeasureP(void);
void bmp085_receiveT(void); // complete measurement readings
void bmp085_receiveP(void);
bool bmp085_readAll(void);  // read and wait T and P
void bmp085_calculate(void); // convert raw data to readable form
bool bmp085_valid(void); // check validity/presence of sensor via EEPROM 0x0000 and 0xFFFF values

#endif
