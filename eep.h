#ifndef __EEP_H__
#define __EEP_H__

// EEPROM data definitions

#include <avr/io.h>
#include <avr/eeprom.h>

#include "gcc_macro.h"
#include "eth/w5100ex.h"
#include "datetime.h"
#include "sensors.h"

#define MAX_HOSTNAME   20

// == functions ==
//! write default IP values into EEPROM
void reset_ip(void);


// NIC setup:
/*
extern EEMEM uint8_t eeMAC[6]; // = { 0x00, 0x08, 0xDC, 0x41, 0x57, 0x30}; // 00:08:DC == WizNet
extern EEMEM uint8_t eeIP[4]; // = { 169, 254, 1, 1 }; // default IP
extern EEMEM uint8_t eeNETMASK[4]; // = { 255, 255, 255, 0 };
extern EEMEM uint8_t eeGW[4]; // = { 169, 254, 1, 1 };
*/
extern EEMEM W5100_CFG eeETH;

// NTP setup
extern EEMEM uint8_t eeNTP[MAX_HOSTNAME+1]; // = { 0, 0, 0, 0 };
extern EEMEM int16_t eeNTPOFFSET;
//extern EEMEM uint8_t eeNTP2[4]; // = { 0, 0, 0, 0 };

extern EEMEM uint8_t eeDNS[4]; // 8.8.8.8

// Narodmon/logger setup
extern EEMEM uint8_t eeNARODMON[MAX_HOSTNAME+1]; // narodmon.ru: {91, 122, 49, 168} and/or { 94, 19, 113, 221 }
extern EEMEM uint16_t eeNARODMON_PORT; // = 8283;
extern EEMEM uint8_t eeLOGGER[MAX_HOSTNAME+1]; // = { 0, 0, 0, 0 }; // local network logger like narodmon.ru
extern EEMEM uint16_t eeLOGGER_PORT; // = 8283;

// External Lirc
extern EEMEM uint8_t eeLIRC[MAX_HOSTNAME+1]; // = { 0, 0, 0, 0 };
extern EEMEM uint16_t eeLIRC_PORT; // = 8765;

//TODO: add other constants here

// Sensors MAC
extern EEMEM SensorID eeSENSORS[SENSOR_COUNT]; // Sensors ID, Flags and MAC

extern EEMEM uint8_t eeWOL_IP[4];
extern EEMEM uint8_t eeWOL_MAC[6];

// === functions ===

// try parse hostaddr text as IP address (111.222.000.111)
bool host2ip(uint8_t *hostaddr, uint8_t *ip);

// get IP for specified host. try host2ip, then via DNS
bool hostbyname(const uint8_t *host, uint8_t *ip);
bool hostbyname_e(const uint8_t *eeHost, uint8_t* ip); // same but for host in EEPROM

void localtime_e(uint32_t time, Time *t);

#endif  // __EEP_H__
