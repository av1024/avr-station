// main.c
#include <avr/io.h>
#include "../delay.h"
#include "../uart/uart.h"
#include "../ir.h"


// comment out for Logger mode
#define SENDER

void setup(void) {
    uart_init(38400);
    ir_init();

    asm("sei");
}

void main(void) {
    setup();
    uart_pprint("\nTEST IR\n");


#ifndef SENDER
    ir_start();
    for(;;) {
        if (ir_available()) {
            if (ir_isNEC8()) {
                uart_pprint("\nNEC[");
                uart_print_hex(ir_NEC8addr());
                uart_pprint("]: ");
                uart_print_hex(ir_NEC8code());
                uint8_t *b = ir_code_bytes();
                uart_pprint(" BITS: ");
                uart_print_bin(b[3]);
                uart_putch(' ');
                uart_print_bin(b[2]);
                uart_putch(' ');
                uart_print_bin(b[1]);
                uart_putch(' ');
                uart_print_bin(b[0]);

                if (ir_isRepeat()) {
                    uart_putch('*');
                }
                if (ir_NEC8code()==0xF0) {
                    uart_pprint("\nSending 0xF0...\n");
                    ir_sendNEC8(0, 0xF0, 0);
                    while (!ir_sendComplete()) delay10ms(1);
                }
            } else {
                //uint32_t c = ir_code();
                uint8_t *b = ir_code_bytes();
                b += 3;
                uart_pprint("\nIR: ");
                uart_print_hex(*b--); // 3
                uart_print_hex(*b--); // 2
                uart_print_hex(*b--); // 1
                uart_print_hex(*b);   // 0
                uart_pprint(" XOR: ");
                uart_print_hex(b[0]^b[1]);
                uart_print_hex(b[2]^b[3]);
                uart_pprint(" BITS: ");
                uart_print_bin(b[3]);
                uart_putch(' ');
                uart_print_bin(b[2]);
                uart_putch(' ');
                uart_print_bin(b[1]);
                uart_putch(' ');
                uart_print_bin(b[0]);
                //uart_putch(' ');
            }

            ir_start();
        }

        delay10ms(1);
    }
#endif // !SENDER
    for(;;) {
        ir_sendNEC8(0, 0x2C, 2);
        delay100ms(10);

    }

}
