// datetime utils

#include <avr/io.h>
#include <avr/eeprom.h>
#include "gcc_macro.h"
#include "datetime.h"

#define FIRSTYEAR 1970

const __flash uint8_t __flash DayOfMonth[12] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

void gettime( uint32_t sec, Time *t )
{
  uint16_t day;
  uint8_t year;
  uint8_t dyy;
  uint8_t leap400;
  uint8_t month;

  t->second = sec % 60;
  sec /= 60;
  t->minute = sec % 60;
  sec /= 60;
  t->hour = sec % 24;
  day = sec / 24;

  year = FIRSTYEAR % 100;           // 0..99
  leap400 = 4 - ((FIRSTYEAR - 1) / 100 & 3);    // 4, 3, 2, 1

  for(;;){
    dyy = 0;
    if( (year & 3) == 0 ){
      dyy = 1;
      if( year == 0 || year == 100 || year == 200 ) // 100 year exception
        if( --leap400 )                 // 400 year exception
          dyy = 0;
    }

    if (day < (uint16_t)(365+dyy)) break;

    day -= 365+dyy;
    year++;                 // 00..136 / 99..235
  }
  t->year = year + FIRSTYEAR / 100 * 100;   // + century

  if( !(dyy & 1) && day > 58 )       // no leap year and after 28.2.
    day++;                  // skip 29.2.

  for( month = 1; day >= DayOfMonth[month-1]; month++ )
    day -= DayOfMonth[month-1];

  t->month = month;             // 1..12
  t->day = day + 1;             // 1..31
}

NONNULL PURE uint32_t mktime(const Time *t) {
  uint32_t ts;
  uint16_t ndays = 0;
  uint16_t dayofyear; //OPTIMIZED: don't change to 8bit (+2 bytes)
  uint8_t year = FIRSTYEAR % 100;
  uint8_t leap400 = 4 - ((FIRSTYEAR - 1) / 100 & 3);
  uint8_t yr = t->year - FIRSTYEAR / 100 * 100; // count of...

  for(;;) {
    dayofyear = 365;
    if( (year & 3) == 0 ){
      dayofyear = 366;                  // leap year
      if( year == 0 || year == 100 || year == 200 ) // 100 year exception
        if( --leap400 )                 // 400 year exception
          dayofyear = 365;
    }
    if (year >= yr) break;
    ndays += dayofyear;
    year++;
  }

  if (dayofyear & 1 && t->month > 2) ndays--; // 28.02 fix

  for(uint8_t month = 1; month < t->month; month++ )
    ndays += DayOfMonth[month-1];

  ndays += t->day - 1; // 0-based day

  ts = (uint32_t)ndays * 24; //OPTIMIZED squence
  ts += t->hour;
  ts *= 60;
  ts += t->minute;
  ts *= 60;
  ts += t->second;

  return ts;
}

void localtime(uint32_t time, Time *t, int16_t tz_offset_min) {
    return gettime(time+(60L*tz_offset_min), t);
}
