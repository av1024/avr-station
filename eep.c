// eep.c: EEPROM data definitions

#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>

#include <stdlib.h>
#include "gcc_macro.h"
#include "eep.h"
#include "eth/w5100ex.h"
#include "eth/dns.h"
#include "cli.h"

#include "uart/uart.h"
#include "uart/strutil.h"

// NIC setup:
/*
EEMEM uint8_t eeMAC[6] = { 0x00, 0x08, 0xDC, 0x41, 0x57, 0x30}; // 00:08:DC == WizNet
EEMEM uint8_t eeIP[4] = { 169, 254, 1, 1 }; // default IP
EEMEM uint8_t eeNETMASK[4] = { 255, 255, 255, 0 };
EEMEM uint8_t eeGW[4] = { 169, 254, 1, 1 };
*/

EEMEM W5100_CFG eeETH = {
    { 0x00, 0x08, 0xDC, 0x41, 0x57, 0x30}, // 00:08:DC == WizNet
    { 169, 254, 1, 1 }, // default IP
    { 255, 255, 255, 0 }, // netmask
    { 169, 254, 1, 1 } // gateway
};

EEMEM uint8_t eeDNS[4] = { 8, 8, 8, 8 }; // default DNS is Google

// NTP setup
EEMEM uint8_t eeNTP[MAX_HOSTNAME+1] = "pool.ntp.org"; // time.ooonet.ru
EEMEM int16_t eeNTPOFFSET = 240;    // UTC to Local in minutes
//EEMEM uint8_t eeNTP2[4] = { 85, 21, 78, 91 }; // ground.corbina.net

// Narodmon/logger setup
//EEMEM uint8_t eeNARODMON_IP[4] = { 91, 122, 49, 168 }; // narodmon.ru: {91, 122, 49, 168} and/or { 94, 19, 113, 221 }
EEMEM uint8_t eeNARODMON[MAX_HOSTNAME+1] = "narodmon.ru";
EEMEM uint16_t eeNARODMON_PORT = 8283;
//EEMEM uint8_t eeLOGGER[4] = { 0, 0, 0, 0 }; // local network logger like narodmon.ru
EEMEM uint8_t eeLOGGER[MAX_HOSTNAME+1] = "0.0.0.0";
EEMEM uint16_t eeLOGGER_PORT = 8283;

// External Lirc
//EEMEM uint8_t eeLIRC_IP[4] = { 0, 0, 0, 0 };
EEMEM uint8_t eeLIRC[MAX_HOSTNAME+1] = "0.0.0.0";
EEMEM uint16_t eeLIRC_PORT = 8765;


// Sensors. implementation in sensors.c
EEMEM SensorID eeSENSORS[SENSOR_COUNT];

static const __flash W5100_CFG eeDEFAULT = {
    { 0x00, 0x08, 0xDC, 0x41, 0x57, 0x30},
    { 169, 254, 1, 1 },
    { 255, 255, 255, 0 },
    { 169, 254, 1, 1 }
};

// write default IP values into EEPROM
void reset_ip(void) {

    for(uint8_t i=0;i<sizeof(W5100_CFG);i++) {
        eeprom_update_byte(&((uint8_t*)&eeETH)[i], pgm_read_byte(((uint8_t*)&eeDEFAULT)[i]));
    }
}


bool host2ip(uint8_t *hostaddr, uint8_t *ip) {
    uint8_t *ips[4];
    uint8_t cnt = cli_split_args(hostaddr, ips, '.', 4);
    if (cnt != 4) return false;
    for(uint8_t i=0;i<4;i++) {
        uint16_t x = (uint16_t)atoi((const char*)ips[i]);
        if (x > 255) return false;
        ip[i] = x & 0xFF;
    }
    return true;
}

bool hostbyname(const uint8_t *host, uint8_t *ip) {
    uint8_t dns[4];
    uint8_t buf[MAX_HOSTNAME+1];
    uint8_t i=0;

    do {
        buf[i] = host[i];
    } while (host[i++]);

    if (host2ip(buf, ip)) {

        //uart_pprint("host2p success\n");

        return true;
    }

    i=0;
    do {
        buf[i] = host[i];
    } while (host[i++]);

    eeprom_read_block(dns, eeDNS, 4);

    if (ip_valid(dns) != W5100_OK) return false;

    return  gethostbyname(DNS_SOCK, dns, buf, ip);
}

bool hostbyname_e(const uint8_t *eeHost, uint8_t* ip) {
    uint8_t nm[MAX_HOSTNAME+1];
    for(uint8_t i=0;i<MAX_HOSTNAME;i++) {
        nm[i] = eeprom_read_byte(&eeHost[i]);
        if (!nm[i]) break;
    }
    nm[MAX_HOSTNAME] = 0;

    return hostbyname(nm, ip);
}

void localtime_e(uint32_t time, Time *t) {
  localtime(time, t, (int16_t)eeprom_read_word((uint16_t*)&eeNTPOFFSET));
}
