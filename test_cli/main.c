// main.c
// AVR project


#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>

#include "gcc_macro.h"

#include "uart/strutil.h"
#include "uart/uart.h"
#include "cli.h"

#ifdef CLI_USE_HELP
static const __flash uint8_t help1[] = "display help";
static const __flash uint8_t  help2[] = "ip [ip address to set]";
static const __flash uint8_t  help3[] = "help cmd3";
static const __flash uint8_t  help4[] = "help cmd4";

const __flash uint8_t * const __flash cliHelps[] = {
    help1,
    help2,
    help3,
    help4
};
#endif

//static const __flash uint8_t _cmd0[] = "";
static const __flash uint8_t _cmd1[] = "help";
static const __flash uint8_t _cmd2[] = "ip";
static const __flash uint8_t _cmd3[] = "time";
static const __flash uint8_t _cmd4[] = "date";


const __flash uint8_t * const __flash cliCommands[] = {
    _cmd1,
    _cmd2,
    _cmd3,
    _cmd4,
//    _cmd0
};


bool cmd1(uint8_t *cmdline) {
    if (*cmdline) {
        uint8_t h = cli_cmd_index(cmdline);
        if (h != 0xFF) uart_printu_p(cliHelps[h]);
        else {
            cli_pprint("No help for '");
            cli_print((const char*)cmdline);
            cli_print_p(PSTR("\'"));
        }
        cli_pprint("\n\r");
    } else {
        uint8_t i=0;
        const void *c=cliFuncs[i];
        while(c) {
            cli_print_p((const char*)cliCommands[i]);
            cli_pprint(":  ");
            cli_print_p((const char*)cliHelps[i]);
            cli_pprint("\n\r");
            c = cliFuncs[++i];
        }
    }
    return true;
}

bool cmd2(uint8_t *cmdline) {
    cli_pprint("IP: ");
    cli_print((const char*)cmdline);
    cli_pprint("\n\r");
    uint8_t *ips[4];
    uint8_t cnt = cli_split_args(cmdline, ips, '.', 4);
    if (!cnt) {
        cli_pprint("..print ip..\n\r");
        return true;
    }
    if (cnt != 4) {
        cli_pprint("Wrong arg count: ");
        cli_print(word2str(cnt));
        return false;
    } else {
        for(uint8_t i=0;i<4;i++) {
            cli_pprint("#");
            cli_print(word2str(i));
            cli_pprint(": ");
            cli_print((const char*)ips[i]);
            int16_t x = atoi((const char*)ips[i]);
            if (x>=0 && x<=255 ) cli_pprint(" ok");
            else {
                cli_pprint(" bad IP\n\r");
                return false;
            }
            cli_pprint("\n\r");
        }
    }
    return true;
}

bool cmd3(uint8_t *cmdline) {
    cli_pprint("#3: ");
    cli_print((const char*)cmdline);
    cli_pprint("\n\r");
    return true;
}

bool cmd4(uint8_t *cmdline) {
    cli_pprint("#4: ");
    cli_print((const char*)cmdline);
    cli_pprint("\n\r");
    return true;
}

CLIFunction * const __flash cliFuncs[] = {
    &cmd1,
    &cmd2,
    &cmd3,
    &cmd4,
    0
};


void setup(void) {
//    DDR(LED_PORT) |= (1<<LED);
    uart_init(9600);
    asm volatile ("sei");
    cli_print = &uart_print;
    cli_print_p = &uart_print_p;
}


CLIData cli;

void onSerial(void) {
    while (!uart_rx_empty()) {
        uint8_t c = uart_getch();
        if (c > 0x40 && c < 0x5B) c += 32; // ASCII LoCase
        if (c == 9) c = ' '; // Tab -> space
        switch(c) {
            case 8:
                if (cli.len) {
                    cli.len--;
                    cli.cmdbuf[cli.len]=0;
                    cli_print("\x08");
                }
                break;

            case '\r':
                cli.cmdbuf[cli.len]=0;
                cli.ready = true;
                cli_pprint("\n\r");
                break;

            case '\n':
                break;

            default:
                if (cli.len <= CLI_MAXCMDSIZE) {
                    if (c >= ' ') {
                        uart_putch(c);
                        cli.cmdbuf[cli.len++] = c;
                    }
                }
        }
    }
}



void main(void) {
    setup();

    asm volatile ("sei");

    uart_pprint("Test CLI\n");
    uart_tx_flush();
    uart_rx_flush();
/*
    CLIFunction *fn = get_fn_1('22');
    if (fn) fn("arg test #1");

    fn = get_fn_2('ip', clis);
    if (fn) fn("arg test #2");
*/
    uart_print("itoa tests\n\r");
    uart_print_dec(0); uart_putch(' ');
    uart_print_dec(10); uart_putch(' ');
    uart_print_dec(100); uart_putch(' ');
    //uart_print_dec(1000); uart_print("\n\r");

    uart_print_hex(0); uart_putch(' ');
    uart_print_hex(10); uart_putch(' ');
    uart_print_hex(100); uart_putch(' ');
    //uart_print_hex(1000); uart_print("\n\r");

    uart_print_bin(0); uart_putch(' ');
    uart_print_bin(10); uart_putch(' ');
    uart_print_bin(100); uart_putch(' ');
    //uart_print_bin(1000); uart_print("\n\r");
/*
    uart_print(int2str(0)); uart_putch(' ');
    uart_print(int2str(10)); uart_putch(' ');
    uart_print(int2str(100)); uart_putch(' ');
    uart_print(int2str(1000)); uart_print("\n\r");

    uart_print(int2str(-10)); uart_putch(' ');
    uart_print(int2str(-100)); uart_putch(' ');
    uart_print(int2str(-1000)); uart_print("\n\r");
*/
    ZEROVAR(cli);
    for(;;) {
        if (!uart_rx_empty()) onSerial();
        if (cli.ready) {
            if (cli_command(cli.cmdbuf)) {
                uart_pprint("\n\rOK\n\r");
                /*uint8_t h=cli_cmd_index(cli.cmdbuf);
                uart_tx_flush();
                uart_pprint("Help[");
                uart_print_dec(h);
                uart_pprint("]: ");
                uart_tx_flush();
                uart_printu_p(cliHelps[h]);
                uart_pprint("\n\r");
                */
                uart_tx_flush();
            } else
                uart_pprint("\n\rERROR\n\r");
            ZEROVAR(cli);
        }
        //onSerial()
    };
}

//#pragma GCC diagnostic pop
