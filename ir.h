#ifndef __IR__H__
#define __IR__H__

/** IR Receiver (32bit width max), NEC sender
 *
 * IR Sender uses Timer2 and defined by IRSENDxxx pin, Interrupt-driven send
 * IR Receiver uses TImer2 and INT0 (PD2, arduino 2) input, Interrupt-driven receive
 *
 * Auto-switch INT0/Timer2 mode on send command
 * Undef IR_RECEIVE if receiver not used for disable INT0 setup switching
 * Undef IR_SEND if sender code is not used
 *
 * Usage:
 *
 * ir_init();
 * ...
 * ir_start(); // (re-) start receiving
 * for(;;) {
    if (ir_available()) {
        ...

        ir_start(); // clean processed code
    }
    if (SOME_CONDITION) {
        while(ir_busy()) ;
        ir_sendNEC8(0, 0xCC, 0); // ir_start() called internally on send complete
    }
 }
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 * Hardware: ATMega328p
 * Date: 2013
 */


#define IR_RECEIVE
#define IR_SEND

#define IRSEND_DDR  DDRC
#define IRSEND_PORT PORTC
#define IRSEND_PIN  PINC
#define IRSEND      3


void ir_init(void);  // initialize registers

#ifdef IR_RECEIVE

//NOTE: Call `ir_start` for resume ir receving
void ir_start(void);  // (re-) start receiving IR events
uint8_t ir_busy(void);  // check for ir receiving in progress
uint8_t ir_available(void);  // check if any code received
uint8_t ir_isNEC8(void);  // check if last received code is valid NEC code
uint8_t ir_NEC8addr(void);  // get addres portion of last received code
uint8_t ir_NEC8code(void);  // get code portion of last received code
uint8_t ir_isRepeat(void);  // is last code is repeat
uint32_t ir_code(void);  // last received code
volatile uint8_t * ir_code_bytes(void);  // last received code as array
#endif

#ifdef IR_SEND
void ir_sendNEC8(uint8_t addr, uint8_t cmd, uint8_t repeats);
uint8_t ir_sendComplete(void);
#endif

#endif // __IR__H__
