// stdlib or xprintf size testings

#include <avr/io.h>

#include "uart/uart.h"
#include "lcd.h"

// code: xsprintf:sprintf => 1954 : 2126 (sprintf + 172 bytes)
// data: xsprintf:sprintf =>  302 :  328

// adding atol =>  2598/377 : 2478/371 (atol - 120 bytes)
// adding strtol => 2528/271 : 3054/367 (strtol + 526 bytes)

//#define STD

#ifdef STD
    #include <stdio.h>
    #include <stdlib.h>
#else
    #include "zprintf.h"
#endif

int16_t freeRam(void) {
    extern int __heap_start, *__brkval;
    int v;
    return (int) &v - (__brkval==0 ? (int) &__heap_start : (int) __brkval);
}

void main(void) {
    uart_init(38400);
    lcd_init(LCD_DISP_ON);
    asm volatile ("sei");

    uint8_t ip[4] = { 192, 168, 0, 1 };
    char buf[128];

    static const __flash char __flash fmt[] = "%04d.%02d.%02d %02d:%02d:%02d\n";
    static const __flash char __flash fmt2[] = "%u.%u.%u.%u\n";
    char *text_ip [] = { "224", "190", "10", "0" };
    int32_t x;
    int16_t y;
    char *s;

    //uart_pprint("test sprintf/atoi/itoa\n");
    zhprintf_p(UART_SOCKET, PSTR("test sprintf/atoi/itoa\n"));
    //zhprintf_p(LCD_SOCKET, PSTR("test sprintf/atoi/itoa\n 123\n"));
    lcd_puts("test sprintf/atoi/itoa");
    lcd_putc('\n');
    lcd_puts("123");

    #ifdef STD
        sprintf(buf, fmt2, ip[0], ip[1], ip[2], ip[3]);
        uart_print(buf);
        sprintf(buf, fmt, 2013, 12, 15, 23, 44, 11);
        uart_print(buf);
        /*s = "123";
        x = atol(s);
        uart_print_dec(x);
        s = "0x123";
        x = atol("0x123");
        uart_print_dec(x);
        for(uint8_t i=0;i<4;i++) {
            x = atol(text_ip[i]);
            uart_print_dec(x);
        }
        */
        for(uint8_t i=0;i<4;i++) {
            x = strtol(text_ip[i], NULL, 10);
            uart_print_dec(x);
        }
    #else
        zhprintf_p(UART_SOCKET, fmt2, ip[0], ip[1], ip[2], ip[3]);
        //uart_print(buf);
        zhprintf_p(UART_SOCKET, fmt, 2013, 12, 15, 23, 44, 11);
        zhprintf_p(LCD_SOCKET, fmt, 2013, 12, 15, 23, 44, 11);
        //uart_print(buf);
        /*s = "123";
        xatoi(&s, &x);
        uart_print_dec(x);
        s = "0x123";
        xatoi(&s, &x);
        uart_print_dec(x);
        for(uint8_t i=0;i<4;i++) {
            xatoi(&text_ip[i], &x);
            uart_print_dec(x);
        }
        */
        for(uint8_t i=0;i<4;i++) {
            zatoi(&text_ip[i], &x);
            //uart_print_dec(x);
            zhprintf_p(UART_SOCKET, PSTR("%d"), x);
        }
        zhprintf_p(UART_SOCKET, PSTR("Free RAM: %d\n"), freeRam());
        //zhprintf_p(LCD_SOCKET, PSTR("Free RAM: %d\n"), freeRam());
        //lcd_clrscr();
        zhprintf_p(LCD_SOCKET, PSTR("%d-ints %d %d %d %d %d\n"), 6, 1, 2, 3, 4, 5);
        zhprintf_p(LCD_SOCKET, PSTR("Zero-pad: %02d\n"), (int)1);

        zhprintf_p(UART_SOCKET, PSTR("Test IP atoi fo 159.254.0.1: "));
        char *aip = "159 254 0 1";
        uint8_t r = 0;
        do {
            r = zatoi(&aip, &x);
            zhprintf_p(UART_SOCKET, PSTR("value: %u\n"), x);
        } while (r != 0);
        zhprintf_p(UART_SOCKET, PSTR("done.\n"));
    #endif

    for(;;) {
        ;
    }
}
