
#include <avr/io.h>
//#include
#include "delay.h"
#include "uart/uart.h"
#include "uart/strutil.h"
#include "i2c.h"
#include "ds1307.h"
#include "datetime.h"

void main(void) {
    asm volatile("sei");

    bool b;
    uart_init(57600);
    i2c_init();
    b = rtc_init();

    uart_pprint("RTC test\n");
    if (b) uart_pprint("init ok\n");
    else
        uart_pprint("init fail\n");

    for(;;) {
        Time t;
        rtc_time(&t);
        uart_pprint("RTC: ");
        uart_print_dec(t.hour); uart_putch(':');
        uart_print_dec(t.minute); uart_putch(':');
        uart_print_dec(t.second); uart_putch('\n');

        delay100ms(10); // 1s
    }
}
