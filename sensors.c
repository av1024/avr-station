/**
 * Common sensor handling routines
 *
 *
 *
 */
#include <avr/io.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "eep.h"
#include "cli_io.h"
#include "sensors.h"

#include "bmp085.h"
#include "DHT22.h"
#include "ds18x20.h"

// 1-wire data
uint8_t ow_cnt = 0;
uint8_t ow_ids[OW_MAXDEVICES][OW_ROMCODE_SIZE];

SensorData sensors[SENSOR_COUNT] = {};

// add sensor to sensors[] if not exist
SensorData * add_sensor(uint8_t id, uint8_t *mac) {

    for(uint8_t i=0;i<SENSOR_COUNT;i++) {
        SensorData *x = &sensors[i];
        if (x->id) {

            if (memcmp(x->mac, mac, 6)==0) {
                return x;
            }

        } else {
            x->id = id;
            x->flags = SENS_FLAG_NEW | SENS_FLAG_ON | SENS_FLAG_OK;
            memcpy(x->mac, mac, 6);
            memset(x->avg, SENS_AVG_INVALID, 2*SENS_AVG_SIZE);
            return x;
        }
    }
    return NULL;
}

SensorData * find_sensor_by_id(uint8_t id) {
    for(uint8_t i=0;i<SENSOR_COUNT;i++) {
        SensorData *x = &sensors[i];
        if (x->id == id) return x;
    }
    return NULL;
}

SensorData * find_sensor_by_mac(uint8_t *mac) {
    for(uint8_t i=0;i<SENSOR_COUNT;i++) {
        SensorData *x = &sensors[i];
        if (x->id && (memcmp(x->mac, mac, 6)==0)) return x;
    }
    return NULL;
}

void update_avg(SensorData *x) {
    if (x->flags & SENS_FLAG_OK) {
        for(uint8_t i=SENS_AVG_SIZE-1;i>=1;i--) x->avg[i] = x->avg[i-1];
        x->avg[0] = x->val.intg * 10 + ((x->val.intg < 0) ? -x->val.frac : x->val.frac);
    }
}

void update_sensor(SensorData *x, int16_t intg, uint8_t frac) {

    x->val.intg = intg;
    x->val.frac = frac;
    if ((x->flags & SENS_FLAG_OK) == 0) {
        _log_byte_mac_p(PSTR(" Sensor #%u online "), x->id, x->mac);
    }
    x->flags = (x->flags << 4) | (x->flags & 0x0F) | SENS_FLAG_OK;
    if (x->flags & SENS_FLAG_NEW) {
        if (!load_sensor(x)) {
            if (!store_sensor(x)) {
                _plog("Failed to save new sensor.");
            } else {
                _log_byte_mac_p(PSTR(" New sensor #%u "), x->id, x->mac);
            }
        }
        x->flags = (x->flags<<4) & ~SENS_FLAG_NEW;
    }
}

//FIXME: rewrite AVG logic for reduce code size
SensorValue avg_sensor(SensorData *x) {
    SensorValue v = {0, 0};
#ifdef SENS_AVG_16BIT
    int16_t z=0;
#else
    int32_t z=0;
#endif
    uint8_t i=0;
    int8_t c;
    while(i<SENS_AVG_SIZE) {
        if (x->avg[i] == SENS_AVG_INVALID_VALUE) break;
        z += x->avg[i];
        i++;
    }
    if (i) {
        c = z % i;
        c = (c * 10) / i;
        c = (c > 4) ? 1 : 0; // round up on 0.5

        z = (z / i) + ((z > 0) ? c : -c);
        v.intg = (int16_t)(z / 10);
        v.frac = (uint8_t)(z % 10);

        if (v.frac & 0x80) v.frac = -v.frac;
    } else {
        v = x->val;
    }
    return v;
}

// change direction as curr-last AVG value
uint8_t dir_sensor(SensorData *x) {
/*
    SensorValue sv = avg_sensor(x);
    //NB: means identical
    int16_t av = sv.intg * 10 + ((sv.intg>=0) ? sv.frac : - sv.frac);
    uint8_t d_a = 0, d_eq = 0, d_b = 0;
    uint8_t i;

    for(i=0;i<SENS_AVG_SIZE;i++) {
        int16_t z = x->avg[i];
        if (z == SENS_AVG_INVALID_VALUE) break;

        if (av < z) d_a++;
        else if (av > z) d_b++;
            else d_eq++;
    }

    if (d_eq >= (d_a + d_b)) return SENS_DIR_NONE;

    if (d_a > d_b) return SENS_DIR_UP;
    if (d_a < d_b) return SENS_DIR_DOWN;
*/
    int16_t last = x->avg[0];
    uint8_t d_a=0, d_b=0, d_eq=0;
    for(uint8_t i=1;i<SENS_AVG_SIZE;i++) {
        int16_t z = x->avg[i];
        if (z == SENS_AVG_INVALID_VALUE) break;
        if (last > z) d_a++;
        else
            if (last < z) d_b++;
            else d_eq++;
    }
    if (d_eq >= (d_a + d_b)) return SENS_DIR_NONE;

    if (d_a > d_b) return SENS_DIR_UP;
    if (d_a < d_b) return SENS_DIR_DOWN;
    return SENS_DIR_NONE;
}

// Try load settings from EEPROM by MAC if exists
bool load_sensor(SensorData *x) {
    SensorID id;

    if (!x) return false;

    for(uint8_t i=0;i<SENSOR_COUNT;i++) {
        eeprom_read_block(&id, &eeSENSORS[i], sizeof(SensorID));

        if (memcmp(id.mac, x->mac, 6)==0 && id.id) {
            x->id = id.id;
            x->flags = id.flags;

            return true; // FOUND
        }
    }
    return false;
}

// Try update/add settings to EEPROM if has room
bool store_sensor(SensorData *x) {
    SensorID id;
    uint8_t free_idx = 0xFF;

    if (!x) return false;

    for(uint8_t i=0;i<SENSOR_COUNT;i++) {
        eeprom_read_block(&id, &eeSENSORS[i], sizeof(SensorID));
        if ((id.id==0) && (free_idx==0xFF)) free_idx = i;
        if (memcmp(id.mac, x->mac, 6)==0) {
            free_idx = i;
            break;
        }
    }
    if (free_idx != 0xFF) {
        id.id = x->id;
        id.flags = x->flags & 0x0F & ~SENS_FLAG_NEW & ~SENS_FLAG_OK; // keep "ON" and "NARODMON"
        memcpy(id.mac, x->mac, 6);

        eeprom_update_block(&id, &eeSENSORS[free_idx], sizeof(SensorID));
        return true;
    }
    return false;
}

/* == Update individual sensors ==
 *
 *
 */

void update_AM2302(void) {
    DHT22_DATA_t dht;
    SensorData *x = find_sensor_by_id(SENS_DHT_ID_T);
    SensorData *y = find_sensor_by_id(SENS_DHT_ID_RH);

    if (!x || !y) {
        _plog("AM: Uninitialized");
        return;
    }

    if (readDHT22(&dht) == DHT_ERROR_NONE) {
        //if (!(SFlags & SFL_DHT)) _log_p(PSTR("AM OK"));

        update_sensor(x, dht.t.intg, dht.t.frac);
        update_sensor(y, dht.rh.intg, dht.rh.frac);
        SFlags |= SFL_DHT;
        return;
    }

    if ((x->flags & SENS_FLAG_OK)) {
        x->flags = (x->flags << 4) | ((x->flags & 0x0F) & ~SENS_FLAG_OK);
        y->flags = (y->flags << 4) | ((y->flags & 0x0F) & ~SENS_FLAG_OK);
        _log_p(PSTR("AM: Fail"));
    }

    SFlags &= ~SFL_DHT;
}


void update_BMP(void) {
    SensorData *x = find_sensor_by_id(SENS_BMP_ID_T);
    SensorData *y = find_sensor_by_id(SENS_BMP_ID_P);

    if (!x || !y) {
        _plog("BMP: Uninitialized");
        return;
    }

    if (bmp085_valid() && bmp085_readAll()) {

        bmp085_calculate();
        update_sensor(x, BMP085Values.t.intg, BMP085Values.t.frac);
        update_sensor(y, BMP085Values.p.intg - SENS_BMP_BASE, BMP085Values.p.frac);

        SFlags |= SFL_BMP;
        return;
    }

    if ((x->flags & SENS_FLAG_OK)) {
        x->flags = (x->flags << 4) | ((x->flags & 0x0F) & ~SENS_FLAG_OK);
        y->flags = (y->flags << 4) | ((y->flags & 0x0F) & ~SENS_FLAG_OK);
        _plog("BMP: Fail");
    }

    SFlags &= ~SFL_BMP;
    bmp085_readEEPROM();
}

void update_DS18x20(void) {
    uint8_t buf[2];
    SensorData *x;

    for(uint8_t i=0;i<ow_cnt;i++) {
        if (DS18x20_ReadData(ow_ids[i], buf)) {
            // convert/print
            x = find_sensor_by_mac(&ow_ids[i][1]);
            if (!x) {
                _log_byte_mac_p(PSTR(" 1W #%u NEW "), SENS_1W_MIN_ID+i, x->mac);
                x = add_sensor(SENS_1W_MIN_ID+i, &ow_ids[i][1]);
                store_sensor(x);
                continue;
            }

            DS18x20Value v = DS18x20_ConvertToTemp(buf);
            if ((v.dec==85 && v.frac==0) | (v.dec>125 || v.dec<-125)) {
                //TODO: Add LCD Flag "1w reset"
                if (x->flags & SENS_FLAG_OK) {
                    _log_byte_mac_p(PSTR(" 1W #%u FAIL "), x->id, x->mac);
                }
                x->flags = (x->flags<<4) & ((x->flags & 0x0F) & ~SENS_FLAG_OK);
                continue;
            }

            update_sensor(x, v.dec, v.frac);
        }
    }
}
