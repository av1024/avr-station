// Alternate versions of delay_ms

#include <util/delay.h>
#include "delay.h"
#include "gcc_macro.h"

NOINLINE void delay1ms(uint8_t ms) {
    while (ms--) _delay_ms(1);
}

NOINLINE void delay10ms(uint8_t ts) {
    while(ts--) delay1ms(10);
}

NOINLINE void delay100ms(uint8_t ds) {
    while(ds--) _delay_ms(100);
}

