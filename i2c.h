#ifndef __I2C_H__
#define __I2C_H__

/**
 * Base hardware I2C functions.
 *
 * Author: Alexander Voronin <av1024@gmail.com>
 * Software: AVR-GCC 4.7
 * Hardware: ATMega328p
 * Date: 2013
 */

/******  SPECIAL DEFINES ******/
//Uncomment to enable internal pull-ups on initialization
//#define I2C_INTERNAL_PULLUPS

// If defined used timeout protected TWCR loop (500us total)
// safe version consume extra 24 bytes
#define I2C_SAFE_WAIT

/****** I2C bus speed *********/

// Fscl = Fcpu/(16+2*(TWBR)*TWPS_mask)
// 100kHz
//#define I2C_TWSR ( (0<<TWPS1) | (1<<TWPS0) )
//#define I2C_TWBR 18
// 200kHz:
//#define I2C_TWSR ( (0<<TWPS1) | (1<<TWPS0) )
//#define I2C_TWBR 8
// 400kHz:
#define I2C_TWSR ( (0<<TWPS1) | (1<<TWPS0) )
#define I2C_TWBR 3
// 800kHz:
//#define I2C_TWSR ( (0<<TWPS1) | (0<<TWPS0) )
//#define I2C_TWBR 2
// 1000kHz:
//#define I2C_TWSR ( (0<<TWPS1) | (0<<TWPS0) )
//#define I2C_TWBR 0

/****** Port defineitions ******/

#define I2C_PORT    PORTC
#define I2C_DDR     DDRC
#define I2C_SDA     PC4
#define I2C_SCL     PC5

/****** I2C control action codes ********/
#define I2C_START           0
#define I2C_RESTART         1
#define I2C_STOP            2
#define I2C_TRANSMIT        3
#define I2C_RECEIVE_ACK     4
#define I2C_RECEIVE_NACK    5

#ifndef bool
    typedef unsigned char bool __attribute__((__mode__(__QI__)));
    #define true    1
    #define false   0
#endif


void i2c_init(void);
uint8_t i2c(uint8_t action);
bool i2c_start(uint8_t address);
void i2c_stop(void);
uint8_t i2c_readAck(void);
uint8_t i2c_readNAck(void);
bool i2c_write(uint8_t data);
uint16_t i2c_read_word(uint8_t addr); // return 0 on error
bool i2c_read_buf(uint8_t addr, uint8_t *buf, uint8_t count);

#endif
