#ifndef __TIMER_H__
#define __TIMER_H__
/**
 * Timer-counter.
 * Uses TIMER0 or TIMER1 for 10ms tick counter. TIMER1 is more accurate, but 16 bit
 *
 * Typical usage:
 *
 * // in setup
 * init_timer();
 * // optional: NTPTime(..., unix_timestamp);
 *
 * void onSecond() {
 *   ...
 * }
 *
 * void onMinute() {
 *
 * }
 *
 * for(;;) {
 *
 *   if (timer.Flags & TFLAGS_SEC) {
 *     onSecond();
 *     timer.Flags &= ~TFLAGS_SEC;
 *   }
 *   if (timer.Flags & TFLAGS_MIN) {
 *     onMinute();
 *     timer.Flags &= ~TFLAGS_MIN;
 *   }
 * }
 */

// *** Timer selection ***

// Timer0 a bit inaccurate 10.05ms (multiplier should be 156.25) but less code
//#define TIMER0_10MS_COUNTER
//Timer1 can provide exact 10.00ms tick, but +16bytes code
#define TIMER1_10MS_COUNTER


//Bit is set on time event (see below)
//User should manually clear bit if necessary

#define TFLAGS_10MS  (1<<0)
#define TFLAGS_500MS (1<<1)
#define TFLAGS_SEC   (1<<2)
#define TFLAGS_MIN   (1<<3)
#define TFLAGS_HOUR  (1<<4)
#define TFLAGS_DAY   (1<<5)

typedef struct {
    uint8_t S;
    uint8_t M;
    uint8_t H;
    uint8_t MS;    // 10ms counter [0..99]
    uint8_t Flags; //Bit is set on time event (see below)
} Timer_t;

extern volatile Timer_t timer;

// unix timestamp counter, incremented in timer ISR
// you should set initial value via NTP/RTC data
extern volatile uint32_t unix_timestamp;

void init_timer(void);

// reset all counters and restart 10ms timer.
// Note: unix_timestamp not cleared, enables interrupts
void restart_timer(void);

#endif // __TIMER_H__
